import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { finalize } from 'rxjs/operators';
import { PayloadPropertiesServiceProxy, CreateOrEditPayloadPropertyDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { PayloadLookupTableModalComponent } from './payload-lookup-table-modal.component';


@Component({
    selector: 'createOrEditPayloadPropertyModal',
    templateUrl: './create-or-edit-payloadProperty-modal.component.html'
})
export class CreateOrEditPayloadPropertyModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;
    @ViewChild('payloadLookupTableModal') payloadLookupTableModal: PayloadLookupTableModalComponent;


    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    payloadProperty: CreateOrEditPayloadPropertyDto = new CreateOrEditPayloadPropertyDto();

    payloadName = '';


    constructor(
        injector: Injector,
        private _payloadPropertiesServiceProxy: PayloadPropertiesServiceProxy
    ) {
        super(injector);
    }

    show(payloadPropertyId?: string): void {

        if (!payloadPropertyId) {
            this.payloadProperty = new CreateOrEditPayloadPropertyDto();
            this.payloadProperty.id = payloadPropertyId;
            this.payloadName = '';

            this.active = true;
            this.modal.show();
        } else {
            this._payloadPropertiesServiceProxy.getPayloadPropertyForEdit(payloadPropertyId).subscribe(result => {
                this.payloadProperty = result.payloadProperty;

                this.payloadName = result.payloadName;

                this.active = true;
                this.modal.show();
            });
        }
    }

    save(): void {
            this.saving = true;
			
            this._payloadPropertiesServiceProxy.createOrEdit(this.payloadProperty)
             .pipe(finalize(() => { this.saving = false; }))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
             });
    }

        openSelectPayloadModal() {
        this.payloadLookupTableModal.id = this.payloadProperty.payloadId;
        this.payloadLookupTableModal.displayName = this.payloadName;
        this.payloadLookupTableModal.show();
    }


        setPayloadIdNull() {
        this.payloadProperty.payloadId = null;
        this.payloadName = '';
    }


        getNewPayloadId() {
        this.payloadProperty.payloadId = this.payloadLookupTableModal.id;
        this.payloadName = this.payloadLookupTableModal.displayName;
    }


    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
