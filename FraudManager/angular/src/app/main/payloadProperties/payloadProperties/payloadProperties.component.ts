import { Component, Injector, ViewEncapsulation, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Http } from '@angular/http';
import { PayloadPropertiesServiceProxy, PayloadPropertyDto , PayloadPropertyDtoDataType } from '@shared/service-proxies/service-proxies';
import { NotifyService } from '@abp/notify/notify.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { CreateOrEditPayloadPropertyModalComponent } from './create-or-edit-payloadProperty-modal.component';
import { ViewPayloadPropertyModalComponent } from './view-payloadProperty-modal.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/components/table/table';
import { Paginator } from 'primeng/components/paginator/paginator';
import { LazyLoadEvent } from 'primeng/components/common/lazyloadevent';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { EntityTypeHistoryModalComponent } from '@app/shared/common/entityHistory/entity-type-history-modal.component';
import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
    templateUrl: './payloadProperties.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class PayloadPropertiesComponent extends AppComponentBase {

    @ViewChild('createOrEditPayloadPropertyModal') createOrEditPayloadPropertyModal: CreateOrEditPayloadPropertyModalComponent;
    @ViewChild('viewPayloadPropertyModalComponent') viewPayloadPropertyModal: ViewPayloadPropertyModalComponent;
    @ViewChild('entityTypeHistoryModal') entityTypeHistoryModal: EntityTypeHistoryModalComponent;
    @ViewChild('dataTable') dataTable: Table;
    @ViewChild('paginator') paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = '';
    nameFilter = '';
    dataTypeFilter = -1;
        payloadNameFilter = '';

    dataTypes = PayloadPropertyDtoDataType;

    _entityTypeFullName = 'Test.PayloadProperties.PayloadProperty';
    entityHistoryEnabled = false;

    constructor(
        injector: Injector,
        private _payloadPropertiesServiceProxy: PayloadPropertiesServiceProxy,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.entityHistoryEnabled = this.setIsEntityHistoryEnabled();
    }

    private setIsEntityHistoryEnabled(): boolean {
        let customSettings = (abp as any).custom;
        return customSettings.EntityHistory && customSettings.EntityHistory.isEnabled && _.filter(customSettings.EntityHistory.enabledEntities, entityType => entityType === this._entityTypeFullName).length === 1;
    }

    getPayloadProperties(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._payloadPropertiesServiceProxy.getAll(
            this.filterText,
            this.nameFilter,
            this.dataTypeFilter,
            this.payloadNameFilter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createPayloadProperty(): void {
        this.createOrEditPayloadPropertyModal.show();
    }

    showHistory(payloadProperty: PayloadPropertyDto): void {
        this.entityTypeHistoryModal.show({
            entityId: payloadProperty.id.toString(),
            entityTypeFullName: this._entityTypeFullName,
            entityTypeDescription: ''
        });
    }

    deletePayloadProperty(payloadProperty: PayloadPropertyDto): void {
        this.message.confirm(
            '',
            (isConfirmed) => {
                if (isConfirmed) {
                    this._payloadPropertiesServiceProxy.delete(payloadProperty.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }

    exportToExcel(): void {
        this._payloadPropertiesServiceProxy.getPayloadPropertiesToExcel(
        this.filterText,
            this.nameFilter,
            this.dataTypeFilter,
            this.payloadNameFilter,
        )
        .subscribe(result => {
            this._fileDownloadService.downloadTempFile(result);
         });
    }
}
