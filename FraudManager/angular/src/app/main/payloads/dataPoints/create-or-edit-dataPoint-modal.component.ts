import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { finalize } from 'rxjs/operators';
import { PayloadsServiceProxy, CreateOrEditDataPointDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';



@Component({
    selector: 'createOrEditDataPointModal',
    templateUrl: './create-or-edit-dataPoint-modal.component.html'
})
export class CreateOrEditDataPointModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;
  

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    properties = [];
    dataPoint: CreateOrEditDataPointDto = new CreateOrEditDataPointDto();


    constructor(
        injector: Injector,
        private _payloadsServiceProxy: PayloadsServiceProxy
    ) {
        super(injector);
    }

    show(dataPointId?: string): void {

        if (!dataPointId) {
            this.dataPoint = new CreateOrEditDataPointDto();
            this.dataPoint.id = null;
            this.active = true;
            this.modal.show();
        } else {
            this._payloadsServiceProxy.getDataPointForEdit(dataPointId).subscribe(result => {

                this.active = true;
                this.dataPoint = result;
                this.modal.show();
            });
        }
    }

    save(): void {
            this.saving = true;

        if (!this.dataPoint.id) {
            this._payloadsServiceProxy.createDataPoint(this.dataPoint)
                .pipe(finalize(() => { this.saving = false; }))
                .subscribe(() => {
                    this.notify.info(this.l('SavedSuccessfully'));
                    this.close();
                    this.modalSave.emit(null);
                });
        }
        else {
            this._payloadsServiceProxy.updateDataPoint(this.dataPoint)
                .pipe(finalize(() => { this.saving = false; }))
                .subscribe(() => {
                    this.notify.info(this.l('SavedSuccessfully'));
                    this.close();
                    this.modalSave.emit(null);
                });
        }
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
