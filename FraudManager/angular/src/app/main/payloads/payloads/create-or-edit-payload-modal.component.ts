import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { finalize } from 'rxjs/operators';
    
import { PayloadsServiceProxy, CreateOrEditPayloadDto, CreateOrEditPayloadPropertyDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { TransactionTypeLookupTableModalComponent } from './transactionType-lookup-table-modal.component';


@Component({
    selector: 'createOrEditPayloadModal',
    templateUrl: './create-or-edit-payload-modal.component.html'
})
export class CreateOrEditPayloadModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;
    @ViewChild('transactionTypeLookupTableModal') transactionTypeLookupTableModal: TransactionTypeLookupTableModalComponent;


    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    props: CreateOrEditPayloadPropertyDto[];
    active = false;
    saving = false;
    properties = [];
    payload: CreateOrEditPayloadDto = new CreateOrEditPayloadDto();
    item = {
        ind: 1,
        name: '',
        dataType: null,
        description: null
    }
    transactionTypeName = '';


    constructor(
        injector: Injector,
        private _payloadsServiceProxy: PayloadsServiceProxy
    ) {
        super(injector);
    }

    show(payloadId?: string): void {

        if (!payloadId) {
            this.payload = new CreateOrEditPayloadDto();
            this.payload.id = payloadId;
            this.transactionTypeName = '';
            this.properties = [];
            this.active = true;
            this.modal.show();
        } else {
            this._payloadsServiceProxy.getPayloadForEdit(payloadId).subscribe(result => {
                this.payload = result.payload;

                this.transactionTypeName = result.transactionTypeName;

                this.properties = [];

                for (var i = 1; i < result.payload.payloadProperties.length; i++) {
                    var newItem = {
                        ind: i,
                        id: result.payload.payloadProperties[i].id,
                        name: result.payload.payloadProperties[i].name,
                        dataType: result.payload.payloadProperties[i].dataType,
                        description: result.payload.payloadProperties[i].description,
                        dataTypeName: result.payload.payloadProperties[i].dataTypeName
                    }
                    this.properties.push(newItem);
                }

                this.active = true;
                this.modal.show();
            });
        }
    }

    save(): void {
        this.saving = true;


        if (this.properties.length == 0) {
            this.notify.error('You need to define at least one property for this payload.');
            return;
        }
        this.props = [];

        for (var i = 0; i < this.properties.length; i++) {
            var item = new CreateOrEditPayloadPropertyDto();
            item.name = this.properties[i].name;
            item.dataType = this.properties[i].dataType;
            item.description = this.properties[i].description;
            item.id = this.properties[i].id;

            this.props.push(item);
        }

        this.payload.payloadProperties = this.props;

        this._payloadsServiceProxy.createOrEdit(this.payload)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }

    openSelectTransactionTypeModal() {
        this.transactionTypeLookupTableModal.id = this.payload.transactionTypeId;
        this.transactionTypeLookupTableModal.displayName = this.transactionTypeName;
        this.transactionTypeLookupTableModal.show();
    }


    addLine(): void {

        this.item.ind = this.properties.length + 1;

        var dataTypeName = '';

        switch (this.item.dataType) {
            case '0':
                {
                    dataTypeName = "String";
                }
                break;
            case '1':
                {
                    dataTypeName = "Integer";
                }
                break;
            case '2':
                {
                    dataTypeName = "Decimal";
                }
                break;
            case '3':
                {
                    dataTypeName = "DateTime";
                }
                break;
        }



        var newItem = {
            ind: this.item.ind,
            name: this.item.name,
            description: this.item.description,
            dataType: this.item.dataType,
            dataTypeName: dataTypeName
        }

        for (var i = 0; i < this.properties.length; i++) {
            if (this.properties[i].name == newItem.name && this.properties[i].name == newItem.description && this.properties[i].dataType == newItem.dataType) {
                this.notify.info("This property has been added already!");
                return;
            }    
        }
        this.properties.push(newItem);
    }


    removeLine(ind: number): void {
        this.properties.splice(ind -1, 1);
    }


    setTransactionTypeIdNull() {
        this.payload.transactionTypeId = null;
        this.transactionTypeName = '';
    }


    getNewTransactionTypeId() {
        this.payload.transactionTypeId = this.transactionTypeLookupTableModal.id;
        this.transactionTypeName = this.transactionTypeLookupTableModal.displayName;
    }


    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
