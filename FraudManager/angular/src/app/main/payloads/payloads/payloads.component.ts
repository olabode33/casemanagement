import { Component, Injector, ViewEncapsulation, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Http } from '@angular/http';
import { PayloadsServiceProxy, GetDataPointsForView  } from '@shared/service-proxies/service-proxies';
import { NotifyService } from '@abp/notify/notify.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { CreateOrEditPayloadModalComponent } from './create-or-edit-payload-modal.component';

import { CreateOrEditDataPointModalComponent } from '@app/main/payloads/dataPoints/create-or-edit-dataPoint-modal.component';


import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/components/table/table';
import { Paginator } from 'primeng/components/paginator/paginator';
import { LazyLoadEvent } from 'primeng/components/common/lazyloadevent';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { EntityTypeHistoryModalComponent } from '@app/shared/common/entityHistory/entity-type-history-modal.component';
import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
    templateUrl: './payloads.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class PayloadsComponent extends AppComponentBase {

    @ViewChild('createOrEditPayloadModal') createOrEditPayloadModal: CreateOrEditPayloadModalComponent;

    @ViewChild('createOrEditDataPointModal') createOrEditDataPointModal: CreateOrEditDataPointModalComponent
    //@ViewChild('viewPayloadModalComponent') viewPayloadModal: ViewPayloadModalComponent;
    @ViewChild('entityTypeHistoryModal') entityTypeHistoryModal: EntityTypeHistoryModalComponent;
    @ViewChild('dataTable') dataTable: Table;
    @ViewChild('paginator') paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = '';
    nameFilter = '';
        transactionTypeNameFilter = '';


    _entityTypeFullName = 'Test.Payloads.Payload';
    entityHistoryEnabled = false;

    constructor(
        injector: Injector,
        private _payloadsServiceProxy: PayloadsServiceProxy,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.entityHistoryEnabled = this.setIsEntityHistoryEnabled();
    }

    private setIsEntityHistoryEnabled(): boolean {
        let customSettings = (abp as any).custom;
        return customSettings.EntityHistory && customSettings.EntityHistory.isEnabled && _.filter(customSettings.EntityHistory.enabledEntities, entityType => entityType === this._entityTypeFullName).length === 1;
    }

    getPayloads(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._payloadsServiceProxy.getAll(
            this.filterText,
            this.nameFilter,
            this.transactionTypeNameFilter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createPayload(): void {
        this.createOrEditPayloadModal.show();
    }

    createDataPoint(): void {
        this.createOrEditDataPointModal.show();
    }


    editDataPoint(data: GetDataPointsForView): void {

        if (data.dataClass == 'Standing Data') {
            this.createOrEditDataPointModal.show(data.id);
        }
        else {
            this.createOrEditPayloadModal.show(data.id);
        }
    }




    deletePayload(payload: string): void {
        this.message.confirm(
            '',
            (isConfirmed) => {
                if (isConfirmed) {
                    this._payloadsServiceProxy.delete(payload)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }

    exportToExcel(): void {
        this._payloadsServiceProxy.getPayloadsToExcel(
        this.filterText,
            this.nameFilter,
            this.transactionTypeNameFilter,
        )
        .subscribe(result => {
            this._fileDownloadService.downloadTempFile(result);
         });
    }
}
