import { CloseCaseModalComponent } from './cases/_subs/close-case-modal/close-case-modal.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppCommonModule } from '@app/shared/common/app-common.module';
import { BlacklistsComponent } from './blacklists/blacklists/blacklists.component';
import { ViewBlacklistModalComponent } from './blacklists/blacklists/view-blacklist-modal.component';
import { CreateOrEditBlacklistModalComponent } from './blacklists/blacklists/create-or-edit-blacklist-modal.component';

import { WhitelistsComponent } from './whitelists/whitelists/whitelists.component';

//import { DemoUiFileUploadComponent as UploadModuleComponent } from '@app/admin/demo-ui-components/demo-ui-file-upload.component';

import { CreateOrEditWhitelistModalComponent } from './whitelists/whitelists/create-or-edit-whitelist-modal.component';

import { CasesComponent } from './cases/cases/cases.component';
import { ViewCaseModalComponent } from './cases/cases/view-case-modal.component';
import { CreateOrEditCaseModalComponent } from './cases/cases/create-or-edit-case-modal.component';


import { UserLookupTableModalComponent } from './ruleEscalations/ruleEscalations/user-lookup-table-modal.component';

import { RuleLookupTableModalComponent } from './ruleConfigurations/ruleConfigurations/rule-lookup-table-modal.component';
import { DataPointLookupTableModalComponent } from './ruleConfigurations/ruleConfigurations/dataPoint-lookup-table-modal.component';

import { RulesComponent } from './rules/rules/rules.component';
import { ViewRuleModalComponent } from './rules/rules/view-rule-modal.component';
import { CreateOrEditRuleModalComponent } from './rules/rules/create-or-edit-rule-modal.component';

import { PayloadPropertiesComponent } from './payloadProperties/payloadProperties/payloadProperties.component';
import { ViewPayloadPropertyModalComponent } from './payloadProperties/payloadProperties/view-payloadProperty-modal.component';
import { CreateOrEditPayloadPropertyModalComponent } from './payloadProperties/payloadProperties/create-or-edit-payloadProperty-modal.component';
import { PayloadLookupTableModalComponent } from './payloadProperties/payloadProperties/payload-lookup-table-modal.component';

import { PayloadsComponent } from './payloads/payloads/payloads.component';

import { CreateOrEditDataPointModalComponent } from './payloads/dataPoints/create-or-edit-dataPoint-modal.component';



import { CreateOrEditPayloadModalComponent } from './payloads/payloads/create-or-edit-payload-modal.component';
import { TransactionTypeLookupTableModalComponent } from './payloads/payloads/transactionType-lookup-table-modal.component';

import { TransactionTypesComponent } from './transactionTypes/transactionTypes/transactionTypes.component';
import { ViewTransactionTypeModalComponent } from './transactionTypes/transactionTypes/view-transactionType-modal.component';
import { CreateOrEditTransactionTypeModalComponent } from './transactionTypes/transactionTypes/create-or-edit-transactionType-modal.component';
import { ChannelLookupTableModalComponent } from './transactionTypes/transactionTypes/channel-lookup-table-modal.component';

import { ChannelsComponent } from './channels/channels/channels.component';
import { CreateOrEditChannelModalComponent } from './channels/channels/create-or-edit-channel-modal.component';
import { AutoCompleteModule } from 'primeng/primeng';
import { PaginatorModule } from 'primeng/primeng';
import { EditorModule } from 'primeng/primeng';
import { InputMaskModule } from 'primeng/primeng'; import { FileUploadModule } from 'primeng/primeng';
import { TableModule } from 'primeng/table';

import { UtilsModule } from '@shared/utils/utils.module';
import { CountoModule } from 'angular2-counto';
import { ModalModule, TabsModule, TooltipModule, BsDropdownModule } from 'ngx-bootstrap';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MainRoutingModule } from './main-routing.module';
import { NgxChartsModule } from '@swimlane/ngx-charts';

import { BsDatepickerModule, BsDatepickerConfig, BsDaterangepickerConfig, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { NgxBootstrapDatePickerConfigService } from 'assets/ngx-bootstrap/ngx-bootstrap-datepicker-config.service';
import { ViewCaseDetailsComponent } from './cases/cases/view-case-details.component';
import { HomeComponent } from '@app/main/home/home.component';
import { CreateEditRuleComponent } from './rules/_subs/create-edit-rule/create-edit-rule.component';
import { ViewRuleComponent } from './rules/_subs/view-rule/view-rule.component';
import { CreateTaskModalComponent } from './cases/_subs/create-task-modal/create-task-modal.component';
import { CompleteTaskModalComponent } from './cases/_subs/complete-task-modal/complete-task-modal.component';

NgxBootstrapDatePickerConfigService.registerNgxBootstrapDatePickerLocales();

@NgModule({
  imports: [
    FileUploadModule,
    AutoCompleteModule,
    PaginatorModule,
    EditorModule,
    InputMaskModule, TableModule,

    CommonModule,
    FormsModule,
    ModalModule,
    TabsModule,
    TooltipModule,
    AppCommonModule,
    UtilsModule,
    MainRoutingModule,
    CountoModule,
    NgxChartsModule,
    BsDatepickerModule.forRoot(),
    BsDropdownModule.forRoot()
  ],
  declarations: [
    CloseCaseModalComponent,
    CompleteTaskModalComponent,
    CreateTaskModalComponent,
    ViewRuleComponent,
    CreateEditRuleComponent,
    HomeComponent,
    BlacklistsComponent,
    ViewBlacklistModalComponent, CreateOrEditBlacklistModalComponent,
    ViewCaseDetailsComponent,
    WhitelistsComponent, CreateOrEditWhitelistModalComponent,
    CasesComponent,
    //UploadModuleComponent,
    ViewCaseModalComponent, CreateOrEditCaseModalComponent,
    UserLookupTableModalComponent,
    RuleLookupTableModalComponent,
    DataPointLookupTableModalComponent,
    RulesComponent,
    ViewRuleModalComponent, CreateOrEditRuleModalComponent,
    PayloadPropertiesComponent,
    ViewPayloadPropertyModalComponent, CreateOrEditPayloadPropertyModalComponent,
    PayloadLookupTableModalComponent,
    PayloadsComponent,
    CreateOrEditPayloadModalComponent,
    TransactionTypeLookupTableModalComponent,
    TransactionTypesComponent,
    ViewTransactionTypeModalComponent, CreateOrEditTransactionTypeModalComponent,
    ChannelLookupTableModalComponent,
    ChannelsComponent,
    CreateOrEditChannelModalComponent, CreateOrEditDataPointModalComponent,
    DashboardComponent
  ],
  providers: [
    { provide: BsDatepickerConfig, useFactory: NgxBootstrapDatePickerConfigService.getDatepickerConfig },
    { provide: BsDaterangepickerConfig, useFactory: NgxBootstrapDatePickerConfigService.getDaterangepickerConfig },
    { provide: BsLocaleService, useFactory: NgxBootstrapDatePickerConfigService.getDatepickerLocale }
  ]
})
export class MainModule { }
