import { Component, ViewChild, Injector, Output, EventEmitter, ViewEncapsulation} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import {PayloadPropertiesServiceProxy, DataPointLookupTableDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { Table } from 'primeng/components/table/table';
import { Paginator } from 'primeng/components/paginator/paginator';
import { LazyLoadEvent } from 'primeng/components/common/lazyloadevent';

@Component({
    selector: 'dataPointLookupTableModal',
    styleUrls: ['./dataPoint-lookup-table-modal.component.less'],
    encapsulation: ViewEncapsulation.None,
    templateUrl: './dataPoint-lookup-table-modal.component.html'
})
export class DataPointLookupTableModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;
    @ViewChild('dataTable') dataTable: Table;
    @ViewChild('paginator') paginator: Paginator;

    filterText = '';
    id: string;
    displayName: string;
    dataType: number;
    transactionType = '';
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active = false;
    saving = false;

    constructor(
        injector: Injector,
        private _payloadPropertiesServiceProxy: PayloadPropertiesServiceProxy
    ) {
        super(injector);
    }

    show(transactionTypeId  : string): void {
        this.active = true;
        this.paginator.rows = 5;
        this.getAll();
        this.transactionType = transactionTypeId;
        this.modal.show();
    }

    getAll(event?: LazyLoadEvent) {
        if (!this.active) {
            return;
        }

        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._payloadPropertiesServiceProxy.getAllDataPointForLookupTable(
            this.filterText, this.transactionType,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    setAndSave(dataPoint: DataPointLookupTableDto) {
        this.id = dataPoint.id;
        this.displayName = dataPoint.displayName;
        this.dataType = dataPoint.dataType;
        console.log(this.dataType);
        this.active = false;
        this.modal.hide();
        this.modalSave.emit(null);
    }

    close(): void {
        this.active = false;
        this.modal.hide();
        this.modalSave.emit(null);
    }
}
