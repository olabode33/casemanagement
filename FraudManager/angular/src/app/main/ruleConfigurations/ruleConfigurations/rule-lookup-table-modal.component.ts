import { Component, ViewChild, Injector, Output, EventEmitter, ViewEncapsulation} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import {RuleConfigurationsServiceProxy, RuleLookupTableDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { Table } from 'primeng/components/table/table';
import { Paginator } from 'primeng/components/paginator/paginator';
import { LazyLoadEvent } from 'primeng/components/common/lazyloadevent';

@Component({
    selector: 'ruleLookupTableModal',
    styleUrls: ['./rule-lookup-table-modal.component.less'],
    encapsulation: ViewEncapsulation.None,
    templateUrl: './rule-lookup-table-modal.component.html'
})
export class RuleLookupTableModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;
    @ViewChild('dataTable') dataTable: Table;
    @ViewChild('paginator') paginator: Paginator;

    filterText = '';
    id: string;
    displayName: string;
    transactionTypeId = '';
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active = false;
    saving = false;

    constructor(
        injector: Injector,
        private _ruleConfigurationsServiceProxy: RuleConfigurationsServiceProxy
    ) {
        super(injector);
    }

    show(transactionTypeId  : string): void {
        this.active = true;
        this.paginator.rows = 10;
        this.transactionTypeId = transactionTypeId;
        this.getAll();

        this.modal.show();
    }

    getAll(event?: LazyLoadEvent) {
        if (!this.active) {
            return;
        }

        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        if (this.transactionTypeId == '') {
            this.transactionTypeId = null; 
        }

        this._ruleConfigurationsServiceProxy.getAllRuleForLookupTable(
            this.filterText, this.transactionTypeId,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    setAndSave(rule: RuleLookupTableDto) {
        this.id = rule.id;
        this.displayName = rule.displayName;
        this.active = false;
        this.modal.hide();
        this.modalSave.emit(null);
    }

    close(): void {
        this.active = false;
        this.modal.hide();
        this.modalSave.emit(null);
    }
}
