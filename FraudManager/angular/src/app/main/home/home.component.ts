import { AppComponentBase } from '@shared/common/app-component-base';
import { Component, OnInit, ViewEncapsulation, Injector, ViewChild } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { TenantDashboardServiceProxy, GetCaseDashboardOutputDto, GetRecentCasesDashboardOutputDto, GetAssignedTaskDashboardOutputDto } from '@shared/service-proxies/service-proxies';
import { Router, ActivatedRoute } from '@angular/router';
import { CompleteTaskModalComponent } from '../cases/_subs/complete-task-modal/complete-task-modal.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  encapsulation: ViewEncapsulation.None,
  animations: [appModuleAnimation()]
})
export class HomeComponent extends AppComponentBase implements OnInit {
  @ViewChild('completeTaskModal') completeTaskModal: CompleteTaskModalComponent;

  caseDashboardStats: GetCaseDashboardOutputDto = new GetCaseDashboardOutputDto();
  tasks: GetAssignedTaskDashboardOutputDto[] = new Array();
  recentCases = [];
  recentCase = {
    id: '',
    customerName: '',
    timeAgo: '',
    description: '',
    status: '',
  };

  loading = false;

  constructor(
    injector: Injector,
    private _dashboardServiceProxy: TenantDashboardServiceProxy,
    private _router: Router,
    private _activatedRoute: ActivatedRoute
  ) {
    super(injector);
  }

  ngOnInit() {
    this.getDashboardStats();
    this.completeTaskModal.setShowGoToCaseButton(true);
  }

  getDashboardStats(): void {
    this.loading = true;
    this._dashboardServiceProxy.getCaseDashboardStats().subscribe(result => {
      this.caseDashboardStats = result;
      this.tasks = result.tasks;
      this.recentCases = [];

      result.recentCases.forEach(element => {
        let item = {
          id: element.caseId,
          customerName: element.customerName,
          timeAgo: element.timeSpan,
          description: this.generateRecentCaseDescription(element),
          status: element.status,
        };

        this.recentCases.push(item);
      });

      this.loading = false;
    });
  }

  generateRecentCaseDescription(item: GetRecentCasesDashboardOutputDto): string {
    return '<div class="row"><div class="col">Detection Source: <i>' + item.detectionSource + '</i></div><div class="col">Transaction Type: <i>' + item.transactionType + '</i></div><div class="col">Transaction Amount: <i>NGN ' + item.transactionAmout.toLocaleString().toString() + '</i></div></div>';
  }

  getRecentCaseStatusClass(status: string): string {
    switch (status) {
      case 'Verifying':
        return 'warning';
      case 'Open':
        return 'success';
      default:
        return 'default';
    }
  }

  getStatusClass(status: number): string {
    switch (status) {
      case 2:
        return 'warning';
      case 0:
        return 'success';
      default:
        return 'default';
    }
  }


  getTaskPriorityClass(priority: string): string {
    switch (priority) {
      case 'High':
        return 'danger';
      case 'Medium':
        return 'warning';
      case 'Low':
        return 'success';
      default:
        return 'default';
    }
  }

  completeTask(taskId: number): void {
    this.completeTaskModal.show(taskId);
  }

  viewCase(caseId: string): void {
    this._router.navigate(['../cases/cases/view', caseId], { relativeTo: this._activatedRoute});
  }

}
