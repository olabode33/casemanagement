import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { finalize } from 'rxjs/operators';
import { WhitelistsServiceProxy, CreateOrEditWhitelistDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { TransactionTypeLookupTableModalComponent } from '@app/main/payloads/payloads/transactionType-lookup-table-modal.component';
import { UserLookupTableModalComponent } from '@app/main/ruleEscalations/ruleEscalations/user-lookup-table-modal.component';



@Component({
    selector: 'createOrEditWhitelistModal',
    templateUrl: './create-or-edit-whitelist-modal.component.html'
})
export class CreateOrEditWhitelistModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;
    @ViewChild('transactionTypeLookupTableModal') transactionTypeLookupTableModal: TransactionTypeLookupTableModalComponent;
    @ViewChild('userLookupTableModal') userLookupTableModal: UserLookupTableModalComponent;


    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    whitelist: CreateOrEditWhitelistDto = new CreateOrEditWhitelistDto();

            approvalDate: Date;
    transactionTypeName = '';
    userName = '';


    constructor(
        injector: Injector,
        private _whitelistsServiceProxy: WhitelistsServiceProxy
    ) {
        super(injector);
    }

    show(whitelistId?: string): void {
this.approvalDate = null;

        if (!whitelistId) {
            this.whitelist = new CreateOrEditWhitelistDto();
            this.whitelist.id = whitelistId;
            this.transactionTypeName = '';
            this.userName = '';

            this.active = true;
            this.modal.show();
        } else {
            this._whitelistsServiceProxy.getWhitelistForEdit(whitelistId).subscribe(result => {
                this.whitelist = result.whitelist;

                if (this.whitelist.approvalDate) {
					this.approvalDate = this.whitelist.approvalDate.toDate();
                }
                this.transactionTypeName = result.transactionTypeName;
                this.userName = result.userName;

                this.active = true;
                this.modal.show();
            });
        }
    }

    save(): void {
            this.saving = true;
			
        if (this.approvalDate) {
            if (!this.whitelist.approvalDate) {
                this.whitelist.approvalDate = moment(this.approvalDate).startOf('day');
            }
            else {
                this.whitelist.approvalDate = moment(this.approvalDate);
            }
        }
        else {
            this.whitelist.approvalDate = null;
        }
            this._whitelistsServiceProxy.createOrEdit(this.whitelist)
             .pipe(finalize(() => { this.saving = false; }))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
             });
    }

        openSelectTransactionTypeModal() {
        this.transactionTypeLookupTableModal.id = this.whitelist.transactionTypeId;
        this.transactionTypeLookupTableModal.displayName = this.transactionTypeName;
        this.transactionTypeLookupTableModal.show();
    }
        openSelectUserModal() {
        this.userLookupTableModal.id = this.whitelist.approverUserId;
        this.userLookupTableModal.displayName = this.userName;
        this.userLookupTableModal.show();
    }


        setTransactionTypeIdNull() {
        this.whitelist.transactionTypeId = null;
        this.transactionTypeName = '';
    }
        setApproverUserIdNull() {
        this.whitelist.approverUserId = null;
        this.userName = '';
    }


        getNewTransactionTypeId() {
        this.whitelist.transactionTypeId = this.transactionTypeLookupTableModal.id;
        this.transactionTypeName = this.transactionTypeLookupTableModal.displayName;
    }
        getNewApproverUserId() {
        this.whitelist.approverUserId = this.userLookupTableModal.id;
        this.userName = this.userLookupTableModal.displayName;
    }


    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
