import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BlacklistsComponent } from './blacklists/blacklists/blacklists.component';
import { WhitelistsComponent } from './whitelists/whitelists/whitelists.component';
import { CasesComponent } from './cases/cases/cases.component';
import { RulesComponent } from './rules/rules/rules.component';
import { PayloadPropertiesComponent } from './payloadProperties/payloadProperties/payloadProperties.component';
import { PayloadsComponent } from './payloads/payloads/payloads.component';
import { TransactionTypesComponent } from './transactionTypes/transactionTypes/transactionTypes.component';
import { ChannelsComponent } from './channels/channels/channels.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ViewCaseDetailsComponent } from './cases/cases/view-case-details.component';
import { HomeComponent } from '@app/main/home/home.component';
import { CreateEditRuleComponent } from './rules/_subs/create-edit-rule/create-edit-rule.component';
import { ViewRuleComponent } from './rules/_subs/view-rule/view-rule.component';


@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                children: [
                    { path: 'blacklists/blacklists', component: BlacklistsComponent, data: { permission: 'Pages.Blacklists' }  },
                    { path: 'whitelists/whitelists', component: WhitelistsComponent, data: { permission: 'Pages.Whitelists' }  },
                    { path: 'cases/cases', component: CasesComponent, data: { permission: 'Pages.Cases' }  },
                    { path: 'rules/rules', component: RulesComponent, data: { permission: 'Pages.Rules' }  },
                    { path: 'payloadProperties/payloadProperties', component: PayloadPropertiesComponent, data: { permission: 'Pages.PayloadProperties' }  },
                    { path: 'payloads/payloads', component: PayloadsComponent, data: { permission: 'Pages.Payloads' }  },
                    { path: 'transactionTypes/transactionTypes', component: TransactionTypesComponent, data: { permission: 'Pages.TransactionTypes' }  },
                    { path: 'channels/channels', component: ChannelsComponent, data: { permission: 'Pages.Channels' }  },
                    { path: 'dashboard', component: DashboardComponent, data: { permission: 'Pages.Tenant.Dashboard' } },
                    { path: 'cases/cases/view/:caseId', component: ViewCaseDetailsComponent, data: { permission: 'Pages.Tenant.Dashboard' } },
                    { path: 'home', component: HomeComponent, data: { permission: 'Pages.Tenant.Dashboard' } },
                    { path: 'rules/create', component: CreateEditRuleComponent, data: { permission: 'Pages.Rules' }  },
                    { path: 'rules/edit/:ruleId', component: CreateEditRuleComponent, data: { permission: 'Pages.Rules' }  },
                    { path: 'rules/view/:ruleId', component: ViewRuleComponent, data: { permission: 'Pages.Rules' }  }

                ]
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class MainRoutingModule { }
