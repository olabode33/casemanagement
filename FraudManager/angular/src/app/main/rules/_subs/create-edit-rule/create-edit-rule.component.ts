import { Component, Injector, ViewEncapsulation, ViewChild, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Http } from '@angular/http';
import { Location } from '@angular/common';
import { RulesServiceProxy, CreateOrEditRuleDto, CreateOrEditRuleConfigurationDto, CreateOrEditRuleEscalationDto, CommonLookupServiceProxy, NameValueDto, RuleObjGroup, RuleConfigurationForRuleEditDto } from '@shared/service-proxies/service-proxies';
import { NotifyService } from '@abp/notify/notify.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/components/table/table';
import { Paginator } from 'primeng/components/paginator/paginator';
import { LazyLoadEvent } from 'primeng/components/common/lazyloadevent';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { EntityTypeHistoryModalComponent } from '@app/shared/common/entityHistory/entity-type-history-modal.component';
import * as _ from 'lodash';
import * as moment from 'moment';
import { DataPointLookupTableModalComponent } from '@app/main/ruleConfigurations/ruleConfigurations/dataPoint-lookup-table-modal.component';
import { UserLookupTableModalComponent } from '@app/main/ruleEscalations/ruleEscalations/user-lookup-table-modal.component';

@Component({
  selector: 'app-create-edit-rule',
  templateUrl: './create-edit-rule.component.html',
  styleUrls: ['./create-edit-rule.component.css'],
  encapsulation: ViewEncapsulation.None,
  animations: [appModuleAnimation()]
})
export class CreateEditRuleComponent extends AppComponentBase implements OnInit {

  @ViewChild('dataPointLookupTableModal') dataPointLookupTableModal: DataPointLookupTableModalComponent;
  @ViewChild('userLookupTableModal') userLookupTableModal: UserLookupTableModalComponent;

  saving = false;

  rule: CreateOrEditRuleDto = new CreateOrEditRuleDto();

  transactionTypeName = '';
  ruleConfigurations = [];
  ruleEscalations = [];
  lineSet = 0;
  configs: CreateOrEditRuleConfigurationDto[];
  users: CreateOrEditRuleEscalationDto[];
  selectedBaseConnector = 'And';

  item = {
    id: '',
    dataPointId: null,
    dataType: 0,
    connector: null,
    value: null,
    operand: null,
    dataPointName: '',
    ind: 0
  };

  dataPointsDropdown: NameValueDto[] = new Array();
  transactionTypeDropdown: NameValueDto[] = new Array();
  connectors = [
    { value: 'And', name: 'ALL' },
    { value: 'Or', name: 'ANY' }
  ];

  ruleGroups: RuleObjGroup[] = new Array();
  newRuleForGroup = '';

  constructor(
    injector: Injector,
    private _rulesServiceProxy: RulesServiceProxy,
    private _commonLookupServiceProxy: CommonLookupServiceProxy,
    private _notifyService: NotifyService,
    private _tokenAuth: TokenAuthServiceProxy,
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _location: Location
  ) {
    super(injector);
    _commonLookupServiceProxy.getAllTransactionTypeForLookupTable('', '', 0, 10000).subscribe(result => {
      this.transactionTypeDropdown = result.items;
    });
    _commonLookupServiceProxy.getAllDataPointForLookupTable('', '', 0, 10000).subscribe(result => {
      this.dataPointsDropdown = result.items;
    });
  }

  ngOnInit() {
    this._activatedRoute.paramMap.subscribe(params => {
      let ruleId = params.get('ruleId');
      this.show(ruleId);
    });
  }

  show(ruleId?: string): void {

    if (!ruleId) {
      this.rule = new CreateOrEditRuleDto();
      this.rule.id = ruleId;
      this.transactionTypeName = '';

      this.ruleConfigurations.splice(0, this.ruleConfigurations.length);
      this.ruleEscalations.splice(0, this.ruleEscalations.length);
      this.ruleGroups = new Array();

    } else {
      this._rulesServiceProxy.getRuleForEdit(ruleId).subscribe(result => {
        this.rule = result.rule;
        this.transactionTypeName = result.transactionTypeName;
        this.selectedBaseConnector = result.rule.baseConnector;
        this.ruleGroups = result.rule.ruleGroups;
        console.log(this.rule);

        this.ruleEscalations = [];
        for (let i = 0; i < result.ruleEscalations.length; i++) {

          let escalation = {
            id: result.ruleEscalations[i].id,
            userId: result.ruleEscalations[i].userId,
            displayName: result.ruleEscalations[i].userFullName,
            ind: i
          };

          this.ruleEscalations.push(escalation);
        }

      });
    }
  }

  addLine(subgroup?: RuleObjGroup): void {
    if (this.item.operand == null || this.item.operand === '') {
      this.notify.error('You need to select an operand');
      return;
    }

    if (this.item.value == null || this.item.operand === '') {
      this.notify.error('You need to enter a value');
      return;
    }

    if (this.item.dataPointId == null || this.item.operand === 0) {
      this.notify.error('You need to select a Data Point');
      return;
    }

    let newAddition = {
      dataPointId: this.item.dataPointId,
      connector: this.item.connector,
      value: this.item.value,
      operand: this.item.operand,
      dataPointName: this.item.dataPointName,
      ind: this.ruleConfigurations.length
    };

    if (this.ruleConfigurations.length === 0) {
      newAddition.connector = null;
    }


    if (subgroup) {
      let rule = new RuleConfigurationForRuleEditDto();
      rule.groupName = subgroup.groupName;
      rule.connector = this.item.connector;
      rule.dataPointId = this.item.dataPointId;
      rule.dataPointName = this.item.dataPointName;
      rule.dataPointType = this.item.dataType.toString();
      rule.operand = this.item.operand;
      rule.value = this.item.value;

      subgroup.ruleObjs.push(rule);
    }

    this.item.connector = 'And';
    this.item.dataPointId = null;
    this.item.dataPointName = null;
    this.item.operand = null;
    this.item.value = null;

  }

  addGroup(): void {
    let newGroup = new RuleObjGroup();
    newGroup.groupName = '0' + (this.ruleGroups.length).toString();
    newGroup.connector = this.connectors[0].value;
    newGroup.ruleObjs = new Array();
    this.ruleGroups.push(newGroup);
    console.log(this.ruleGroups);
  }

  addRuleObj(group: RuleObjGroup): void {
    let rule = new RuleConfigurationForRuleEditDto();
    rule.groupName = group.groupName;
    rule.connector = this.connectors[0].value;
    group.ruleObjs.push(rule);
  }

  showRuleAddFor(groupName: string): void {
    this.newRuleForGroup = groupName;
  }

  removeLine(group: RuleObjGroup, line: RuleConfigurationForRuleEditDto): void {
    this.message.confirm(
      'Selected item will be deleted from group',
      (isConfirmed) => {
          if (isConfirmed) {
            let i = group.ruleObjs.findIndex(x => x === line);
            group.ruleObjs.splice(i, 1);
          }
      }
  );
  }

  removeGroup(group: RuleObjGroup): void {
    this.message.confirm(
      'All items for this group will be deleted!',
      (isConfirmed) => {
          if (isConfirmed) {
            let i = this.ruleGroups.findIndex(x => x === group);
            this.ruleGroups.splice(i, 1);
          }
      }
  );
  }

  removeUser(line: number): void {
    this.ruleEscalations.splice(line, 1);
  }

  save(): void {
    this.saving = true;

    if (this.ruleGroups.length === 0) {
      this.notify.error('You need to define at least one rule definition.');
      this.saving = false;
      return;
    }

    this.users = [];

    for (let i = 0; i < this.ruleEscalations.length; i++) {
      let user = new CreateOrEditRuleEscalationDto();
      user.userId = this.ruleEscalations[i].userId;
      this.users.push(user);
    }

    this.rule.ruleEscalations = this.users;

    this.rule.baseConnector = this.selectedBaseConnector;
    this.rule.ruleGroups = this.ruleGroups;
    console.log(this.rule);
    this._rulesServiceProxy.createOrEdit(this.rule)
      .subscribe(() => {
        this.message.success(this.l('Rule successfully saved!.'));
        this.goBack();
      });
  }

  goBack(): void {
    this._location.back();
    // if (this.rule.id) {
    //   this._router.navigate(['../../rules'], { relativeTo: this._activatedRoute });
    // } else {
    //   this._router.navigate(['../rules'], { relativeTo: this._activatedRoute });
    // }
  }

  openSelectDataPointModal(line: number) {
    this.dataPointLookupTableModal.id = this.item.dataPointId;
    this.dataPointLookupTableModal.displayName = this.item.dataPointName;
    this.dataPointLookupTableModal.show(this.rule.transactionTypeId);

  }

  setDataPoint() {
    this.item.dataPointId = this.dataPointLookupTableModal.id;
    this.item.dataPointName = this.dataPointLookupTableModal.displayName;
    this.item.dataType = this.dataPointLookupTableModal.dataType;
  }

  openSelectUserModal() {
    this.userLookupTableModal.show();
  }


  setTransactionTypeIdNull() {
    this.rule.transactionTypeId = null;
    this.transactionTypeName = '';
  }


  getNewUserId() {
    for (let i = 0; i < this.ruleEscalations.length; i++) {
      if (this.ruleEscalations[i].userId === this.userLookupTableModal.id) {
        this.notify.info('You have added this user already.');
        return;
      }
    }

    let item = {
      userId: this.userLookupTableModal.id,
      displayName: this.userLookupTableModal.displayName,
      ind: this.ruleEscalations.length
    };

    this.ruleEscalations.push(item);
  }

}
