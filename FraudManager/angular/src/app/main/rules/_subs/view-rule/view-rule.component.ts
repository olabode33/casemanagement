import { Component, Injector, ViewEncapsulation, ViewChild, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common'
import { Http } from '@angular/http';
import { RulesServiceProxy, CreateOrEditRuleDto, CreateOrEditRuleConfigurationDto, CreateOrEditRuleEscalationDto, CommonLookupServiceProxy, NameValueDto, RuleObjGroup, RuleConfigurationForRuleEditDto, UpdateRuleStatusInputDto, EntityDtoOfGuid } from '@shared/service-proxies/service-proxies';
import { NotifyService } from '@abp/notify/notify.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/components/table/table';
import { Paginator } from 'primeng/components/paginator/paginator';
import { LazyLoadEvent } from 'primeng/components/common/lazyloadevent';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { EntityTypeHistoryModalComponent } from '@app/shared/common/entityHistory/entity-type-history-modal.component';
import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
  selector: 'app-view-rule',
  templateUrl: './view-rule.component.html',
  styleUrls: ['./view-rule.component.css'],
  encapsulation: ViewEncapsulation.None,
  animations: [appModuleAnimation()]
})
export class ViewRuleComponent extends AppComponentBase implements OnInit {

  rule: CreateOrEditRuleDto = new CreateOrEditRuleDto();

  transactionTypeName = '';
  ruleConfigurations = [];
  ruleEscalations = [];
  lineSet = 0;
  configs: CreateOrEditRuleConfigurationDto[];
  users: CreateOrEditRuleEscalationDto[];
  selectedBaseConnector = 'And';

  item = {
    id: '',
    dataPointId: null,
    dataType: 0,
    connector: null,
    value: null,
    operand: null,
    dataPointName: '',
    ind: 0
  };

  dataPointsDropdown: NameValueDto[] = new Array();
  transactionTypeDropdown: NameValueDto[] = new Array();
  connectors = [
    { value: 'And', name: 'ALL' },
    { value: 'Or', name: 'ANY' }
  ];

  ruleGroups: RuleObjGroup[] = new Array();
  newRuleForGroup = '';

  RULE_STATUS_DRAFT = 0;
  RULE_STATUS_SUBMITTED = 1;
  RULE_STATUS_PUBLISHED = 2;

  constructor(
    injector: Injector,
    private _rulesServiceProxy: RulesServiceProxy,
    private _commonLookupServiceProxy: CommonLookupServiceProxy,
    private _notifyService: NotifyService,
    private _tokenAuth: TokenAuthServiceProxy,
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _location: Location
  ) {
    super(injector);
  }

  ngOnInit() {
    this._activatedRoute.paramMap.subscribe(params => {
      let ruleId = params.get('ruleId');
      if (ruleId) {
         this.show(ruleId);
      } else {
        this.goBack();
      }
    });
  }

  show(ruleId: string): void {
    this._rulesServiceProxy.getRuleForEdit(ruleId).subscribe(result => {
      this.rule = result.rule;
      this.transactionTypeName = result.transactionTypeName;
      this.selectedBaseConnector = result.rule.baseConnector;
      this.ruleGroups = result.rule.ruleGroups;
      console.log(this.rule);

      this.ruleEscalations = [];
      for (let i = 0; i < result.ruleEscalations.length; i++) {

        let escalation = {
          id: result.ruleEscalations[i].id,
          userId: result.ruleEscalations[i].userId,
          displayName: result.ruleEscalations[i].userFullName,
          ind: i
        };

        this.ruleEscalations.push(escalation);
      }

    });
  }

  edit(): void {
    this._router.navigate(['../../edit', this.rule.id], { relativeTo: this._activatedRoute });
  }

  goBack(): void {
    this._location.back();
    //this._router.navigate(['../../rules'], { relativeTo: this._activatedRoute });
  }

  updateStatus(status: number): void {
    let newStatus = new UpdateRuleStatusInputDto();
    newStatus.id = this.rule.id;
    newStatus.newStatus = status;

    if (status === this.RULE_STATUS_PUBLISHED) {
      this.message.confirm(
        'Note: This process cannot be reversed!',
        (isConfirmed) => {
          if (isConfirmed) {
            this._rulesServiceProxy.updateStatus(newStatus)
              .subscribe(() => {
                this.reloadPage();
                this.message.success(this.l('EnumRuleStatusUpdate_' + status));
              });
          }
        }
      );
    } else {
      this._rulesServiceProxy.updateStatus(newStatus)
        .subscribe(() => {
          this.reloadPage();
          this.message.success(this.l('EnumRuleStatusUpdate_' + status));
        });
    }
  }

  reloadPage() {
    this.show(this.rule.id);
  }

  toogleActvation(): void {
    let ruleGuid = new EntityDtoOfGuid();
    ruleGuid.id = this.rule.id;
    this._rulesServiceProxy.toggleActiveStatus(ruleGuid)
      .subscribe(() => {
        this.reloadPage();
        if (!this.rule.active) {
          this.message.success('Rule activated successfully');
        } else {
          this.message.info('Rule deactivated successfully');
        }
      });
  }

  getRuleStatusBadgeCSSClass(status: number): string {
    switch (status) {
        case 1:
            return 'warning';
        case 2:
            return 'success';
        default:
            return 'default';
    }
}

}
