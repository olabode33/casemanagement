import { Component, Injector, ViewEncapsulation, ViewChild, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Http } from '@angular/http';
import { RulesServiceProxy, RuleDto, RuleDtoSeverity, RuleDtoAction, UpdateRuleStatusInputDto, EntityDtoOfGuid, RuleDtoRuleStatus } from '@shared/service-proxies/service-proxies';
import { NotifyService } from '@abp/notify/notify.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { CreateOrEditRuleModalComponent } from './create-or-edit-rule-modal.component';
import { ViewRuleModalComponent } from './view-rule-modal.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/components/table/table';
import { Paginator } from 'primeng/components/paginator/paginator';
import { LazyLoadEvent } from 'primeng/components/common/lazyloadevent';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { EntityTypeHistoryModalComponent } from '@app/shared/common/entityHistory/entity-type-history-modal.component';
import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
    templateUrl: './rules.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class RulesComponent extends AppComponentBase implements OnInit {

    @ViewChild('createOrEditRuleModal') createOrEditRuleModal: CreateOrEditRuleModalComponent;
    @ViewChild('viewRuleModalComponent') viewRuleModal: ViewRuleModalComponent;
    @ViewChild('entityTypeHistoryModal') entityTypeHistoryModal: EntityTypeHistoryModalComponent;
    @ViewChild('dataTable') dataTable: Table;
    @ViewChild('paginator') paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = '';
    nameFilter = '';
    severityFilter = -1;
    actionFilter = -1;
    transactionTypeNameFilter = '';
    approvalStatusFilter = -1;

    severity = RuleDtoSeverity;
    actions = RuleDtoAction;
    RULE_STATUS_DRAFT = 0;
    RULE_STATUS_SUBMITTED = 1;
    RULE_STATUS_PUBLISHED = 2;
    RuleStatusDropdown = [
        { value: -1, name: 'All'},
        { value: this.RULE_STATUS_DRAFT, name: 'Draft' },
        { value: this.RULE_STATUS_SUBMITTED, name: 'Submitted' },
        { value: this.RULE_STATUS_PUBLISHED, name: 'Published' }
    ];

    _entityTypeFullName = 'Test.Rules.Rule';
    entityHistoryEnabled = false;

    constructor(
        injector: Injector,
        private _rulesServiceProxy: RulesServiceProxy,
        private _router: Router,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.entityHistoryEnabled = this.setIsEntityHistoryEnabled();
    }

    private setIsEntityHistoryEnabled(): boolean {
        let customSettings = (abp as any).custom;
        return customSettings.EntityHistory && customSettings.EntityHistory.isEnabled && _.filter(customSettings.EntityHistory.enabledEntities, entityType => entityType === this._entityTypeFullName).length === 1;
    }

    getRules(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._rulesServiceProxy.getAll(
            this.filterText,
            this.nameFilter,
            this.severityFilter,
            this.actionFilter,
            this.transactionTypeNameFilter,
            this.approvalStatusFilter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createRule(): void {
        this._router.navigate(['../create'], { relativeTo: this._activatedRoute });
        //this.createOrEditRuleModal.show();
    }

    editRule(ruleId: string): void {
        this._router.navigate(['../edit', ruleId], { relativeTo: this._activatedRoute });
        //this.createOrEditRuleModal.show();
    }

    viewRule(ruleId: string): void {
        this._router.navigate(['../view', ruleId], { relativeTo: this._activatedRoute });
        //this.createOrEditRuleModal.show();
    }

    showHistory(rule: RuleDto): void {
        this.entityTypeHistoryModal.show({
            entityId: rule.id.toString(),
            entityTypeFullName: this._entityTypeFullName,
            entityTypeDescription: ''
        });
    }

    deleteRule(rule: RuleDto): void {
        this.message.confirm(
            '',
            (isConfirmed) => {
                if (isConfirmed) {
                    this._rulesServiceProxy.delete(rule.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.message.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }

    updateStatus(ruleId: string, status: number): void {
        let newStatus = new UpdateRuleStatusInputDto();
        newStatus.id = ruleId;
        newStatus.newStatus = status;

        if (status === this.RULE_STATUS_PUBLISHED) {
            this.message.confirm(
                'Note: This process cannot be reversed!',
                (isConfirmed) => {
                    if (isConfirmed) {
                        this._rulesServiceProxy.updateStatus(newStatus)
                            .subscribe(() => {
                                this.reloadPage();
                                this.message.success(this.l('EnumRuleStatusUpdate_' + status));
                            });
                    }
                }
            );
        } else {
            this._rulesServiceProxy.updateStatus(newStatus)
                .subscribe(() => {
                    this.reloadPage();
                    this.message.success(this.l('EnumRuleStatusUpdate_' + status));
                });
        }
    }

    toogleActvation(rule: RuleDto): void {
        let ruleGuid = new EntityDtoOfGuid();
        ruleGuid.id = rule.id;
        this._rulesServiceProxy.toggleActiveStatus(ruleGuid)
            .subscribe(() => {
                this.reloadPage();
                if (!rule.active) {
                    this.message.success('Rule activated successfully');
                } else {
                    this.message.info('Rule deactivated successfully');
                }
            });
    }

    exportToExcel(): void {
        this._rulesServiceProxy.getRulesToExcel(
            this.filterText,
            this.nameFilter,
            this.severityFilter,
            this.actionFilter,
            this.transactionTypeNameFilter,
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    getRuleStatusBadgeCSSClass(status: number): string {
        switch (status) {
            case 1:
                return 'warning';
            case 2:
                return 'success';
            default:
                return 'default';
        }
    }

    makeCopy(rule: RuleDto): void {
        let ruleGuid = new EntityDtoOfGuid();
        ruleGuid.id = rule.id;
        this.message.confirm(
            'Make a copy of this rule',
            (isConfirmed) => {
                if (isConfirmed) {
                    this._rulesServiceProxy.makeCopy(ruleGuid)
                        .subscribe(() => {
                            this.reloadPage();
                            this.message.success('Rule successfully cloned!');
                        });
                }
            }
        );

    }
}
