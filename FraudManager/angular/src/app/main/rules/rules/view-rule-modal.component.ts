import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { GetRuleForView, RuleDto , RuleDtoSeverity, RuleDtoAction} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'viewRuleModal',
    templateUrl: './view-rule-modal.component.html'
})
export class ViewRuleModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item: GetRuleForView;
    severity = RuleDtoSeverity;
    actions = RuleDtoAction;


    constructor(
        injector: Injector
    ) {
        super(injector);
        this.item = new GetRuleForView();
        this.item.rule = new RuleDto();
    }

    show(item: GetRuleForView): void {
        this.item = item;
        this.active = true;
        this.modal.show();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
