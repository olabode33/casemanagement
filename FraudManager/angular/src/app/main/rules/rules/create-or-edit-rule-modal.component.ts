import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { finalize } from 'rxjs/operators';
import { RulesServiceProxy, CreateOrEditRuleDto, CreateOrEditRuleConfigurationDto, CreateOrEditRuleEscalationDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { TransactionTypeLookupTableModalComponent } from '@app/main/payloads/payloads/transactionType-lookup-table-modal.component';
import { UserLookupTableModalComponent } from '@app/main/ruleEscalations/ruleEscalations/user-lookup-table-modal.component';

import { DataPointLookupTableModalComponent } from '@app/main/ruleConfigurations/ruleConfigurations/dataPoint-lookup-table-modal.component';

@Component({
    selector: 'createOrEditRuleModal',
    templateUrl: './create-or-edit-rule-modal.component.html'
})
export class CreateOrEditRuleModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;
    @ViewChild('transactionTypeLookupTableModal') transactionTypeLookupTableModal: TransactionTypeLookupTableModalComponent;
    @ViewChild('dataPointLookupTableModal') dataPointLookupTableModal: DataPointLookupTableModalComponent;
    @ViewChild('userLookupTableModal') userLookupTableModal: UserLookupTableModalComponent;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    rule: CreateOrEditRuleDto = new CreateOrEditRuleDto();

    transactionTypeName = '';
    ruleConfigurations = [];
    ruleEscalations = [];
    lineSet = 0;
    configs: CreateOrEditRuleConfigurationDto[];
    users: CreateOrEditRuleEscalationDto[];

    item = {
        id: '',
        dataPointId: null,
        dataType: 0,
        connector: null,
        value: null,
        operand: null,
        dataPointName: '',
        ind: 0
    };

    constructor(
        injector: Injector,
        private _rulesServiceProxy: RulesServiceProxy
    ) {
        super(injector);
    }

    show(ruleId?: string): void {

        if (!ruleId) {
            this.rule = new CreateOrEditRuleDto();
            this.rule.id = ruleId;
            this.transactionTypeName = '';

            this.active = true;

            this.ruleConfigurations.splice(0, this.ruleConfigurations.length);
            this.ruleEscalations.splice(0, this.ruleEscalations.length);
            this.modal.show();

        } else {
            this._rulesServiceProxy.getRuleForEdit(ruleId).subscribe(result => {
                this.rule = result.rule;

                this.transactionTypeName = result.transactionTypeName;
                this.ruleConfigurations = [];
                for (var i = 0; i < result.ruleConfigurations.length; i++) {

                    var item = {
                        id: result.ruleConfigurations[i].id,
                        dataPointId: result.ruleConfigurations[i].dataPointId,
                        connector: result.ruleConfigurations[i].connector,
                        value: result.ruleConfigurations[i].value,
                        operand: result.ruleConfigurations[i].operand,
                        dataPointName: result.ruleConfigurations[i].dataPointName,
                        ind: i
                    };

                    this.ruleConfigurations.push(item);
                    this.item.connector = 'And';
                }


                this.ruleEscalations = [];
                for (var i = 0; i < result.ruleEscalations.length; i++) {

                    var escalation = {
                        id: result.ruleEscalations[i].id,
                        userId: result.ruleEscalations[i].userId,
                        displayName: result.ruleEscalations[i].userFullName,
                        ind: i
                    };

                    this.ruleEscalations.push(escalation);
                }

                this.active = true;
                this.modal.show();
            });
        }
    }

    addLine(): void {
        if (this.item.operand == null || this.item.operand == '') {
            this.notify.error('You need to select an operand');
            return;
        }

        if (this.item.value == null || this.item.operand == '') {
            this.notify.error('You need to enter a value');
            return;
        }

        if (this.item.dataPointId == null || this.item.operand == 0) {
            this.notify.error('You need to select a Data Point');
            return;
        }

        var newAddition = {
            dataPointId: this.item.dataPointId,
            connector: this.item.connector,
            value: this.item.value,
            operand: this.item.operand,
            dataPointName: this.item.dataPointName,
            ind: this.ruleConfigurations.length
        }

        if (this.ruleConfigurations.length == 0) {
            newAddition.connector = null;
        }

        this.ruleConfigurations.push(newAddition);

        this.item.connector = 'And';
        this.item.dataPointId = null;
        this.item.dataPointName = null;
        this.item.operand = null;
        this.item.value = null;

    }

    removeLine(line: number): void {
        this.ruleConfigurations.pop();

        if (this.ruleConfigurations.length > 0) {
            this.item.connector = 'And';
        }
    }

    removeUser(line: number): void {
        this.ruleEscalations.splice(line, 1);
    }

    save(): void {
        this.saving = true;

        if (this.ruleConfigurations.length == 0) {
            this.notify.error('You need to define at least one rule definition.');
            return;
        }
        this.configs = [];
        for (var i = 0; i < this.ruleConfigurations.length; i++) {
            var item = new CreateOrEditRuleConfigurationDto();
            item.connector = this.ruleConfigurations[i].connector;
            item.dataPointId = this.ruleConfigurations[i].dataPointId;
            item.operand = this.ruleConfigurations[i].operand;
            item.value = this.ruleConfigurations[i].value;

            this.configs.push(item);
        }

        this.rule.ruleConfigurations = this.configs;
        this.users= [];

        for (var i = 0; i < this.ruleEscalations.length; i++) {
            var user = new CreateOrEditRuleEscalationDto();
            user.userId = this.ruleEscalations[i].userId;
            this.users.push(user);
        }

        this.rule.ruleEscalations = this.users;

        console.log(this.rule);
        this._rulesServiceProxy.createOrEdit(this.rule)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.info(this.l('Your action has been processed successfulyy.'));
                this.close();
                this.modalSave.emit(null);
            });
    }

    openSelectTransactionTypeModal() {
        this.transactionTypeLookupTableModal.id = this.rule.transactionTypeId;
        this.transactionTypeLookupTableModal.displayName = this.transactionTypeName;
        this.transactionTypeLookupTableModal.show();
    }

    openSelectUserModal() {
        this.userLookupTableModal.show();
    }


    openSelectDataPointModal(line: number) {
        this.dataPointLookupTableModal.id = this.item.dataPointId;
        this.dataPointLookupTableModal.displayName = this.item.dataPointName;
        this.dataPointLookupTableModal.show(this.rule.transactionTypeId);

    }


    setTransactionTypeIdNull() {
        this.rule.transactionTypeId = null;
        this.transactionTypeName = '';
    }


    getNewUserId() {

        for (var i = 0; i < this.ruleEscalations.length; i++) {
            if (this.ruleEscalations[i].userId == this.transactionTypeLookupTableModal.id) {
                this.notify.info("You have added this user already.");
                return;
            }
        }

        var item = {
            userId: this.userLookupTableModal.id,
            displayName: this.userLookupTableModal.displayName,
            ind: this.ruleEscalations.length
        }

        this.ruleEscalations.push(item);
    }


    getNewTransactionTypeId() {
        this.rule.transactionTypeId = this.transactionTypeLookupTableModal.id;
        this.transactionTypeName = this.transactionTypeLookupTableModal.displayName;
    }


    setDataPoint() {
        this.item.dataPointId = this.dataPointLookupTableModal.id;
        this.item.dataPointName = this.dataPointLookupTableModal.displayName;
        this.item.dataType = this.dataPointLookupTableModal.dataType;
    }


    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
