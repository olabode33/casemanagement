import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { finalize } from 'rxjs/operators';
import { TransactionTypesServiceProxy, CreateOrEditTransactionTypeDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { ChannelLookupTableModalComponent } from './channel-lookup-table-modal.component';


@Component({
    selector: 'createOrEditTransactionTypeModal',
    templateUrl: './create-or-edit-transactionType-modal.component.html'
})
export class CreateOrEditTransactionTypeModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;
    @ViewChild('channelLookupTableModal') channelLookupTableModal: ChannelLookupTableModalComponent;


    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    transactionType: CreateOrEditTransactionTypeDto = new CreateOrEditTransactionTypeDto();

    channelName = '';


    constructor(
        injector: Injector,
        private _transactionTypesServiceProxy: TransactionTypesServiceProxy
    ) {
        super(injector);
    }

    show(transactionTypeId?: string): void {

        if (!transactionTypeId) {
            this.transactionType = new CreateOrEditTransactionTypeDto();
            this.transactionType.id = transactionTypeId;
            this.channelName = '';

            this.active = true;
            this.modal.show();
        } else {
            this._transactionTypesServiceProxy.getTransactionTypeForEdit(transactionTypeId).subscribe(result => {
                this.transactionType = result.transactionType;

                this.channelName = result.channelName;

                this.active = true;
                this.modal.show();
            });
        }
    }

    save(): void {
            this.saving = true;
			
            this._transactionTypesServiceProxy.createOrEdit(this.transactionType)
             .pipe(finalize(() => { this.saving = false; }))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
             });
    }

        openSelectChannelModal() {
        this.channelLookupTableModal.id = this.transactionType.channelId;
        this.channelLookupTableModal.displayName = this.channelName;
        this.channelLookupTableModal.show();
    }


        setChannelIdNull() {
        this.transactionType.channelId = null;
        this.channelName = '';
    }


        getNewChannelId() {
        this.transactionType.channelId = this.channelLookupTableModal.id;
        this.channelName = this.channelLookupTableModal.displayName;
    }


    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
