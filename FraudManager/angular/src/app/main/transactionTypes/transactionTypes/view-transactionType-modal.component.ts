import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { GetTransactionTypeForView, TransactionTypeDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'viewTransactionTypeModal',
    templateUrl: './view-transactionType-modal.component.html'
})
export class ViewTransactionTypeModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item: GetTransactionTypeForView;


    constructor(
        injector: Injector
    ) {
        super(injector);
        this.item = new GetTransactionTypeForView();
        this.item.transactionType = new TransactionTypeDto();
    }

    show(item: GetTransactionTypeForView): void {
        this.item = item;
        this.active = true;
        this.modal.show();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
