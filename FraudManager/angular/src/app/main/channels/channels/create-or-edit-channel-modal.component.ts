import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { finalize } from 'rxjs/operators';
import { ChannelsServiceProxy, CreateOrEditChannelDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';


@Component({
    selector: 'createOrEditChannelModal',
    templateUrl: './create-or-edit-channel-modal.component.html'
})
export class CreateOrEditChannelModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;


    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    channel: CreateOrEditChannelDto = new CreateOrEditChannelDto();



    constructor(
        injector: Injector,
        private _channelsServiceProxy: ChannelsServiceProxy
    ) {
        super(injector);
    }

    show(channelId?: string): void {

        if (!channelId) {
            this.channel = new CreateOrEditChannelDto();
            this.channel.id = channelId;

            this.active = true;
            this.modal.show();
        } else {
            this._channelsServiceProxy.getChannelForEdit(channelId).subscribe(result => {
                this.channel = result.channel;


                this.active = true;
                this.modal.show();
            });
        }
    }

    save(): void {
            this.saving = true;
			
            this._channelsServiceProxy.createOrEdit(this.channel)
             .pipe(finalize(() => { this.saving = false; }))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
             });
    }







    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
