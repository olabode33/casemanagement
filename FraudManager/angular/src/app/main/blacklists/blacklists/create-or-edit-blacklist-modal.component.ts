import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { finalize } from 'rxjs/operators';
import { BlacklistsServiceProxy, CreateOrEditBlacklistDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { TransactionTypeLookupTableModalComponent } from '@app/main/payloads/payloads/transactionType-lookup-table-modal.component';
import { UserLookupTableModalComponent } from '@app/main/ruleEscalations/ruleEscalations/user-lookup-table-modal.component';


@Component({
    selector: 'createOrEditBlacklistModal',
    templateUrl: './create-or-edit-blacklist-modal.component.html'
})
export class CreateOrEditBlacklistModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;
    @ViewChild('transactionTypeLookupTableModal') transactionTypeLookupTableModal: TransactionTypeLookupTableModalComponent;
    @ViewChild('userLookupTableModal') userLookupTableModal: UserLookupTableModalComponent;


    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    blacklist: CreateOrEditBlacklistDto = new CreateOrEditBlacklistDto();

            approvalDate: Date;
    transactionTypeName = '';
    userName = '';


    constructor(
        injector: Injector,
        private _blacklistsServiceProxy: BlacklistsServiceProxy
    ) {
        super(injector);
    }

    show(blacklistId?: string): void {
this.approvalDate = null;

        if (!blacklistId) {
            this.blacklist = new CreateOrEditBlacklistDto();
            this.blacklist.id = blacklistId;
            this.transactionTypeName = '';
            this.userName = '';

            this.active = true;
            this.modal.show();
        } else {
            this._blacklistsServiceProxy.getBlacklistForEdit(blacklistId).subscribe(result => {
                this.blacklist = result.blacklist;

                if (this.blacklist.approvalDate) {
					this.approvalDate = this.blacklist.approvalDate.toDate();
                }
                this.transactionTypeName = result.transactionTypeName;
                this.userName = result.userName;

                this.active = true;
                this.modal.show();
            });
        }
    }

    save(): void {
            this.saving = true;

			
        if (this.approvalDate) {
            if (!this.blacklist.approvalDate) {
                this.blacklist.approvalDate = moment(this.approvalDate).startOf('day');
            }
            else {
                this.blacklist.approvalDate = moment(this.approvalDate);
            }
        }
        else {
            this.blacklist.approvalDate = null;
        }
            this._blacklistsServiceProxy.createOrEdit(this.blacklist)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
             });
    }

        openSelectTransactionTypeModal() {
        this.transactionTypeLookupTableModal.id = this.blacklist.transactionTypeId;
        this.transactionTypeLookupTableModal.displayName = this.transactionTypeName;
        this.transactionTypeLookupTableModal.show();
    }
        openSelectUserModal() {
        this.userLookupTableModal.id = this.blacklist.approverUserId;
        this.userLookupTableModal.displayName = this.userName;
        this.userLookupTableModal.show();
    }


        setTransactionTypeIdNull() {
        this.blacklist.transactionTypeId = null;
        this.transactionTypeName = '';
    }
        setApproverUserIdNull() {
        this.blacklist.approverUserId = null;
        this.userName = '';
    }


        getNewTransactionTypeId() {
        this.blacklist.transactionTypeId = this.transactionTypeLookupTableModal.id;
        this.transactionTypeName = this.transactionTypeLookupTableModal.displayName;
    }
        getNewApproverUserId() {
        this.blacklist.approverUserId = this.userLookupTableModal.id;
        this.userName = this.userLookupTableModal.displayName;
    }


    close(): void {

        this.active = false;
        this.modal.hide();
    }
}
