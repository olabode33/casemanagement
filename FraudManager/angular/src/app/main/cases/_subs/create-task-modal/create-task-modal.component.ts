import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { finalize, takeLast } from 'rxjs/operators';
import { CreateOrEditCaseTaskDto, CaseTasksServiceProxy, NameValueDto, CommonLookupServiceProxy, FindUsersInput } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { AppConsts } from '@shared/AppConsts';
import { UserLookupTableModalComponent } from '@app/main/ruleEscalations/ruleEscalations/user-lookup-table-modal.component';


@Component({
    selector: 'app-create-task-modal',
    templateUrl: './create-task-modal.component.html'
})
export class CreateTaskModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    reassign = false;

    task: CreateOrEditCaseTaskDto = new CreateOrEditCaseTaskDto();
    assigneeName = '';
    allusers: NameValueDto[] = new Array();

    constructor(
        injector: Injector,
        private _caseTasksServiceProxy: CaseTasksServiceProxy,
        private _commonLookupServiceProxy: CommonLookupServiceProxy
    ) {
        super(injector);
        let filter = new FindUsersInput();
        filter.filter = '';
        filter.skipCount = 0;
        filter.maxResultCount = 1000;
        _commonLookupServiceProxy.findUsers(filter).subscribe(result => {
            this.allusers = result.items;
        });
    }

    show(caseId?: string): void {
        this.reassign = false;
        if (!caseId) {
            this.notify.error('Task must be assigned to a case!');
            this.active = false;
            this.modal.hide();
        } else {
            this.task = new CreateOrEditCaseTaskDto();
            this.task.caseId = caseId;
            this.task.type = 3;
            this.task.expectedCompletionDate = moment().startOf('day');
            this.assigneeName = '';

            this.active = true;
            this.modal.show();
        }
    }

    edit(taskId: number, isReassignment = false): void {
        this.reassign = isReassignment;

        this._caseTasksServiceProxy.getCaseTaskForEdit(taskId).subscribe(result => {
            this.task = result.caseTask;
            this.task.type = 2;
            console.log(this.task);
            this.active = true;
            this.modal.show();
        });
    }



    save(): void {
        this.saving = true;
        // if (this.task.assigneeUserId === abp.session.userId) {
        //     this.message.error('You cannot assign a task to yourself');
        // }

        this._caseTasksServiceProxy.createOrEdit(this.task)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.message.success('Task successfully created and assigned to user!');
                this.close();
                this.modalSave.emit(null);
            });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
