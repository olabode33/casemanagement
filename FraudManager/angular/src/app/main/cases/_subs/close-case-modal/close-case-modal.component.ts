import { ResolveCaseInputDto } from './../../../../../shared/service-proxies/service-proxies';
import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { finalize, takeLast } from 'rxjs/operators';
import { CasesServiceProxy, CreateOrEditCaseDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { AppConsts } from '@shared/AppConsts';

@Component({
  selector: 'app-close-case-modal',
  templateUrl: './close-case-modal.component.html',
  styleUrls: ['./close-case-modal.component.css']
})
export class CloseCaseModalComponent extends AppComponentBase {

  @ViewChild('createOrEditModal') modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    caseResolution: ResolveCaseInputDto = new ResolveCaseInputDto();

    constructor(
        injector: Injector,
        private _caseServiceProxy: CasesServiceProxy
    ) {
        super(injector);
    }

    show(caseId: string): void {
      this._caseServiceProxy.getCaseForEdit(caseId).subscribe(result => {
        this.caseResolution.caseId = result.case.id;
        this.caseResolution.conclusion = result.case.conclusion.valueOf();
        this.caseResolution.closureComment = result.case.closureComments;
        this.active = true;
        this.modal.show();
      });
    }

    save(): void {
        this.saving = true;

        this._caseServiceProxy.resolveCase(this.caseResolution)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.message.success('Case successfully closed!');
                this.close();
                this.modalSave.emit(null);
            });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

}
