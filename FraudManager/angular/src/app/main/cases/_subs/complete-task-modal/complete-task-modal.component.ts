import { Router } from '@angular/router';
import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { finalize, takeLast } from 'rxjs/operators';
import { CreateOrEditCaseTaskDto, CaseTasksServiceProxy, NameValueDto, CommonLookupServiceProxy, FindUsersInput } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { AppConsts } from '@shared/AppConsts';
import { UserLookupTableModalComponent } from '@app/main/ruleEscalations/ruleEscalations/user-lookup-table-modal.component';



@Component({
  selector: 'app-complete-task-modal',
  templateUrl: './complete-task-modal.component.html',
  styleUrls: ['./complete-task-modal.component.css']
})
export class CompleteTaskModalComponent extends AppComponentBase {

  @ViewChild('createOrEditModal') modal: ModalDirective;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  @Output() modalCancel: EventEmitter<any> = new EventEmitter<any>();

  active = false;
  saving = false;
  completing = false;
  showGoToCaseButton = false;

  task: CreateOrEditCaseTaskDto = new CreateOrEditCaseTaskDto();
  assignerName = '';
  assigneeName = '';
  viewOnly = false;

  constructor(
    injector: Injector,
    private _caseTasksServiceProxy: CaseTasksServiceProxy,
    private _commonLookupServiceProxy: CommonLookupServiceProxy,
    private _router: Router
  ) {
    super(injector);
  }

  show(taskId: number): void {
    this.viewOnly = false;
    this._caseTasksServiceProxy.getCaseTaskForEdit(taskId).subscribe(result => {
      this.task = result.caseTask;
      this.assignerName = result.userName;
      this.active = true;
      this.modal.show();
    });
  }

  view(taskId: number): void {
    this.viewOnly = true;
    this._caseTasksServiceProxy.getCaseTaskForEdit(taskId).subscribe(result => {
      this.task = result.caseTask;
      this.assignerName = result.userName;
      this.assigneeName = result.userName2;
      this.active = true;
      this.modal.show();
    });
  }

  save(): void {
    this.saving = true;
    this.task.status = 2;
    this._caseTasksServiceProxy.createOrEdit(this.task)
      .pipe(finalize(() => { this.saving = false; }))
      .subscribe(() => {
        this.message.success('Draft Saved!');
        this.close();
        this.modalSave.emit(null);
      });
  }

  complete(): void {
    if (this.task.comment === null || this.task.comment === '') {
      this.notify.error('Please a comment before saving...');
      return;
    }

    this.saving = true;

    this._caseTasksServiceProxy.completeTask(this.task)
      .pipe(finalize(() => { this.saving = false; }))
      .subscribe(() => {
        this.message.success('Task completed!');
        this.close();
        this.modalSave.emit(null);
      });
  }

  close(): void {
    this.active = false;
    this.modal.hide();
    this.modalCancel.emit(null);
  }

  goToCase(): void {
    this._router.navigate(['app/main/cases/cases/view', this.task.caseId]);
  }

  setShowGoToCaseButton(flag: boolean): void {
    this.showGoToCaseButton = flag;
  }

  getTaskPriorityClass(): string {
    switch (this.task.priority) {
      case 2:
        return 'danger';
      case 1:
        return 'warning';
      case 0:
        return 'success';
      default:
        return 'default';
    }
  }

}
