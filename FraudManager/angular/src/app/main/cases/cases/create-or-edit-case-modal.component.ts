import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { finalize } from 'rxjs/operators';
import { CasesServiceProxy, CreateOrEditCaseDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { TransactionTypeLookupTableModalComponent } from '@app/main/payloads/payloads/transactionType-lookup-table-modal.component';
import { UserLookupTableModalComponent } from '@app/main/ruleEscalations/ruleEscalations/user-lookup-table-modal.component';
//import { DemoUiFileUploadComponent } from '@app/admin/demo-ui-components/demo-ui-file-upload.component';
import { DemoUiComponentsServiceProxy } from '@shared/service-proxies/service-proxies';
import { RuleLookupTableModalComponent } from '@app/main/ruleConfigurations/ruleConfigurations/rule-lookup-table-modal.component';
import { AppConsts } from '@shared/AppConsts';


@Component({
    selector: 'createOrEditCaseModal',
    templateUrl: './create-or-edit-case-modal.component.html'
})
export class CreateOrEditCaseModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;
    @ViewChild('transactionTypeLookupTableModal') transactionTypeLookupTableModal: TransactionTypeLookupTableModalComponent;
    @ViewChild('userLookupTableModal') userLookupTableModal: UserLookupTableModalComponent;
    @ViewChild('ruleLookupTableModal') ruleLookupTableModal: RuleLookupTableModalComponent;


    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    case: CreateOrEditCaseDto = new CreateOrEditCaseDto();

    transactionTypeName = '';
    userName = '';
    ruleName = '';

    uploadUrl: string;
    uploadedFiles: any[] = [];

    constructor(
        injector: Injector,
        private _casesServiceProxy: CasesServiceProxy,
        private demoUiComponentsService: DemoUiComponentsServiceProxy
    ) {
        super(injector);
        this.uploadUrl = AppConsts.remoteServiceBaseUrl + '/DemoUiComponents/UploadFiles';
    }

    // upload completed event
    onUpload(event): void {
        for (const file of event.files) {
            this.uploadedFiles.push(file);
        }
    }

    onBeforeSend(event): void {
        event.xhr.setRequestHeader('Authorization', 'Bearer ' + abp.auth.getToken());
    }

    show(caseId?: string): void {

        if (!caseId) {
            this.case = new CreateOrEditCaseDto();
            this.case.id = caseId;
            this.case.transactionDate = moment().startOf('day');
            this.transactionTypeName = '';
            this.userName = '';
            this.ruleName = '';

            this.active = true;
            this.modal.show();
        } else {
            this._casesServiceProxy.getCaseForEdit(caseId).subscribe(result => {
                this.case = result.case;

                this.transactionTypeName = result.transactionTypeName;
                this.userName = result.userName;
                this.ruleName = result.ruleName;

                this.active = true;
                this.modal.show();
            });
        }
    }

    save(): void {
            this.saving = true;
			
            this._casesServiceProxy.createOrEdit(this.case)
             .pipe(finalize(() => { this.saving = false; }))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
             });
    }

        openSelectTransactionTypeModal() {
        this.transactionTypeLookupTableModal.id = this.case.transactionTypeId;
        this.transactionTypeLookupTableModal.displayName = this.transactionTypeName;
        this.transactionTypeLookupTableModal.show();
    }
        openSelectUserModal() {
        this.userLookupTableModal.id = this.case.allocationUserId;
        this.userLookupTableModal.displayName = this.userName;
        this.userLookupTableModal.show();
    }
    openSelectRuleModal() {
        if (this.case.transactionTypeId == null) {
            this.notify.error("Please select a transaction type first.")
            return;
        }

        this.ruleLookupTableModal.id = this.case.ruleId;
        this.ruleLookupTableModal.displayName = this.ruleName;
        this.ruleLookupTableModal.show(this.case.transactionTypeId);
    }


        setTransactionTypeIdNull() {
        this.case.transactionTypeId = null;
            this.transactionTypeName = '';

            this.setRuleIdNull();
    }
        setAllocationUserIdNull() {
        this.case.allocationUserId = null;
        this.userName = '';
    }
        setRuleIdNull() {
        this.case.ruleId = null;
        this.ruleName = '';
    }


        getNewTransactionTypeId() {
        this.case.transactionTypeId = this.transactionTypeLookupTableModal.id;
            this.transactionTypeName = this.transactionTypeLookupTableModal.displayName;
            this.setRuleIdNull();
    }
        getNewAllocationUserId() {
        this.case.allocationUserId = this.userLookupTableModal.id;
        this.userName = this.userLookupTableModal.displayName;
    }
        getNewRuleId() {
        this.case.ruleId = this.ruleLookupTableModal.id;
        this.ruleName = this.ruleLookupTableModal.displayName;
    }


    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
