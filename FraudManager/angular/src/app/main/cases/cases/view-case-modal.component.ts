import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { GetCaseForView, CaseDto , CaseDtoDetectionSource, CaseDtoSource, CaseDtoTransactionStatus, CaseDtoConclusion, CaseDtoStatus, PhoneServiceProxy, TwilioInputDto} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'viewCaseModal',
    templateUrl: './view-case-modal.component.html'
})
export class ViewCaseModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item: GetCaseForView;
    dectectionSource = CaseDtoDetectionSource;
    sources = CaseDtoSource;
    transactionStatus = CaseDtoTransactionStatus;
    conclusion = CaseDtoConclusion;
    status = CaseDtoStatus;

    callInputDto: TwilioInputDto = new TwilioInputDto();

    constructor(
        injector: Injector,
        private _phoneServiceProxy: PhoneServiceProxy
    ) {
        super(injector);
        this.item = new GetCaseForView();
        this.item.case = new CaseDto();
    }

    show(item: GetCaseForView): void {
        this.item = item;
        this.active = true;
        this.modal.show();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
        this.modalSave.emit(null);
    }

    callCustomer(): void {
        this.callInputDto.toPhoneNumber = this.item.case.phoneNumber;
        this.callInputDto.transactionId = this.item.case.id;
        this.callInputDto.isfraud = '1';
        this.callInputDto.amount = this.item.case.amount;

        this.message.confirm(
            'Contact Customer',
            (isConfirmed) => {
                if (isConfirmed) {
                    this._phoneServiceProxy.contactCustomer(this.callInputDto).subscribe();
                    this.notify.info('Processing call...');
                    this.close();
                }
            }
        );
    }
}
