import { CloseCaseModalComponent } from './../_subs/close-case-modal/close-case-modal.component';
import { Table } from 'primeng/components/table/table';
import { Component, ViewChild, Injector, Output, EventEmitter, OnInit, ViewEncapsulation} from '@angular/core';
import { Location } from '@angular/common';
import { CasesServiceProxy, CreateOrEditCaseDto, GetCaseFailedRuleForViewDto, GetCaseTaskForViewDto, CaseTasksServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { Router, ActivatedRoute, Params } from '../../../../../node_modules/@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { CreateTaskModalComponent } from '../_subs/create-task-modal/create-task-modal.component';
import { CompleteTaskModalComponent } from '../_subs/complete-task-modal/complete-task-modal.component';

@Component({
    selector: 'viewCaseDetails',
    templateUrl: './view-case-details.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class ViewCaseDetailsComponent extends AppComponentBase implements OnInit {

    @ViewChild('failedRulesTable') failedRulesTable: Table;
    @ViewChild('tasksTable') tasksTable: Table;
    @ViewChild('createCaseTaskModal') createCaseTaskModal: CreateTaskModalComponent;
    @ViewChild('completeTaskModal') completeTaskModal: CompleteTaskModalComponent;
    @ViewChild('closeCaseModal') closeCaseModal: CloseCaseModalComponent;

    case: CreateOrEditCaseDto = new CreateOrEditCaseDto();
    failedRules: GetCaseFailedRuleForViewDto[] = new Array();
    tasks: GetCaseTaskForViewDto[] = new Array();
    pendingTasks: GetCaseTaskForViewDto[] = new Array();

    caseId = '';

    transactionTypeName = '';
    userName = '';
    ruleName = '';

    constructor(
        injector: Injector,
        private _casesServiceProxy: CasesServiceProxy,
        private _caseTasksServiceProxy: CaseTasksServiceProxy,
        private _router: Router,
        private _activatedRoute: ActivatedRoute,
        private _location: Location
    ) {
        super(injector);
    }

    ngOnInit() {
        this._activatedRoute.params.subscribe((params: Params) => {
            if (params.caseId) {
                this.caseId = params['caseId'];
                this.getCaseDetails(this.caseId);
            }
        });
        this.completeTaskModal.setShowGoToCaseButton(false);
    }

    getCaseDetails(caseId: string) {
        this._casesServiceProxy.getCaseForEdit(caseId).subscribe(result => {
            this.case = result.case;
            this.failedRules = result.failedRules;
            this.tasks = result.tasks;
            this.pendingTasks = result.tasks.filter(x => x.caseTask.status !== 1);


            this.transactionTypeName = result.transactionTypeName;
            this.userName = result.userName;
            this.ruleName = result.ruleName;
        });
    }

    reload(): void {
        this.getCaseDetails(this.caseId);
    }

    goBack() {
        this._location.back();
        //this._router.navigate(['../../'], { relativeTo: this._activatedRoute});
    }

    closeCase(): void {
        if (this.pendingTasks.length > 0) {
            this.message.error('All tasks must be completed before closing the case.');
            return;
        }

        this.closeCaseModal.show(this.caseId);
    }

    viewTask(task: GetCaseTaskForViewDto): void {
        this.completeTaskModal.view(task.caseTask.id);
    }

    createTask(): void {
        this.createCaseTaskModal.show(this.caseId);
    }

    editTask(taskId: number): void {
        this.createCaseTaskModal.edit(taskId);
    }

    reassignTask(taskid: number): void {
        this.createCaseTaskModal.edit(taskid, true);
    }

    completeTask(task: GetCaseTaskForViewDto): void {
        if (task.caseTask.assigneeUserId === abp.session.userId) {
            this.completeTaskModal.show(task.caseTask.id);
        } else {
            this.notify.error('Task can only be completed by the user it was assigned to!');
        }
    }

    viewRule(ruleId: string): void {
        this._router.navigate(['app/main/rules/view', ruleId]);
    }

    deleteTask(task: GetCaseTaskForViewDto): void {
        if (task.caseTask.assignerUserId === abp.session.userId) {
            this.message.confirm(
                '',
                (isConfirmed) => {
                    if (isConfirmed) {
                        this._caseTasksServiceProxy.delete(task.caseTask.id)
                            .subscribe(() => {
                                this.reload();
                                this.message.success(this.l('SuccessfullyDeleted'));
                            });
                    }
                }
            );
        } else {
            this.notify.error('Task can only be deleted by the user that created it!');
        }
    }

    getTransactionStatusClass(transactionStatus: number): string {
        switch (transactionStatus) {
            case 1:
                return 'success';
            default:
                return 'danger';
        }
    }

    getCaseStatusClass(caseStatus: number): string {
        switch (caseStatus) {
            case 0:
                return 'danger';
            case 1:
                return 'success';
            case 2:
                return 'warning';
            default:
                return 'default';
        }
    }

    getConclustionStatusClass(conclusion: number): string {
        switch (conclusion) {
            case 0:
                return 'warning';
            case 2:
                return 'success';
            default:
                return 'danger';
        }
    }

    getTaskPriorityClass(priority: number): string {
        switch (priority) {
          case 2:
            return 'danger';
          case 1:
            return 'warning';
          case 0:
            return 'success';
          default:
            return 'default';
        }
    }

    showButton(task: GetCaseTaskForViewDto, button: string): boolean {
        switch (button) {
            case 'complete':
                if (task.caseTask.assigneeUserId === abp.session.userId && task.caseTask.status !== 1) {
                    return true;
                } else {
                    return false;
                }
            case 'editDelete':
                if (task.caseTask.assignerUserId === abp.session.userId && task.caseTask.status !== 1) {
                    return true;
                } else {
                    return false;
                }
            case 'reassign':
                if ((task.caseTask.assignerUserId === abp.session.userId || task.caseTask.assigneeUserId === abp.session.userId || task.caseTask.assignerUserId === null)
                    && task.caseTask.status !== 1) {
                    return true;
                } else {
                    return false;
                }
            default:
                return false;
        }
    }

}
