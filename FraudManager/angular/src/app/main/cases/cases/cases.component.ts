import { Component, Injector, ViewEncapsulation, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Http } from '@angular/http';
import { CasesServiceProxy, CaseDto, CaseDtoDetectionSource, CaseDtoSource, CaseDtoTransactionStatus, CaseDtoConclusion, CaseDtoStatus } from '@shared/service-proxies/service-proxies';
import { NotifyService } from '@abp/notify/notify.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { CreateOrEditCaseModalComponent } from './create-or-edit-case-modal.component';
import { ViewCaseModalComponent } from './view-case-modal.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/components/table/table';
import { Paginator } from 'primeng/components/paginator/paginator';
import { LazyLoadEvent } from 'primeng/components/common/lazyloadevent';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { CreateTaskModalComponent } from '../_subs/create-task-modal/create-task-modal.component';

@Component({
    templateUrl: './cases.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class CasesComponent extends AppComponentBase {

    @ViewChild('createOrEditCaseModal') createOrEditCaseModal: CreateOrEditCaseModalComponent;
    @ViewChild('createCaseTaskModal') createCaseTaskModal: CreateTaskModalComponent;
    @ViewChild('viewCaseModalComponent') viewCaseModal: ViewCaseModalComponent;
    @ViewChild('dataTable') dataTable: Table;
    @ViewChild('paginator') paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = '';
    maxTransactionDateFilter: moment.Moment;
    minTransactionDateFilter: moment.Moment;
    accountNumberFilter = '';
    accountTypeFilter = '';
    detectionSourceFilter = -1;
    sourceFilter = -1;
    transactionStatusFilter = -1;
    conclusionFilter = -1;
    statusFilter = -1;
    transactionTypeNameFilter = '';
    userNameFilter = '';
    ruleNameFilter = '';

    dectectionSource = CaseDtoDetectionSource;
    sources = CaseDtoSource;
    transactionStatus = CaseDtoTransactionStatus;
    conclusion = CaseDtoConclusion;
    status = CaseDtoStatus;



    constructor(
        injector: Injector,
        private _casesServiceProxy: CasesServiceProxy,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    getCases(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._casesServiceProxy.getAll(
            this.filterText,
            this.maxTransactionDateFilter,
            this.minTransactionDateFilter,
            this.accountNumberFilter,
            this.accountTypeFilter,
            this.detectionSourceFilter,
            this.sourceFilter,
            this.transactionStatusFilter,
            this.conclusionFilter,
            this.statusFilter,
            this.transactionTypeNameFilter,
            this.userNameFilter,
            this.ruleNameFilter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createCase(): void {
        this.createOrEditCaseModal.show();
    }

    createTask(caseId: string): void {
        this.createCaseTaskModal.show(caseId);
    }

    deleteCase(alert: CaseDto): void {
        this.message.confirm(
            '',
            (isConfirmed) => {
                if (isConfirmed) {
                    this._casesServiceProxy.delete(alert.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }

    viewCaseDetails(caseId: string) {
        this._router.navigate(['view', caseId], { relativeTo: this._activatedRoute });
    }

    exportToExcel(): void {
        this._casesServiceProxy.getCasesToExcel(
            this.filterText,
            this.maxTransactionDateFilter,
            this.minTransactionDateFilter,
            this.accountNumberFilter,
            this.accountTypeFilter,
            this.detectionSourceFilter,
            this.sourceFilter,
            this.transactionStatusFilter,
            this.conclusionFilter,
            this.statusFilter,
            this.transactionTypeNameFilter,
            this.userNameFilter,
            this.ruleNameFilter,
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    getCaseStatusClass(caseStatus: number): string {
        switch (caseStatus) {
            case 0:
                return 'danger';
            case 1:
                return 'success';
            case 2:
                return 'warning';
            default:
                return 'default';
        }
    }

    getConclustionStatusClass(conclusion: number): string {
        switch (conclusion) {
            case 0:
                return 'warning';
            case 2:
                return 'success';
            default:
                return 'danger';
        }
    }
}
