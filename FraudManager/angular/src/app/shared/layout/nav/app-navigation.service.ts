import { PermissionCheckerService } from '@abp/auth/permission-checker.service';
import { AppSessionService } from '@shared/common/session/app-session.service';

import { Injectable } from '@angular/core';
import { AppMenu } from './app-menu';
import { AppMenuItem } from './app-menu-item';

@Injectable()
export class AppNavigationService {

    constructor(
        private _permissionCheckerService: PermissionCheckerService,
        private _appSessionService: AppSessionService
    ) {
    }

    getMenu(): AppMenu {
        return new AppMenu('MainMenu', 'MainMenu', [
            new AppMenuItem('Home', 'Pages.Tenant.Dashboard', 'flaticon-buildings', '/app/main/home'),
            new AppMenuItem('Dashboard', 'Pages.Administration.Host.Dashboard', 'flaticon-line-graph', '/app/admin/hostDashboard'),
            //new AppMenuItem('Dashboard', 'Pages.Tenant.Dashboard', 'flaticon-line-graph', '/app/main/dashboard'),
            new AppMenuItem('Tenants', 'Pages.Tenants', 'flaticon-list-3', '/app/admin/tenants'),
            new AppMenuItem('Editions', 'Pages.Editions', 'flaticon-app', '/app/admin/editions'),
           
           // new AppMenuItem('Channels', 'Pages.Channels', 'flaticon-more', '/app/main/channels/channels'),
            
            //new AppMenuItem('TransactionTypes', 'Pages.TransactionTypes', 'flaticon-more', '/app/main/transactionTypes/transactionTypes'),
            
            //new AppMenuItem('Payloads', 'Pages.Payloads', 'flaticon-more', '/app/main/payloads/payloads'),
            
            //new AppMenuItem('PayloadProperties', 'Pages.PayloadProperties', 'flaticon-more', '/app/main/payloadProperties/payloadProperties'),
            
            //new AppMenuItem('Rules', 'Pages.Rules', 'flaticon-more', '/app/main/rules/rules'),
            
            //new AppMenuItem('RuleConfigurations', 'Pages.RuleConfigurations', 'flaticon-more', '/app/main/ruleConfigurations/ruleConfigurations'),

            new AppMenuItem('Rule Management', 'Pages.Rules', 'flaticon-book', '/app/main/rules/rules'),

            new AppMenuItem('Case Management', '', 'flaticon-suitcase', '', [
                new AppMenuItem('Cases', 'Pages.Cases', 'flaticon-suitcase', '/app/main/cases/cases'),
                new AppMenuItem('Whitelists', 'Pages.Whitelists', 'flaticon-list-3', '/app/main/whitelists/whitelists'),
                new AppMenuItem('Blacklists', 'Pages.Blacklists', '	flaticon-signs-1', '/app/main/blacklists/blacklists')
            ]),

            new AppMenuItem('Configuration', '', 'flaticon-interface-1', '', [
                new AppMenuItem('Channels', 'Pages.Channels', 'flaticon-map', '/app/main/channels/channels'),
                new AppMenuItem('TransactionTypes', 'Pages.TransactionTypes', '	flaticon-list', '/app/main/transactionTypes/transactionTypes'),
                new AppMenuItem('Data Points', 'Pages.Payloads', 'flaticon-apps', '/app/main/payloads/payloads'),
            ]),


          //  new AppMenuItem('RuleEscalations', 'Pages.RuleEscalations', 'flaticon-more', '/app/main/ruleEscalations/ruleEscalations'),
            
  
            

            
             new AppMenuItem('Administration', '', 'flaticon-settings', '', [
                new AppMenuItem('OrganizationUnits', 'Pages.Administration.OrganizationUnits', 'flaticon-map', '/app/admin/organization-units'),
                new AppMenuItem('Roles', 'Pages.Administration.Roles', 'flaticon-users', '/app/admin/roles'),
                new AppMenuItem('Users', 'Pages.Administration.Users', 'flaticon-user', '/app/admin/users'),
                new AppMenuItem('Languages', 'Pages.Administration.Languages', 'flaticon-tabs', '/app/admin/languages'),
                new AppMenuItem('AuditLogs', 'Pages.Administration.AuditLogs', 'flaticon-folder-1', '/app/admin/auditLogs'),
                new AppMenuItem('Maintenance', 'Pages.Administration.Host.Maintenance', 'flaticon-lock', '/app/admin/maintenance'),
              //  new AppMenuItem('Subscription', 'Pages.Administration.Tenant.SubscriptionManagement', 'flaticon-refresh', '/app/admin/subscription-management'),
                new AppMenuItem('VisualSettings', 'Pages.Administration.UiCustomization', 'flaticon-medical', '/app/admin/ui-customization'),
                new AppMenuItem('Settings', 'Pages.Administration.Host.Settings', 'flaticon-settings', '/app/admin/hostSettings'),
                new AppMenuItem('Settings', 'Pages.Administration.Tenant.Settings', 'flaticon-settings', '/app/admin/tenantSettings')
            ])
          //  new AppMenuItem('DemoUiComponents', 'Pages.DemoUiComponents', 'flaticon-shapes', '/app/admin/demo-ui-components')
        ]);
    }

    checkChildMenuItemPermission(menuItem): boolean {

        for (let i = 0; i < menuItem.items.length; i++) {
            let subMenuItem = menuItem.items[i];

            if (subMenuItem.permissionName && this._permissionCheckerService.isGranted(subMenuItem.permissionName)) {
                return true;
            } else if (subMenuItem.items && subMenuItem.items.length) {
                return this.checkChildMenuItemPermission(subMenuItem);
            }
        }

        return false;
    }

    showMenuItem(menuItem: AppMenuItem): boolean {
        if (menuItem.permissionName === 'Pages.Administration.Tenant.SubscriptionManagement' && this._appSessionService.tenant && !this._appSessionService.tenant.edition) {
            return false;
        }

        let hideMenuItem = false;

        if (menuItem.requiresAuthentication && !this._appSessionService.user) {
            hideMenuItem = true;
        }

        if (menuItem.permissionName && !this._permissionCheckerService.isGranted(menuItem.permissionName)) {
            hideMenuItem = true;
        }

        if (menuItem.hasFeatureDependency() && !menuItem.featureDependencySatisfied()) {
            hideMenuItem = true;
        }

        if (!hideMenuItem && menuItem.items && menuItem.items.length) {
            return this.checkChildMenuItemPermission(menuItem);
        }

        return !hideMenuItem;
    }
}
