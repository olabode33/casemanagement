﻿using Abp.Auditing;
using Microsoft.AspNetCore.Mvc;

namespace Test.Web.Controllers
{
    public class HomeController : TestControllerBase
    {
        [DisableAuditing]
        public IActionResult Index()
        {
            return Redirect("/swagger");
        }
    }
}
