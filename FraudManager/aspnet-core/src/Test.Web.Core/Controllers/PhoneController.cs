﻿using System;
using System.Collections.Generic;
using System.Text;
//using System.Web.Mvc;

using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

using Twilio.TwiML;
using Twilio.AspNet.Common;
using Microsoft.AspNetCore.Mvc;
using Twilio.TwiML.Voice;
using Twilio.AspNet.Core;

namespace Test.Web.Controllers
{
    public class PhoneController: TestControllerBase
    {
        private string accountsid = "ACf96f55b7044bc5ac6c9a83833e3be5d1";
        private string authToken = "a5357ca20b14f82c91dd6ee79cd8dbe7";

        public string MakeCall()
        {
            TwilioClient.Init(accountsid, authToken);

            var from = new PhoneNumber("+13252214024");
            var to = new PhoneNumber("+2348068754606");
            var call = CallResource.Create(
                to: to,
                from: from,
                url: new Uri("http://df3c6c2c.ngrok.io/voice/work")
            );
            return (call.Sid);
        }

        public string SendSMS()
        {
            
            TwilioClient.Init(accountsid, authToken);

            var from = new PhoneNumber("+13252214024");
            var to = new PhoneNumber("+2348068754606");
            var message = MessageResource.Create(
                body: "This is a test message... RUNNNN",
                from: from,
                to: to
              );
            return (message.Sid);
        }

        //[HttpPost]
        //public TwiMLResult Work()
        //{

        //    var response = new VoiceResponse();
        //    response.Append(
        //        new Gather(
        //            numDigits: 4,
        //            action: new Uri("http://722970b8.ngrok.io/phone/HandleCallResponse")
        //            ).Say("Welcome to my bank. You have a pending transaction, please enter your year of birth to authorise followed by the # key")
        //        );
        //    return TwiML(response);
        //}

        //[HttpPost]
        //public TwiMLResult HandleCallResponse(string digits)
        //{
        //    var response = new VoiceResponse();
        //    if (!string.IsNullOrEmpty(digits))
        //    {
        //        switch (digits)
        //        {
        //            case "1992":
        //            case "1991":
        //                response.Say("Your transaction has been approved, thank you");
        //                break;

        //            default:
        //                response.Say("Sorry, your transaction will be denied, thank you.").Pause();
        //                break;
        //        }
        //    }
        //    else
        //    {

        //    }
        //    return TwiML(response);
        //}
    }
}
