﻿using System.Threading.Tasks;
using Test.Sessions.Dto;

namespace Test.Web.Session
{
    public interface IPerRequestSessionCache
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformationsAsync();
    }
}
