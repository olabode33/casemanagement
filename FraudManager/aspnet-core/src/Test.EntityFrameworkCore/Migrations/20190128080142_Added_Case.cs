﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Test.Migrations
{
    public partial class Added_Case : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Cases",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TransactionDate = table.Column<DateTime>(nullable: false),
                    AccountNumber = table.Column<string>(nullable: false),
                    AccountName = table.Column<string>(nullable: true),
                    AccountType = table.Column<string>(nullable: true),
                    ProductName = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    DetectionSource = table.Column<int>(nullable: false),
                    Source = table.Column<int>(nullable: false),
                    TransactionStatus = table.Column<int>(nullable: false),
                    Conclusion = table.Column<int>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    ClosureComments = table.Column<string>(nullable: true),
                    TransactionTypeId = table.Column<Guid>(nullable: false),
                    AllocationUserId = table.Column<long>(nullable: true),
                    RuleId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cases", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Cases_AbpUsers_AllocationUserId",
                        column: x => x.AllocationUserId,
                        principalTable: "AbpUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Cases_Rules_RuleId",
                        column: x => x.RuleId,
                        principalTable: "Rules",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Cases_TransactionTypes_TransactionTypeId",
                        column: x => x.TransactionTypeId,
                        principalTable: "TransactionTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Cases_AllocationUserId",
                table: "Cases",
                column: "AllocationUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Cases_RuleId",
                table: "Cases",
                column: "RuleId");

            migrationBuilder.CreateIndex(
                name: "IX_Cases_TransactionTypeId",
                table: "Cases",
                column: "TransactionTypeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Cases");
        }
    }
}
