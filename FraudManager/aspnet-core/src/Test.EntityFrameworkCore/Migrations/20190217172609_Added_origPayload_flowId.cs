﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Test.Migrations
{
    public partial class Added_origPayload_flowId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "VerificationCode",
                table: "Cases",
                nullable: true,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FlowId",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OriginalPayLoad",
                table: "Cases",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FlowId",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "OriginalPayLoad",
                table: "Cases");

            migrationBuilder.AlterColumn<int>(
                name: "VerificationCode",
                table: "Cases",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
