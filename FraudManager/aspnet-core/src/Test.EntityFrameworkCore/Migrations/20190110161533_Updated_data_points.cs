﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Test.Migrations
{
    public partial class Updated_data_points : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PayloadProperties_Payloads_PayloadId",
                table: "PayloadProperties");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PayloadProperties",
                table: "PayloadProperties");

            migrationBuilder.RenameTable(
                name: "PayloadProperties",
                newName: "DataPoints");

            migrationBuilder.RenameIndex(
                name: "IX_PayloadProperties_PayloadId",
                table: "DataPoints",
                newName: "IX_DataPoints_PayloadId");

            migrationBuilder.AlterColumn<Guid>(
                name: "PayloadId",
                table: "DataPoints",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "DataPoints",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddPrimaryKey(
                name: "PK_DataPoints",
                table: "DataPoints",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_DataPoints_Payloads_PayloadId",
                table: "DataPoints",
                column: "PayloadId",
                principalTable: "Payloads",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DataPoints_Payloads_PayloadId",
                table: "DataPoints");

            migrationBuilder.DropPrimaryKey(
                name: "PK_DataPoints",
                table: "DataPoints");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "DataPoints");

            migrationBuilder.RenameTable(
                name: "DataPoints",
                newName: "PayloadProperties");

            migrationBuilder.RenameIndex(
                name: "IX_DataPoints_PayloadId",
                table: "PayloadProperties",
                newName: "IX_PayloadProperties_PayloadId");

            migrationBuilder.AlterColumn<Guid>(
                name: "PayloadId",
                table: "PayloadProperties",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_PayloadProperties",
                table: "PayloadProperties",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_PayloadProperties_Payloads_PayloadId",
                table: "PayloadProperties",
                column: "PayloadId",
                principalTable: "Payloads",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
