﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Test.Migrations
{
    public partial class Updated_case_task : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CaseId",
                table: "CaseTasks",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_CaseTasks_CaseId",
                table: "CaseTasks",
                column: "CaseId");

            migrationBuilder.AddForeignKey(
                name: "FK_CaseTasks_Cases_CaseId",
                table: "CaseTasks",
                column: "CaseId",
                principalTable: "Cases",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CaseTasks_Cases_CaseId",
                table: "CaseTasks");

            migrationBuilder.DropIndex(
                name: "IX_CaseTasks_CaseId",
                table: "CaseTasks");

            migrationBuilder.DropColumn(
                name: "CaseId",
                table: "CaseTasks");
        }
    }
}
