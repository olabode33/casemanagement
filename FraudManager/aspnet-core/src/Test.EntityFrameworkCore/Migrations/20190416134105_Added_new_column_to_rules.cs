﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Test.Migrations
{
    public partial class Added_new_column_to_rules : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Active",
                table: "Rules",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<long>(
                name: "ApprovedBy",
                table: "Rules",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ApprovedDate",
                table: "Rules",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsApproved",
                table: "Rules",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<decimal>(
                name: "Score",
                table: "Rules",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Active",
                table: "Rules");

            migrationBuilder.DropColumn(
                name: "ApprovedBy",
                table: "Rules");

            migrationBuilder.DropColumn(
                name: "ApprovedDate",
                table: "Rules");

            migrationBuilder.DropColumn(
                name: "IsApproved",
                table: "Rules");

            migrationBuilder.DropColumn(
                name: "Score",
                table: "Rules");
        }
    }
}
