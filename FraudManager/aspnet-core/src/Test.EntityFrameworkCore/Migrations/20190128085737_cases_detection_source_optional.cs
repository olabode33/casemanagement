﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Test.Migrations
{
    public partial class cases_detection_source_optional : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "DetectionSource",
                table: "Cases",
                nullable: true,
                oldClrType: typeof(int));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "DetectionSource",
                table: "Cases",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);
        }
    }
}
