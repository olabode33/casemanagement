﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Test.Migrations
{
    public partial class Addedphonenumber : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CustomerID",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PhoneNumber",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "VerificationCode",
                table: "Cases",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CustomerID",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "PhoneNumber",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "VerificationCode",
                table: "Cases");
        }
    }
}
