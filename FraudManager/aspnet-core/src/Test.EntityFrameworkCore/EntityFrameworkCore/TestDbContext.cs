using Test.Blacklists;
using Test.Whitelists;
using Test.Cases;
using Test.RuleEscalations;
using Test.RuleConfigurations;
using Test.Rules;
using Test.PayloadProperties;
using Test.Payloads;
using Test.TransactionTypes;
using Test.Channels;
using Abp.IdentityServer4;
using Abp.Zero.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Test.Authorization.Roles;
using Test.Authorization.Users;
using Test.Chat;
using Test.Editions;
using Test.Friendships;
using Test.MultiTenancy;
using Test.MultiTenancy.Accounting;
using Test.MultiTenancy.Payments;
using Test.Storage;

namespace Test.EntityFrameworkCore
{
    public class TestDbContext : AbpZeroDbContext<Tenant, Role, User, TestDbContext>, IAbpPersistedGrantDbContext
    {
        public virtual DbSet<CaseAction> CaseActions { get; set; }

        public virtual DbSet<CaseTask> CaseTasks { get; set; }

        public virtual DbSet<CaseFailedRule> CaseFailedRules { get; set; }

        public virtual DbSet<Blacklist> Blacklists { get; set; }

        public virtual DbSet<Whitelist> Whitelists { get; set; }

        public virtual DbSet<Alert> Cases { get; set; }

        public virtual DbSet<RuleEscalation> RuleEscalations { get; set; }

        public virtual DbSet<RuleConfiguration> RuleConfigurations { get; set; }


        
        public virtual DbSet<DataPoint> DataPoints { get; set; }

        public virtual DbSet<Rule> Rules { get; set; }

        public virtual DbSet<PayloadProperty> PayloadProperties { get; set; }

        public virtual DbSet<Payload> Payloads { get; set; }

        public virtual DbSet<TransactionType> TransactionTypes { get; set; }

        public virtual DbSet<Channel> Channels { get; set; }

        /* Define an IDbSet for each entity of the application */

        public virtual DbSet<BinaryObject> BinaryObjects { get; set; }

        public virtual DbSet<Friendship> Friendships { get; set; }

        public virtual DbSet<ChatMessage> ChatMessages { get; set; }

        public virtual DbSet<SubscribableEdition> SubscribableEditions { get; set; }

        public virtual DbSet<SubscriptionPayment> SubscriptionPayments { get; set; }

        public virtual DbSet<Invoice> Invoices { get; set; }

        public virtual DbSet<PersistedGrantEntity> PersistedGrants { get; set; }

        public TestDbContext(DbContextOptions<TestDbContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<BinaryObject>(b =>
            {
                b.HasIndex(e => new { e.TenantId });
            });

            modelBuilder.Entity<ChatMessage>(b =>
            {
                b.HasIndex(e => new { e.TenantId, e.UserId, e.ReadState });
                b.HasIndex(e => new { e.TenantId, e.TargetUserId, e.ReadState });
                b.HasIndex(e => new { e.TargetTenantId, e.TargetUserId, e.ReadState });
                b.HasIndex(e => new { e.TargetTenantId, e.UserId, e.ReadState });
            });

            modelBuilder.Entity<Friendship>(b =>
            {
                b.HasIndex(e => new { e.TenantId, e.UserId });
                b.HasIndex(e => new { e.TenantId, e.FriendUserId });
                b.HasIndex(e => new { e.FriendTenantId, e.UserId });
                b.HasIndex(e => new { e.FriendTenantId, e.FriendUserId });
            });

            modelBuilder.Entity<Tenant>(b =>
            {
                b.HasIndex(e => new { e.SubscriptionEndDateUtc });
                b.HasIndex(e => new { e.CreationTime });
            });

            modelBuilder.Entity<SubscriptionPayment>(b =>
            {
                b.HasIndex(e => new { e.Status, e.CreationTime });
                b.HasIndex(e => new { e.PaymentId, e.Gateway });
            });

            modelBuilder.ConfigurePersistedGrantEntity();
        }
    }
}
