﻿using Microsoft.Extensions.Configuration;

namespace Test.Configuration
{
    public interface IAppConfigurationAccessor
    {
        IConfigurationRoot Configuration { get; }
    }
}
