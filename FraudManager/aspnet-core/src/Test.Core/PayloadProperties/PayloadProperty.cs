using Test;
using Test.Payloads;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;
using Abp.Auditing;

namespace Test.PayloadProperties
{
	[Table("PayloadProperties")]
    [Audited]
    public class PayloadProperty : DataPoint
    {
		public virtual Guid PayloadId { get; set; }
		public Payload Payload { get; set; }
		
    }
}