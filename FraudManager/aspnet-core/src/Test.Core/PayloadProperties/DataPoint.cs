﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Test.PayloadProperties
{
    public class DataPoint : FullAuditedEntity<Guid>
    {
        [StringLength(PayloadPropertyConsts.MaxNameLength, MinimumLength = PayloadPropertyConsts.MinNameLength)]
        public virtual string Name { get; set; }

        public virtual DataTypes DataType { get; set; }

        public virtual string Description { get; set; }
    }
}
