using Abp.Authorization;
using Abp.Configuration.Startup;
using Abp.Localization;
using Abp.MultiTenancy;

namespace Test.Authorization
{
    /// <summary>
    /// Application's authorization provider.
    /// Defines permissions for the application.
    /// See <see cref="AppPermissions"/> for all permission names.
    /// </summary>
    public class AppAuthorizationProvider : AuthorizationProvider
    {
        private readonly bool _isMultiTenancyEnabled;

        public AppAuthorizationProvider(bool isMultiTenancyEnabled)
        {
            _isMultiTenancyEnabled = isMultiTenancyEnabled;
        }

        public AppAuthorizationProvider(IMultiTenancyConfig multiTenancyConfig)
        {
            _isMultiTenancyEnabled = multiTenancyConfig.IsEnabled;
        }

        public override void SetPermissions(IPermissionDefinitionContext context)
        {
            //COMMON PERMISSIONS (FOR BOTH OF TENANTS AND HOST)

            var pages = context.GetPermissionOrNull(AppPermissions.Pages) ?? context.CreatePermission(AppPermissions.Pages, L("Pages"));

            var caseActions = pages.CreateChildPermission(AppPermissions.Pages_CaseActions, L("CaseActions"));
            caseActions.CreateChildPermission(AppPermissions.Pages_CaseActions_Create, L("CreateNewCaseAction"));
            caseActions.CreateChildPermission(AppPermissions.Pages_CaseActions_Edit, L("EditCaseAction"));
            caseActions.CreateChildPermission(AppPermissions.Pages_CaseActions_Delete, L("DeleteCaseAction"));



            var caseTasks = pages.CreateChildPermission(AppPermissions.Pages_CaseTasks, L("CaseTasks"));
            caseTasks.CreateChildPermission(AppPermissions.Pages_CaseTasks_Create, L("CreateNewCaseTask"));
            caseTasks.CreateChildPermission(AppPermissions.Pages_CaseTasks_Edit, L("EditCaseTask"));
            caseTasks.CreateChildPermission(AppPermissions.Pages_CaseTasks_Delete, L("DeleteCaseTask"));
            caseTasks.CreateChildPermission(AppPermissions.Pages_CaseTasks_Reassign, L("ReassignCaseTask"));



            var caseFailedRules = pages.CreateChildPermission(AppPermissions.Pages_CaseFailedRules, L("CaseFailedRules"));
            caseFailedRules.CreateChildPermission(AppPermissions.Pages_CaseFailedRules_Create, L("CreateNewCaseFailedRule"));
            caseFailedRules.CreateChildPermission(AppPermissions.Pages_CaseFailedRules_Edit, L("EditCaseFailedRule"));
            caseFailedRules.CreateChildPermission(AppPermissions.Pages_CaseFailedRules_Delete, L("DeleteCaseFailedRule"));



            var blacklists = pages.CreateChildPermission(AppPermissions.Pages_Blacklists, L("Blacklists"));
            blacklists.CreateChildPermission(AppPermissions.Pages_Blacklists_Create, L("CreateNewBlacklist"));
            blacklists.CreateChildPermission(AppPermissions.Pages_Blacklists_Edit, L("EditBlacklist"));
            blacklists.CreateChildPermission(AppPermissions.Pages_Blacklists_Delete, L("DeleteBlacklist"));



            var whitelists = pages.CreateChildPermission(AppPermissions.Pages_Whitelists, L("Whitelists"));
            whitelists.CreateChildPermission(AppPermissions.Pages_Whitelists_Create, L("CreateNewWhitelist"));
            whitelists.CreateChildPermission(AppPermissions.Pages_Whitelists_Edit, L("EditWhitelist"));
            whitelists.CreateChildPermission(AppPermissions.Pages_Whitelists_Delete, L("DeleteWhitelist"));



            var cases = pages.CreateChildPermission(AppPermissions.Pages_Cases, L("Cases"));
            cases.CreateChildPermission(AppPermissions.Pages_Cases_Create, L("CreateNewCase"));
            cases.CreateChildPermission(AppPermissions.Pages_Cases_Edit, L("EditCase"));
            cases.CreateChildPermission(AppPermissions.Pages_Cases_Delete, L("DeleteCase"));
            cases.CreateChildPermission(AppPermissions.Pages_Cases_Close, L("Close Case"));



            var ruleEscalations = pages.CreateChildPermission(AppPermissions.Pages_RuleEscalations, L("RuleEscalations"));
            ruleEscalations.CreateChildPermission(AppPermissions.Pages_RuleEscalations_Create, L("CreateNewRuleEscalation"));
            ruleEscalations.CreateChildPermission(AppPermissions.Pages_RuleEscalations_Edit, L("EditRuleEscalation"));
            ruleEscalations.CreateChildPermission(AppPermissions.Pages_RuleEscalations_Delete, L("DeleteRuleEscalation"));



            var ruleConfigurations = pages.CreateChildPermission(AppPermissions.Pages_RuleConfigurations, L("RuleConfigurations"));
            ruleConfigurations.CreateChildPermission(AppPermissions.Pages_RuleConfigurations_Create, L("CreateNewRuleConfiguration"));
            ruleConfigurations.CreateChildPermission(AppPermissions.Pages_RuleConfigurations_Edit, L("EditRuleConfiguration"));
            ruleConfigurations.CreateChildPermission(AppPermissions.Pages_RuleConfigurations_Delete, L("DeleteRuleConfiguration"));



            var rules = pages.CreateChildPermission(AppPermissions.Pages_Rules, L("Rules"));
            rules.CreateChildPermission(AppPermissions.Pages_Rules_Create, L("CreateNewRule"));
            rules.CreateChildPermission(AppPermissions.Pages_Rules_Edit, L("EditRule"));
            rules.CreateChildPermission(AppPermissions.Pages_Rules_Delete, L("DeleteRule"));
            rules.CreateChildPermission(AppPermissions.Pages_Rules_Approve, L("ApproveRule"));
            rules.CreateChildPermission(AppPermissions.Pages_Rules_Activate, L("ActivateRule"));
        



            var payloadProperties = pages.CreateChildPermission(AppPermissions.Pages_PayloadProperties, L("PayloadProperties"));
            payloadProperties.CreateChildPermission(AppPermissions.Pages_PayloadProperties_Create, L("CreateNewPayloadProperty"));
            payloadProperties.CreateChildPermission(AppPermissions.Pages_PayloadProperties_Edit, L("EditPayloadProperty"));
            payloadProperties.CreateChildPermission(AppPermissions.Pages_PayloadProperties_Delete, L("DeletePayloadProperty"));



            var payloads = pages.CreateChildPermission(AppPermissions.Pages_Payloads, L("Payloads"));
            payloads.CreateChildPermission(AppPermissions.Pages_Payloads_Create, L("CreateNewPayload"));
            payloads.CreateChildPermission(AppPermissions.Pages_Payloads_Edit, L("EditPayload"));
            payloads.CreateChildPermission(AppPermissions.Pages_Payloads_Delete, L("DeletePayload"));



            var transactionTypes = pages.CreateChildPermission(AppPermissions.Pages_TransactionTypes, L("TransactionTypes"));
            transactionTypes.CreateChildPermission(AppPermissions.Pages_TransactionTypes_Create, L("CreateNewTransactionType"));
            transactionTypes.CreateChildPermission(AppPermissions.Pages_TransactionTypes_Edit, L("EditTransactionType"));
            transactionTypes.CreateChildPermission(AppPermissions.Pages_TransactionTypes_Delete, L("DeleteTransactionType"));



            var channels = pages.CreateChildPermission(AppPermissions.Pages_Channels, L("Channels"));
            channels.CreateChildPermission(AppPermissions.Pages_Channels_Create, L("CreateNewChannel"));
            channels.CreateChildPermission(AppPermissions.Pages_Channels_Edit, L("EditChannel"));
            channels.CreateChildPermission(AppPermissions.Pages_Channels_Delete, L("DeleteChannel"));


            pages.CreateChildPermission(AppPermissions.Pages_DemoUiComponents, L("DemoUiComponents"));

            var administration = pages.CreateChildPermission(AppPermissions.Pages_Administration, L("Administration"));

            var roles = administration.CreateChildPermission(AppPermissions.Pages_Administration_Roles, L("Roles"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Create, L("CreatingNewRole"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Edit, L("EditingRole"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Delete, L("DeletingRole"));

            var users = administration.CreateChildPermission(AppPermissions.Pages_Administration_Users, L("Users"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Create, L("CreatingNewUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Edit, L("EditingUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Delete, L("DeletingUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_ChangePermissions, L("ChangingPermissions"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Impersonation, L("LoginForUsers"));

            var languages = administration.CreateChildPermission(AppPermissions.Pages_Administration_Languages, L("Languages"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Create, L("CreatingNewLanguage"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Edit, L("EditingLanguage"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Delete, L("DeletingLanguages"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_ChangeTexts, L("ChangingTexts"));

            administration.CreateChildPermission(AppPermissions.Pages_Administration_AuditLogs, L("AuditLogs"));

            var organizationUnits = administration.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits, L("OrganizationUnits"));
            organizationUnits.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits_ManageOrganizationTree, L("ManagingOrganizationTree"));
            organizationUnits.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits_ManageMembers, L("ManagingMembers"));

            administration.CreateChildPermission(AppPermissions.Pages_Administration_UiCustomization, L("VisualSettings"));

            //TENANT-SPECIFIC PERMISSIONS

            pages.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard, L("Dashboard"), multiTenancySides: MultiTenancySides.Tenant);

            administration.CreateChildPermission(AppPermissions.Pages_Administration_Tenant_Settings, L("Settings"), multiTenancySides: MultiTenancySides.Tenant);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_Tenant_SubscriptionManagement, L("Subscription"), multiTenancySides: MultiTenancySides.Tenant);

            //HOST-SPECIFIC PERMISSIONS

            var editions = pages.CreateChildPermission(AppPermissions.Pages_Editions, L("Editions"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Create, L("CreatingNewEdition"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Edit, L("EditingEdition"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Delete, L("DeletingEdition"), multiTenancySides: MultiTenancySides.Host);

            var tenants = pages.CreateChildPermission(AppPermissions.Pages_Tenants, L("Tenants"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Create, L("CreatingNewTenant"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Edit, L("EditingTenant"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_ChangeFeatures, L("ChangingFeatures"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Delete, L("DeletingTenant"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Impersonation, L("LoginForTenants"), multiTenancySides: MultiTenancySides.Host);

            administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Settings, L("Settings"), multiTenancySides: MultiTenancySides.Host);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Maintenance, L("Maintenance"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_HangfireDashboard, L("HangfireDashboard"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Dashboard, L("Dashboard"), multiTenancySides: MultiTenancySides.Host);
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, TestConsts.LocalizationSourceName);
        }
    }
}
