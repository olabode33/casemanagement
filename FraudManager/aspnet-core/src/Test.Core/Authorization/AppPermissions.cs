namespace Test.Authorization
{
    /// <summary>
    /// Defines string constants for application's permission names.
    /// <see cref="AppAuthorizationProvider"/> for permission definitions.
    /// </summary>
    public static class AppPermissions
    {
        public const string Pages_CaseActions = "Pages.CaseActions";
        public const string Pages_CaseActions_Create = "Pages.CaseActions.Create";
        public const string Pages_CaseActions_Edit = "Pages.CaseActions.Edit";
        public const string Pages_CaseActions_Delete = "Pages.CaseActions.Delete";

        public const string Pages_CaseTasks = "Pages.CaseTasks";
        public const string Pages_CaseTasks_Create = "Pages.CaseTasks.Create";
        public const string Pages_CaseTasks_Edit = "Pages.CaseTasks.Edit";
        public const string Pages_CaseTasks_Delete = "Pages.CaseTasks.Delete";
        public const string Pages_CaseTasks_Reassign = "Pages.CaseTasks.Reassign";

        public const string Pages_CaseFailedRules = "Pages.CaseFailedRules";
        public const string Pages_CaseFailedRules_Create = "Pages.CaseFailedRules.Create";
        public const string Pages_CaseFailedRules_Edit = "Pages.CaseFailedRules.Edit";
        public const string Pages_CaseFailedRules_Delete = "Pages.CaseFailedRules.Delete";

        public const string Pages_Blacklists = "Pages.Blacklists";
        public const string Pages_Blacklists_Create = "Pages.Blacklists.Create";
        public const string Pages_Blacklists_Edit = "Pages.Blacklists.Edit";
        public const string Pages_Blacklists_Delete = "Pages.Blacklists.Delete";

        public const string Pages_Whitelists = "Pages.Whitelists";
        public const string Pages_Whitelists_Create = "Pages.Whitelists.Create";
        public const string Pages_Whitelists_Edit = "Pages.Whitelists.Edit";
        public const string Pages_Whitelists_Delete = "Pages.Whitelists.Delete";

        public const string Pages_Cases = "Pages.Cases";
        public const string Pages_Cases_Create = "Pages.Cases.Create";
        public const string Pages_Cases_Edit = "Pages.Cases.Edit";
        public const string Pages_Cases_Delete = "Pages.Cases.Delete";
        public const string Pages_Cases_Close = "Pages.Cases.Close";

        public const string Pages_RuleEscalations = "Pages.RuleEscalations";
        public const string Pages_RuleEscalations_Create = "Pages.RuleEscalations.Create";
        public const string Pages_RuleEscalations_Edit = "Pages.RuleEscalations.Edit";
        public const string Pages_RuleEscalations_Delete = "Pages.RuleEscalations.Delete";

        public const string Pages_RuleConfigurations = "Pages.RuleConfigurations";
        public const string Pages_RuleConfigurations_Create = "Pages.RuleConfigurations.Create";
        public const string Pages_RuleConfigurations_Edit = "Pages.RuleConfigurations.Edit";
        public const string Pages_RuleConfigurations_Delete = "Pages.RuleConfigurations.Delete";

        public const string Pages_Rules = "Pages.Rules";
        public const string Pages_Rules_Create = "Pages.Rules.Create";
        public const string Pages_Rules_Edit = "Pages.Rules.Edit";
        public const string Pages_Rules_Delete = "Pages.Rules.Delete";
        public const string Pages_Rules_Approve = "Pages.Rules.Approve";
        public const string Pages_Rules_Activate = "Pages.Rules.Activate";

        public const string Pages_PayloadProperties = "Pages.PayloadProperties";
        public const string Pages_PayloadProperties_Create = "Pages.PayloadProperties.Create";
        public const string Pages_PayloadProperties_Edit = "Pages.PayloadProperties.Edit";
        public const string Pages_PayloadProperties_Delete = "Pages.PayloadProperties.Delete";

        public const string Pages_Payloads = "Pages.Payloads";
        public const string Pages_Payloads_Create = "Pages.Payloads.Create";
        public const string Pages_Payloads_Edit = "Pages.Payloads.Edit";
        public const string Pages_Payloads_Delete = "Pages.Payloads.Delete";

        public const string Pages_TransactionTypes = "Pages.TransactionTypes";
        public const string Pages_TransactionTypes_Create = "Pages.TransactionTypes.Create";
        public const string Pages_TransactionTypes_Edit = "Pages.TransactionTypes.Edit";
        public const string Pages_TransactionTypes_Delete = "Pages.TransactionTypes.Delete";

        public const string Pages_Channels = "Pages.Channels";
        public const string Pages_Channels_Create = "Pages.Channels.Create";
        public const string Pages_Channels_Edit = "Pages.Channels.Edit";
        public const string Pages_Channels_Delete = "Pages.Channels.Delete";

        //COMMON PERMISSIONS (FOR BOTH OF TENANTS AND HOST)

        public const string Pages = "Pages";

        public const string Pages_DemoUiComponents= "Pages.DemoUiComponents";
        public const string Pages_Administration = "Pages.Administration";

        public const string Pages_Administration_Roles = "Pages.Administration.Roles";
        public const string Pages_Administration_Roles_Create = "Pages.Administration.Roles.Create";
        public const string Pages_Administration_Roles_Edit = "Pages.Administration.Roles.Edit";
        public const string Pages_Administration_Roles_Delete = "Pages.Administration.Roles.Delete";

        public const string Pages_Administration_Users = "Pages.Administration.Users";
        public const string Pages_Administration_Users_Create = "Pages.Administration.Users.Create";
        public const string Pages_Administration_Users_Edit = "Pages.Administration.Users.Edit";
        public const string Pages_Administration_Users_Delete = "Pages.Administration.Users.Delete";
        public const string Pages_Administration_Users_ChangePermissions = "Pages.Administration.Users.ChangePermissions";
        public const string Pages_Administration_Users_Impersonation = "Pages.Administration.Users.Impersonation";

        public const string Pages_Administration_Languages = "Pages.Administration.Languages";
        public const string Pages_Administration_Languages_Create = "Pages.Administration.Languages.Create";
        public const string Pages_Administration_Languages_Edit = "Pages.Administration.Languages.Edit";
        public const string Pages_Administration_Languages_Delete = "Pages.Administration.Languages.Delete";
        public const string Pages_Administration_Languages_ChangeTexts = "Pages.Administration.Languages.ChangeTexts";

        public const string Pages_Administration_AuditLogs = "Pages.Administration.AuditLogs";

        public const string Pages_Administration_OrganizationUnits = "Pages.Administration.OrganizationUnits";
        public const string Pages_Administration_OrganizationUnits_ManageOrganizationTree = "Pages.Administration.OrganizationUnits.ManageOrganizationTree";
        public const string Pages_Administration_OrganizationUnits_ManageMembers = "Pages.Administration.OrganizationUnits.ManageMembers";

        public const string Pages_Administration_HangfireDashboard = "Pages.Administration.HangfireDashboard";

        public const string Pages_Administration_UiCustomization = "Pages.Administration.UiCustomization";

        //TENANT-SPECIFIC PERMISSIONS

        public const string Pages_Tenant_Dashboard = "Pages.Tenant.Dashboard";

        public const string Pages_Administration_Tenant_Settings = "Pages.Administration.Tenant.Settings";

        public const string Pages_Administration_Tenant_SubscriptionManagement = "Pages.Administration.Tenant.SubscriptionManagement";

        //HOST-SPECIFIC PERMISSIONS

        public const string Pages_Editions = "Pages.Editions";
        public const string Pages_Editions_Create = "Pages.Editions.Create";
        public const string Pages_Editions_Edit = "Pages.Editions.Edit";
        public const string Pages_Editions_Delete = "Pages.Editions.Delete";

        public const string Pages_Tenants = "Pages.Tenants";
        public const string Pages_Tenants_Create = "Pages.Tenants.Create";
        public const string Pages_Tenants_Edit = "Pages.Tenants.Edit";
        public const string Pages_Tenants_ChangeFeatures = "Pages.Tenants.ChangeFeatures";
        public const string Pages_Tenants_Delete = "Pages.Tenants.Delete";
        public const string Pages_Tenants_Impersonation = "Pages.Tenants.Impersonation";

        public const string Pages_Administration_Host_Maintenance = "Pages.Administration.Host.Maintenance";
        public const string Pages_Administration_Host_Settings = "Pages.Administration.Host.Settings";
        public const string Pages_Administration_Host_Dashboard = "Pages.Administration.Host.Dashboard";

    }
}