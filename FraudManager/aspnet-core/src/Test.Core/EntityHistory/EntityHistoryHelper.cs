using Test.Blacklists;
using Test.Whitelists;
using Test.RuleEscalations;
using Test.RuleConfigurations;
using Test.Rules;
using Test.PayloadProperties;
using Test.Payloads;
using Test.TransactionTypes;
using Test.Channels;
using System;
using System.Linq;
using Abp.Organizations;
using Test.Authorization.Roles;
using Test.MultiTenancy;

namespace Test.EntityHistory
{
    public static class EntityHistoryHelper
    {
        public static readonly Type[] HostSideTrackedTypes =
        {
            typeof(OrganizationUnit), typeof(Role), typeof(Tenant)
        };

        public static readonly Type[] TenantSideTrackedTypes =
        {
            typeof(OrganizationUnit), typeof(Role)
        };

        public static readonly Type[] TrackedTypes =
            HostSideTrackedTypes
                .Concat(TenantSideTrackedTypes)
                .GroupBy(type=>type.FullName)
                .Select(types=>types.First())
                .ToArray();
    }
}
