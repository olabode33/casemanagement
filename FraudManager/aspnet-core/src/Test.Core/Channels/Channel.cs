using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;
using Abp.Auditing;

namespace Test.Channels
{
	[Table("Channels")]
    [Audited]
    public class Channel : FullAuditedEntity<Guid> 
    {

		[Required]
		[StringLength(ChannelConsts.MaxNameLength, MinimumLength = ChannelConsts.MinNameLength)]
		public virtual string Name { get; set; }
		
		public virtual string Description { get; set; }
		

    }
}