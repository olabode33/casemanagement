using Test.Rules;
using Test.PayloadProperties;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;
using Abp.Auditing;

namespace Test.RuleConfigurations
{
	[Table("RuleConfigurations")]
    [Audited]
    public class RuleConfiguration : FullAuditedEntity<Guid> 
    {

		[Required]
		public virtual string Operand { get; set; }
		
		public virtual string Connector { get; set; }
		
		[Required]
		public virtual string Value { get; set; }
		

		public virtual Guid RuleId { get; set; }
		public Rule Rule { get; set; }
		
		public virtual Guid DataPointId { get; set; }
		public DataPoint DataPoint { get; set; }

        public virtual string GroupName { get; set; }
    }
}