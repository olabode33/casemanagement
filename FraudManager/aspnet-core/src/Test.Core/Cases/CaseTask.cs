using Test;
using Test;
using Test;
using Test.Authorization.Users;
using Test.Authorization.Users;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace Test.Cases
{
	[Table("CaseTasks")]
    public class CaseTask : FullAuditedEntity 
    {

		[Required]
		[StringLength(CaseTaskConsts.MaxSubjectLength, MinimumLength = CaseTaskConsts.MinSubjectLength)]
		public virtual string Subject { get; set; }
		
		public virtual string Description { get; set; }
		
		public virtual Severity Priority { get; set; }
		
		public virtual DateTime ExpectedCompletionDate { get; set; }
		
		public virtual DateTime? ActualCompletionDate { get; set; }
		
		public virtual string Comment { get; set; }
		
		public virtual Status Status { get; set; }
		
		public virtual TaskType Type { get; set; }
		

		public virtual long? AssignerUserId { get; set; }
		public User AssignerUser { get; set; }
		
		public virtual long? AssigneeUserId { get; set; }
		public User AssigneeUser { get; set; }

        public virtual string CaseId { get; set; }
        public Alert Case { get; set; }
    }
}