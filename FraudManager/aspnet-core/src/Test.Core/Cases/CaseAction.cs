using Test.Cases;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace Test.Cases
{
	[Table("CaseActions")]
    public class CaseAction : FullAuditedEntity 
    {

		[Required]
		[StringLength(CaseActionConsts.MaxTitleLength, MinimumLength = CaseActionConsts.MinTitleLength)]
		public virtual string Title { get; set; }
		
		public virtual string Comment { get; set; }
		

		public virtual string AlertId { get; set; }
		public Alert Alert { get; set; }
		
    }
}