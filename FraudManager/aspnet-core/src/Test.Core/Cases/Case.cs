using Test;
using Test;
using Test;
using Test;
using Test;
using Test.TransactionTypes;
using Test.Authorization.Users;
using Test.Rules;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;
using Abp.Organizations;

namespace Test.Cases
{
	[Table("Cases")]
    public class Alert : FullAuditedEntity<string> 
    {

		public virtual DateTime TransactionDate { get; set; }
		
		[Required]
		public virtual string AccountNumber { get; set; }
		
		public virtual string AccountName { get; set; }
		
		public virtual string AccountType { get; set; }
		
		public virtual string ProductName { get; set; }
		
		public virtual string Description { get; set; }
		
		public virtual DectectionSource? DetectionSource { get; set; }
		
		public virtual Sources Source { get; set; }
		
		public virtual TransactionStatus TransactionStatus { get; set; }
		
		public virtual Conclusion? Conclusion { get; set; }
		
		public virtual Status Status { get; set; }
		
		public virtual string ClosureComments { get; set; }

        public virtual decimal Amount { get; set; }

        public virtual string OtherDetails { get; set; }

        public virtual Guid TransactionTypeId { get; set; }
		public TransactionType TransactionType { get; set; }
		
		public virtual long? AllocationUserId { get; set; }
		public User AllocationUser { get; set; }
		
		public virtual Guid? RuleId { get; set; }
		public Rule Rule { get; set; }

        public virtual string VerificationCode { get; set; }
        public virtual string PhoneNumber { get; set; }
        public virtual string CustomerID { get; set; }

        public virtual string OriginalPayLoad { get; set; }

        public virtual string FlowId { get; set; }

        public virtual string Anomally { get; set; }
    }
}