using Test.Cases;
using Test.Rules;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace Test.Cases
{
	[Table("CaseFailedRules")]
    public class CaseFailedRule : FullAuditedEntity 
    {


		public virtual string AlertId { get; set; }
		public Alert Alert { get; set; }
		
		public virtual Guid? RuleId { get; set; }
		public Rule Rule { get; set; }
		
    }
}