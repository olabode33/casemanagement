using Test.TransactionTypes;
using Test.Authorization.Users;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;
using Abp.Auditing;

namespace Test.Whitelists
{
	[Table("Whitelists")]
    [Audited]
    public class Whitelist : FullAuditedEntity<Guid> 
    {

		[Required]
		[StringLength(WhitelistConsts.MaxAccountNumberLength, MinimumLength = WhitelistConsts.MinAccountNumberLength)]
		public virtual string AccountNumber { get; set; }
		
		public virtual string AccountName { get; set; }
		
		public virtual string Description { get; set; }
		
		public virtual DateTime? ApprovalDate { get; set; }
		

		public virtual Guid TransactionTypeId { get; set; }
		public TransactionType TransactionType { get; set; }

        public bool IsActive { get; set; }

        public virtual long? ApproverUserId { get; set; }
		public User ApproverUser { get; set; }
		
    }
}