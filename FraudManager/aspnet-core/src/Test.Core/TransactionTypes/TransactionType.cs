using Test.Channels;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;
using Abp.Auditing;

namespace Test.TransactionTypes
{
	[Table("TransactionTypes")]
    [Audited]
    public class TransactionType : FullAuditedEntity<Guid> 
    {

		[Required]
		[StringLength(TransactionTypeConsts.MaxNameLength, MinimumLength = TransactionTypeConsts.MinNameLength)]
		public virtual string Name { get; set; }
		
		public virtual string Description { get; set; }
		

		public virtual Guid ChannelId { get; set; }
		public Channel Channel { get; set; }
		
    }
}