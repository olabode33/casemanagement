using Test;
using Test;
using Test.TransactionTypes;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;
using Abp.Auditing;

namespace Test.Rules
{
	[Table("Rules")]
    [Audited]
    public class Rule : FullAuditedEntity<Guid> 
    {

		[Required]
		[StringLength(RuleConsts.MaxNameLength, MinimumLength = RuleConsts.MinNameLength)]
		public virtual string Name { get; set; }
		
		public virtual string Description { get; set; }
		
		public virtual Severity Severity { get; set; }
		
		public virtual Actions Action { get; set; }
		

		public virtual Guid? TransactionTypeId { get; set; }
		public TransactionType TransactionType { get; set; }

        public virtual string JsonLogicRule { get; set; }
		
        public virtual decimal? Score { get; set; }
        public virtual DateTime? ApprovedDate { get; set; }
        public virtual long? ApprovedBy { get; set; }
        public virtual RuleStatus RuleStatus { get; set; }
        public virtual bool IsApproved { get; set; }
        public virtual bool Active { get; set; }
        public virtual string BaseConnector { get; set; }
    }
}