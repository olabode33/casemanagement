using Test.Rules;
using Test.Authorization.Users;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;
using Abp.Auditing;

namespace Test.RuleEscalations
{
	[Table("RuleEscalations")]
    [Audited]
    public class RuleEscalation : FullAuditedEntity<Guid> 
    {


		public virtual Guid RuleId { get; set; }
		public Rule Rule { get; set; }
		
		public virtual long UserId { get; set; }
		public User User { get; set; }
		
    }
}