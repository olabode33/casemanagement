using Test.TransactionTypes;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;
using Abp.Auditing;

namespace Test.Payloads
{
	[Table("Payloads")]
    [Audited]
    public class Payload : FullAuditedEntity<Guid> 
    {

		[Required]
		[StringLength(PayloadConsts.MaxNameLength, MinimumLength = PayloadConsts.MinNameLength)]
		public virtual string Name { get; set; }
		
		public virtual string Description { get; set; }
		

		public virtual Guid TransactionTypeId { get; set; }
		public TransactionType TransactionType { get; set; }
		
    }
}