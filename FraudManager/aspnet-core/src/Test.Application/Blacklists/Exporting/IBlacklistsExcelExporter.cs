using System.Collections.Generic;
using Test.Blacklists.Dtos;
using Test.Dto;

namespace Test.Blacklists.Exporting
{
    public interface IBlacklistsExcelExporter
    {
        FileDto ExportToFile(List<GetBlacklistForViewDto> blacklists);
    }
}