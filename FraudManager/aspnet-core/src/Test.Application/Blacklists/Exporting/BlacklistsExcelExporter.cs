using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using Test.DataExporting.Excel.EpPlus;
using Test.Blacklists.Dtos;
using Test.Dto;
using Test.Storage;

namespace Test.Blacklists.Exporting
{
    public class BlacklistsExcelExporter : EpPlusExcelExporterBase, IBlacklistsExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public BlacklistsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetBlacklistForViewDto> blacklists)
        {
            return CreateExcelPackage(
                "Blacklists.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Blacklists"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("AccountNumber"),
                        L("AccountName"),
                        L("Description"),
                        L("ApprovalDate"),
                        (L("TransactionType")) + L("Name"),
                        (L("User")) + L("Name")
                        );

                    AddObjects(
                        sheet, 2, blacklists,
                        _ => _.Blacklist.AccountNumber,
                        _ => _.Blacklist.AccountName,
                        _ => _.Blacklist.Description,
                        _ => _timeZoneConverter.Convert(_.Blacklist.ApprovalDate, _abpSession.TenantId, _abpSession.GetUserId()),
                        _ => _.TransactionTypeName,
                        _ => _.UserName
                        );

					var approvalDateColumn = sheet.Column(4);
                    approvalDateColumn.Style.Numberformat.Format = "yyyy-mm-dd";
					approvalDateColumn.AutoFit();
					

                });
        }
    }
}
