using Test.TransactionTypes;
using Test.Authorization.Users;

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Test.Blacklists.Exporting;
using Test.Blacklists.Dtos;
using Test.Dto;
using Abp.Application.Services.Dto;
using Test.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Test.Payloads.Dtos;

namespace Test.Blacklists
{
	[AbpAuthorize(AppPermissions.Pages_Blacklists)]
    public class BlacklistsAppService : TestAppServiceBase, IBlacklistsAppService
    {
		 private readonly IRepository<Blacklist, Guid> _blacklistRepository;
		 private readonly IBlacklistsExcelExporter _blacklistsExcelExporter;
		 private readonly IRepository<TransactionType,Guid> _transactionTypeRepository;
		 private readonly IRepository<User,long> _userRepository;
		 

		  public BlacklistsAppService(IRepository<Blacklist, Guid> blacklistRepository, IBlacklistsExcelExporter blacklistsExcelExporter , IRepository<TransactionType, Guid> transactionTypeRepository, IRepository<User, long> userRepository) 
		  {
			_blacklistRepository = blacklistRepository;
			_blacklistsExcelExporter = blacklistsExcelExporter;
			_transactionTypeRepository = transactionTypeRepository;
		_userRepository = userRepository;
		
		  }

		 public async Task<PagedResultDto<GetBlacklistForViewDto>> GetAll(GetAllBlacklistsInput input)
         {
			
			var filteredBlacklists = _blacklistRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.AccountNumber.Contains(input.Filter) || e.AccountName.Contains(input.Filter) || e.Description.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.AccountNumberFilter),  e => e.AccountNumber.ToLower() == input.AccountNumberFilter.ToLower().Trim());


			var query = (from o in filteredBlacklists
                         join o1 in _transactionTypeRepository.GetAll() on o.TransactionTypeId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         
                         join o2 in _userRepository.GetAll() on o.ApproverUserId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()
                         
                         select new GetBlacklistForViewDto() {
							Blacklist = ObjectMapper.Map<BlacklistDto>(o),
                         	TransactionTypeName = s1 == null ? "" : s1.Name.ToString(),
                         	UserName = s2 == null ? "" : s2.Name.ToString()
						})
						.WhereIf(!string.IsNullOrWhiteSpace(input.TransactionTypeNameFilter), e => e.TransactionTypeName.ToLower() == input.TransactionTypeNameFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.UserNameFilter), e => e.UserName.ToLower() == input.UserNameFilter.ToLower().Trim());

            var totalCount = await query.CountAsync();

            var blacklists = await query
                .OrderBy(input.Sorting ?? "blacklist.id asc")
                .PageBy(input)
                .ToListAsync();

            return new PagedResultDto<GetBlacklistForViewDto>(
                totalCount,
                blacklists
            );
         }
		 
		 public async Task<GetBlacklistForViewDto> GetBlacklistForView(Guid id)
         {
            var blacklist = await _blacklistRepository.GetAsync(id);

            var output = new GetBlacklistForViewDto { Blacklist = ObjectMapper.Map<BlacklistDto>(blacklist) };

		    if (output.Blacklist.TransactionTypeId != null)
            {
                var transactionType = await _transactionTypeRepository.FirstOrDefaultAsync((Guid)output.Blacklist.TransactionTypeId);
                output.TransactionTypeName = transactionType.Name.ToString();
            }

		    if (output.Blacklist.ApproverUserId != null)
            {
                var user = await _userRepository.FirstOrDefaultAsync((long)output.Blacklist.ApproverUserId);
                output.UserName = user.Name.ToString();
            }
			
            return output;
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_Blacklists_Edit)]
		 public async Task<GetBlacklistForEditOutput> GetBlacklistForEdit(EntityDto<Guid> input)
         {
            var blacklist = await _blacklistRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetBlacklistForEditOutput {Blacklist = ObjectMapper.Map<CreateOrEditBlacklistDto>(blacklist)};

		    if (output.Blacklist.TransactionTypeId != null)
            {
                var transactionType = await _transactionTypeRepository.FirstOrDefaultAsync((Guid)output.Blacklist.TransactionTypeId);
                output.TransactionTypeName = transactionType.Name.ToString();
            }

		    if (output.Blacklist.ApproverUserId != null)
            {
                var user = await _userRepository.FirstOrDefaultAsync((long)output.Blacklist.ApproverUserId);
                output.UserName = user.Name.ToString();
            }
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditBlacklistDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_Blacklists_Create)]
		 private async Task Create(CreateOrEditBlacklistDto input)
         {
            var blacklist = ObjectMapper.Map<Blacklist>(input);

			

            await _blacklistRepository.InsertAsync(blacklist);
         }

		 [AbpAuthorize(AppPermissions.Pages_Blacklists_Edit)]
		 private async Task Update(CreateOrEditBlacklistDto input)
         {
            var blacklist = await _blacklistRepository.FirstOrDefaultAsync((Guid)input.Id);
             ObjectMapper.Map(input, blacklist);
         }

		 [AbpAuthorize(AppPermissions.Pages_Blacklists_Delete)]
         public async Task Delete(EntityDto<Guid> input)
         {
            await _blacklistRepository.DeleteAsync(input.Id);
         } 

		public async Task<FileDto> GetBlacklistsToExcel(GetAllBlacklistsForExcelInput input)
         {
			
			var filteredBlacklists = _blacklistRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.AccountNumber.Contains(input.Filter) || e.AccountName.Contains(input.Filter) || e.Description.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.AccountNumberFilter),  e => e.AccountNumber.ToLower() == input.AccountNumberFilter.ToLower().Trim());


			var query = (from o in filteredBlacklists
                         join o1 in _transactionTypeRepository.GetAll() on o.TransactionTypeId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         
                         join o2 in _userRepository.GetAll() on o.ApproverUserId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()
                         
                         select new GetBlacklistForViewDto() { 
							Blacklist = ObjectMapper.Map<BlacklistDto>(o),
                         	TransactionTypeName = s1 == null ? "" : s1.Name.ToString(),
                         	UserName = s2 == null ? "" : s2.Name.ToString()
						 })
						.WhereIf(!string.IsNullOrWhiteSpace(input.TransactionTypeNameFilter), e => e.TransactionTypeName.ToLower() == input.TransactionTypeNameFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.UserNameFilter), e => e.UserName.ToLower() == input.UserNameFilter.ToLower().Trim());


            var blacklistListDtos = await query.ToListAsync();

            return _blacklistsExcelExporter.ExportToFile(blacklistListDtos);
         }



		[AbpAuthorize(AppPermissions.Pages_Blacklists)]
         public async Task<PagedResultDto<TransactionTypeLookupTableDto>> GetAllTransactionTypeForLookupTable(GetAllForLookupTableInput input)
         {
             var query = _transactionTypeRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                   e=> e.Name.ToString().Contains(input.Filter)
                );

            var totalCount = await query.CountAsync();

            var transactionTypeList = await query
                .PageBy(input)
                .ToListAsync();

			var lookupTableDtoList = new List<TransactionTypeLookupTableDto>();
			foreach(var transactionType in transactionTypeList){
				lookupTableDtoList.Add(new TransactionTypeLookupTableDto
				{
					Id = transactionType.Id.ToString(),
					DisplayName = transactionType.Name?.ToString()
				});
			}

            return new PagedResultDto<TransactionTypeLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
         }
    }
}