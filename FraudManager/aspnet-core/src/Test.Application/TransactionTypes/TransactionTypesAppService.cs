using Test.Channels;

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Test.TransactionTypes.Exporting;
using Test.TransactionTypes.Dtos;
using Test.Dto;
using Abp.Application.Services.Dto;
using Test.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;

namespace Test.TransactionTypes
{
	[AbpAuthorize(AppPermissions.Pages_TransactionTypes)]
    public class TransactionTypesAppService : TestAppServiceBase, ITransactionTypesAppService
    {
		 private readonly IRepository<TransactionType, Guid> _transactionTypeRepository;
		 private readonly ITransactionTypesExcelExporter _transactionTypesExcelExporter;
		 private readonly IRepository<Channel,Guid> _channelRepository;
		 

		  public TransactionTypesAppService(IRepository<TransactionType, Guid> transactionTypeRepository, ITransactionTypesExcelExporter transactionTypesExcelExporter , IRepository<Channel, Guid> channelRepository) 
		  {
			_transactionTypeRepository = transactionTypeRepository;
			_transactionTypesExcelExporter = transactionTypesExcelExporter;
			_channelRepository = channelRepository;
		
		  }

		 public async Task<PagedResultDto<GetTransactionTypeForView>> GetAll(GetAllTransactionTypesInput input)
         {
			
			var filteredTransactionTypes = _transactionTypeRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.Name.Contains(input.Filter) || e.Description.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter),  e => e.Name.ToLower() == input.NameFilter.ToLower().Trim());


			var query = (from o in filteredTransactionTypes
                         join o1 in _channelRepository.GetAll() on o.ChannelId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         
                         select new GetTransactionTypeForView() {
							TransactionType = ObjectMapper.Map<TransactionTypeDto>(o),
                         	ChannelName = s1 == null ? "" : s1.Name.ToString()
						})
						.WhereIf(!string.IsNullOrWhiteSpace(input.ChannelNameFilter), e => e.ChannelName.ToLower() == input.ChannelNameFilter.ToLower().Trim());

            var totalCount = await query.CountAsync();

            var transactionTypes = await query
                .OrderBy(input.Sorting ?? "transactionType.id asc")
                .PageBy(input)
                .ToListAsync();

            return new PagedResultDto<GetTransactionTypeForView>(
                totalCount,
                transactionTypes
            );
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_TransactionTypes_Edit)]
		 public async Task<GetTransactionTypeForEditOutput> GetTransactionTypeForEdit(EntityDto<Guid> input)
         {
            var transactionType = await _transactionTypeRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetTransactionTypeForEditOutput {TransactionType = ObjectMapper.Map<CreateOrEditTransactionTypeDto>(transactionType)};

		    if (output.TransactionType.ChannelId != null)
            {
                var channel = await _channelRepository.FirstOrDefaultAsync((Guid)output.TransactionType.ChannelId);
                output.ChannelName = channel.Name.ToString();
            }
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditTransactionTypeDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_TransactionTypes_Create)]
		 private async Task Create(CreateOrEditTransactionTypeDto input)
         {
            var transactionType = ObjectMapper.Map<TransactionType>(input);

			

            await _transactionTypeRepository.InsertAsync(transactionType);
         }

		 [AbpAuthorize(AppPermissions.Pages_TransactionTypes_Edit)]
		 private async Task Update(CreateOrEditTransactionTypeDto input)
         {
            var transactionType = await _transactionTypeRepository.FirstOrDefaultAsync((Guid)input.Id);
             ObjectMapper.Map(input, transactionType);
         }

		 [AbpAuthorize(AppPermissions.Pages_TransactionTypes_Delete)]
         public async Task Delete(EntityDto<Guid> input)
         {
            await _transactionTypeRepository.DeleteAsync(input.Id);
         } 

		public async Task<FileDto> GetTransactionTypesToExcel(GetAllTransactionTypesForExcelInput input)
         {
			
			var filteredTransactionTypes = _transactionTypeRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.Name.Contains(input.Filter) || e.Description.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter),  e => e.Name.ToLower() == input.NameFilter.ToLower().Trim());


			var query = (from o in filteredTransactionTypes
                         join o1 in _channelRepository.GetAll() on o.ChannelId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         
                         select new GetTransactionTypeForView() { 
							TransactionType = ObjectMapper.Map<TransactionTypeDto>(o),
                         	ChannelName = s1 == null ? "" : s1.Name.ToString()
						 })
						.WhereIf(!string.IsNullOrWhiteSpace(input.ChannelNameFilter), e => e.ChannelName.ToLower() == input.ChannelNameFilter.ToLower().Trim());


            var transactionTypeListDtos = await query.ToListAsync();

            return _transactionTypesExcelExporter.ExportToFile(transactionTypeListDtos);
         }



		[AbpAuthorize(AppPermissions.Pages_TransactionTypes)]
         public async Task<PagedResultDto<ChannelLookupTableDto>> GetAllChannelForLookupTable(GetAllForLookupTableInput input)
         {
             var query = _channelRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                   e=> e.Name.ToString().Contains(input.Filter)
                );

            var totalCount = await query.CountAsync();

            var channelList = await query
                .PageBy(input)
                .ToListAsync();

			var lookupTableDtoList = new List<ChannelLookupTableDto>();
			foreach(var channel in channelList){
				lookupTableDtoList.Add(new ChannelLookupTableDto
				{
					Id = channel.Id.ToString(),
					DisplayName = channel.Name?.ToString()
				});
			}

            return new PagedResultDto<ChannelLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
         }
    }
}