using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using Test.DataExporting.Excel.EpPlus;
using Test.TransactionTypes.Dtos;
using Test.Dto;
using Test.Storage;

namespace Test.TransactionTypes.Exporting
{
    public class TransactionTypesExcelExporter : EpPlusExcelExporterBase, ITransactionTypesExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public TransactionTypesExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetTransactionTypeForView> transactionTypes)
        {
            return CreateExcelPackage(
                "TransactionTypes.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("TransactionTypes"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Name"),
                        (L("Channel")) + L("Name")
                        );

                    AddObjects(
                        sheet, 2, transactionTypes,
                        _ => _.TransactionType.Name,
                        _ => _.ChannelName
                        );

					

                });
        }
    }
}
