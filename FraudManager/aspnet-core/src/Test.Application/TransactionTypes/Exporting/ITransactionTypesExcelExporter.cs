using System.Collections.Generic;
using Test.TransactionTypes.Dtos;
using Test.Dto;

namespace Test.TransactionTypes.Exporting
{
    public interface ITransactionTypesExcelExporter
    {
        FileDto ExportToFile(List<GetTransactionTypeForView> transactionTypes);
    }
}