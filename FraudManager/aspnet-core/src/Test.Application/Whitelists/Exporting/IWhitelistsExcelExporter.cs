using System.Collections.Generic;
using Test.Whitelists.Dtos;
using Test.Dto;

namespace Test.Whitelists.Exporting
{
    public interface IWhitelistsExcelExporter
    {
        FileDto ExportToFile(List<GetWhitelistForView> whitelists);
    }
}