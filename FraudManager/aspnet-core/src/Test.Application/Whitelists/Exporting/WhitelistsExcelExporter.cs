using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using Test.DataExporting.Excel.EpPlus;
using Test.Whitelists.Dtos;
using Test.Dto;
using Test.Storage;

namespace Test.Whitelists.Exporting
{
    public class WhitelistsExcelExporter : EpPlusExcelExporterBase, IWhitelistsExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public WhitelistsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetWhitelistForView> whitelists)
        {
            return CreateExcelPackage(
                "Whitelists.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Whitelists"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("AccountNumber"),
                        L("AccountName"),
                        L("Description"),
                        L("ApprovalDate"),
                        (L("TransactionType")) + L("Name"),
                        (L("User")) + L("Name")
                        );

                    AddObjects(
                        sheet, 2, whitelists,
                        _ => _.Whitelist.AccountNumber,
                        _ => _.Whitelist.AccountName,
                        _ => _.Whitelist.Description,
                        _ => _timeZoneConverter.Convert(_.Whitelist.ApprovalDate, _abpSession.TenantId, _abpSession.GetUserId()),
                        _ => _.TransactionTypeName,
                        _ => _.UserName
                        );

					var approvalDateColumn = sheet.Column(4);
                    approvalDateColumn.Style.Numberformat.Format = "yyyy-mm-dd";
					approvalDateColumn.AutoFit();
					

                });
        }
    }
}
