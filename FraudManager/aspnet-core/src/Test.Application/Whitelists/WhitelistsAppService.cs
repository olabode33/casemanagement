using Test.TransactionTypes;
using Test.Authorization.Users;

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Test.Whitelists.Exporting;
using Test.Whitelists.Dtos;
using Test.Dto;
using Abp.Application.Services.Dto;
using Test.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Abp.UI;

namespace Test.Whitelists
{
	[AbpAuthorize(AppPermissions.Pages_Whitelists)]
    public class WhitelistsAppService : TestAppServiceBase, IWhitelistsAppService
    {
		 private readonly IRepository<Whitelist, Guid> _whitelistRepository;
		 private readonly IWhitelistsExcelExporter _whitelistsExcelExporter;
		 private readonly IRepository<TransactionType,Guid> _transactionTypeRepository;
		 private readonly IRepository<User,long> _userRepository;
		 

		  public WhitelistsAppService(IRepository<Whitelist, Guid> whitelistRepository, IWhitelistsExcelExporter whitelistsExcelExporter , IRepository<TransactionType, Guid> transactionTypeRepository, IRepository<User, long> userRepository) 
		  {
			_whitelistRepository = whitelistRepository;
			_whitelistsExcelExporter = whitelistsExcelExporter;
			_transactionTypeRepository = transactionTypeRepository;
		_userRepository = userRepository;
		
		  }

		 public async Task<PagedResultDto<GetWhitelistForView>> GetAll(GetAllWhitelistsInput input)
         {
			
			var filteredWhitelists = _whitelistRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.AccountNumber.Contains(input.Filter) || e.AccountName.Contains(input.Filter) || e.Description.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.AccountNumberFilter),  e => e.AccountNumber.ToLower() == input.AccountNumberFilter.ToLower().Trim());


			var query = (from o in filteredWhitelists
                         join o1 in _transactionTypeRepository.GetAll() on o.TransactionTypeId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         
                         join o2 in _userRepository.GetAll() on o.ApproverUserId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()
                         
                         select new GetWhitelistForView() {
							Whitelist = ObjectMapper.Map<WhitelistDto>(o),
                         	TransactionTypeName = s1 == null ? "" : s1.Name.ToString(),
                         	UserName = s2 == null ? "" : s2.FullName.ToString()
						})
						.WhereIf(!string.IsNullOrWhiteSpace(input.TransactionTypeNameFilter), e => e.TransactionTypeName.ToLower() == input.TransactionTypeNameFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.UserNameFilter), e => e.UserName.ToLower() == input.UserNameFilter.ToLower().Trim());

            var totalCount = await query.CountAsync();

            var whitelists = await query
                .OrderBy(input.Sorting ?? "whitelist.id asc")
                .PageBy(input)
                .ToListAsync();

            return new PagedResultDto<GetWhitelistForView>(
                totalCount,
                whitelists
            );
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_Whitelists_Edit)]
		 public async Task<GetWhitelistForEditOutput> GetWhitelistForEdit(EntityDto<Guid> input)
         {
            var whitelist = await _whitelistRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetWhitelistForEditOutput {Whitelist = ObjectMapper.Map<CreateOrEditWhitelistDto>(whitelist)};

		    if (output.Whitelist.TransactionTypeId != null)
            {
                var transactionType = await _transactionTypeRepository.FirstOrDefaultAsync((Guid)output.Whitelist.TransactionTypeId);
                output.TransactionTypeName = transactionType.Name.ToString();
            }

		    if (output.Whitelist.ApproverUserId != null)
            {
                var user = await _userRepository.FirstOrDefaultAsync((long)output.Whitelist.ApproverUserId);
                output.UserName = user.Name.ToString();
            }
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditWhitelistDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_Whitelists_Create)]
		 private async Task Create(CreateOrEditWhitelistDto input)
         {
            var whitelist = ObjectMapper.Map<Whitelist>(input);

            whitelist.IsActive = false;

            await _whitelistRepository.InsertAsync(whitelist);
         }

		 [AbpAuthorize(AppPermissions.Pages_Whitelists_Edit)]
		 private async Task Update(CreateOrEditWhitelistDto input)
         {
            var whitelist = await _whitelistRepository.FirstOrDefaultAsync((Guid)input.Id);
             ObjectMapper.Map(input, whitelist);
         }

		 [AbpAuthorize(AppPermissions.Pages_Whitelists_Delete)]
         public async Task Delete(EntityDto<Guid> input)
         {
            await _whitelistRepository.DeleteAsync(input.Id);
         }

        [AbpAuthorize(AppPermissions.Pages_Whitelists_Edit)]
        public async Task Approve(Guid input)
        {
            var whitelist = await _whitelistRepository.FirstOrDefaultAsync(input);

            if (whitelist.CreatorUserId == AbpSession.UserId)
            {
                throw new UserFriendlyException("You can not authorize a white list you have created.");
            }

            whitelist.IsActive = true;
        }

        [AbpAuthorize(AppPermissions.Pages_Whitelists_Edit)]
        public async Task Suspend(Guid input)
        {
            var whitelist = await _whitelistRepository.FirstOrDefaultAsync(input);
            whitelist.IsActive = false;
        }

        public async Task<FileDto> GetWhitelistsToExcel(GetAllWhitelistsForExcelInput input)
         {
			
			var filteredWhitelists = _whitelistRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.AccountNumber.Contains(input.Filter) || e.AccountName.Contains(input.Filter) || e.Description.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.AccountNumberFilter),  e => e.AccountNumber.ToLower() == input.AccountNumberFilter.ToLower().Trim());


			var query = (from o in filteredWhitelists
                         join o1 in _transactionTypeRepository.GetAll() on o.TransactionTypeId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         
                         join o2 in _userRepository.GetAll() on o.ApproverUserId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()
                         
                         select new GetWhitelistForView() { 
							Whitelist = ObjectMapper.Map<WhitelistDto>(o),
                         	TransactionTypeName = s1 == null ? "" : s1.Name.ToString(),
                         	UserName = s2 == null ? "" : s2.Name.ToString()
						 })
						.WhereIf(!string.IsNullOrWhiteSpace(input.TransactionTypeNameFilter), e => e.TransactionTypeName.ToLower() == input.TransactionTypeNameFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.UserNameFilter), e => e.UserName.ToLower() == input.UserNameFilter.ToLower().Trim());


            var whitelistListDtos = await query.ToListAsync();

            return _whitelistsExcelExporter.ExportToFile(whitelistListDtos);
         }

   }
}