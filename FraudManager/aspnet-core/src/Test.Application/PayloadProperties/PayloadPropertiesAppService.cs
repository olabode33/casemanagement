using Test.Payloads;

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Test.PayloadProperties.Exporting;
using Test.PayloadProperties.Dtos;
using Test.Dto;
using Abp.Application.Services.Dto;
using Test.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Test.TransactionTypes;

namespace Test.PayloadProperties
{
    [AbpAuthorize(AppPermissions.Pages_PayloadProperties)]
    public class PayloadPropertiesAppService : TestAppServiceBase, IPayloadPropertiesAppService
    {
        private readonly IRepository<PayloadProperty, Guid> _payloadPropertyRepository;
        private readonly IRepository<DataPoint, Guid> _dataPointRepository;
        private readonly IRepository<TransactionType, Guid> _transactionTypeRepository;
        private readonly IPayloadPropertiesExcelExporter _payloadPropertiesExcelExporter;
        private readonly IRepository<Payload, Guid> _payloadRepository;


        public PayloadPropertiesAppService(IRepository<TransactionType, Guid> _transactionTypeRepository,
            IRepository<DataPoint, Guid> dataPointRepository,
            IRepository<PayloadProperty, Guid> payloadPropertyRepository,
            IPayloadPropertiesExcelExporter payloadPropertiesExcelExporter, IRepository<Payload, Guid> payloadRepository)
        {
            _payloadPropertyRepository = payloadPropertyRepository;
            _payloadPropertiesExcelExporter = payloadPropertiesExcelExporter;
            _dataPointRepository = dataPointRepository;
            _payloadRepository = payloadRepository;

        }

        public async Task<PagedResultDto<GetPayloadPropertyForView>> GetAll(GetAllPayloadPropertiesInput input)
        {
            var dataTypeFilter = (DataTypes)input.DataTypeFilter;

            var filteredPayloadProperties = _payloadPropertyRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter) || e.Description.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name.ToLower() == input.NameFilter.ToLower().Trim())
                        .WhereIf(input.DataTypeFilter > -1, e => e.DataType == dataTypeFilter);


            var query = (from o in filteredPayloadProperties
                         join o1 in _payloadRepository.GetAll() on o.PayloadId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()

                         select new GetPayloadPropertyForView()
                         {
                             PayloadProperty = ObjectMapper.Map<PayloadPropertyDto>(o),
                             PayloadName = s1 == null ? "" : s1.Name.ToString()
                         })
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PayloadNameFilter), e => e.PayloadName.ToLower() == input.PayloadNameFilter.ToLower().Trim());

            var totalCount = await query.CountAsync();

            var payloadProperties = await query
                .OrderBy(input.Sorting ?? "payloadProperty.id asc")
                .PageBy(input)
                .ToListAsync();

            return new PagedResultDto<GetPayloadPropertyForView>(
                totalCount,
                payloadProperties
            );
        }

        [AbpAuthorize(AppPermissions.Pages_PayloadProperties_Edit)]
        public async Task<GetPayloadPropertyForEditOutput> GetPayloadPropertyForEdit(EntityDto<Guid> input)
        {
            var payloadProperty = await _payloadPropertyRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetPayloadPropertyForEditOutput { PayloadProperty = ObjectMapper.Map<CreateOrEditPayloadPropertyDto>(payloadProperty) };

            if (output.PayloadProperty.PayloadId != null)
            {
                var payload = await _payloadRepository.FirstOrDefaultAsync((Guid)output.PayloadProperty.PayloadId);
                output.PayloadName = payload.Name.ToString();
            }

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditPayloadPropertyDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_PayloadProperties_Create)]
        private async Task Create(CreateOrEditPayloadPropertyDto input)
        {
            var payloadProperty = ObjectMapper.Map<PayloadProperty>(input);



            await _payloadPropertyRepository.InsertAsync(payloadProperty);
        }

        [AbpAuthorize(AppPermissions.Pages_PayloadProperties_Edit)]
        private async Task Update(CreateOrEditPayloadPropertyDto input)
        {
            var payloadProperty = await _payloadPropertyRepository.FirstOrDefaultAsync((Guid)input.Id);
            ObjectMapper.Map(input, payloadProperty);
        }

        [AbpAuthorize(AppPermissions.Pages_PayloadProperties_Delete)]
        public async Task Delete(EntityDto<Guid> input)
        {
            await _payloadPropertyRepository.DeleteAsync(input.Id);
        }

        public async Task<FileDto> GetPayloadPropertiesToExcel(GetAllPayloadPropertiesForExcelInput input)
        {
            var dataTypeFilter = (DataTypes)input.DataTypeFilter;

            var filteredPayloadProperties = _payloadPropertyRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter) || e.Description.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name.ToLower() == input.NameFilter.ToLower().Trim())
                        .WhereIf(input.DataTypeFilter > -1, e => e.DataType == dataTypeFilter);


            var query = (from o in filteredPayloadProperties
                         join o1 in _payloadRepository.GetAll() on o.PayloadId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()

                         select new GetPayloadPropertyForView()
                         {
                             PayloadProperty = ObjectMapper.Map<PayloadPropertyDto>(o),
                             PayloadName = s1 == null ? "" : s1.Name.ToString()
                         })
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PayloadNameFilter), e => e.PayloadName.ToLower() == input.PayloadNameFilter.ToLower().Trim());


            var payloadPropertyListDtos = await query.ToListAsync();

            return _payloadPropertiesExcelExporter.ExportToFile(payloadPropertyListDtos);
        }

        [AbpAuthorize(AppPermissions.Pages_PayloadProperties)]
        public async Task<PagedResultDto<PayloadLookupTableDto>> GetAllDataPointForLookupTable(GetAllForLookupTableInput input)
        {

            var query = _dataPointRepository.GetAll()
                .Where(x => x.GetType().Name == "DataPoint")
                .WhereIf(
                   !string.IsNullOrWhiteSpace(input.Filter),
                  e => e.Name.ToString().Contains(input.Filter)
               );

            var dataPoints = await query
                .ToListAsync();

            var lookupTableDtoList = new List<PayloadLookupTableDto>();
            foreach (var dataPoint in dataPoints)
            {
                lookupTableDtoList.Add(new PayloadLookupTableDto
                {
                    Id = dataPoint.Id.ToString(),
                    DisplayName = dataPoint.Name?.ToString(),
                    Description = dataPoint.Description,
                    DataType = ((int)dataPoint.DataType).ToString()
                });
            }


            var querypayloadProperty = _payloadPropertyRepository.GetAll().Include(x => x.Payload)
.Where( e => e.Payload.TransactionTypeId == input.TransactionType)
     .WhereIf(
        !string.IsNullOrWhiteSpace(input.Filter),
       e => e.Name.ToString().Contains(input.Filter)
    );

            var payloadProperties = await querypayloadProperty
                .ToListAsync();

            foreach (var payloadProperty in payloadProperties)
            {
                lookupTableDtoList.Add(new PayloadLookupTableDto
                {
                    Id = payloadProperty.Id.ToString(),
                    DisplayName = payloadProperty.Name?.ToString(),
                    Description = payloadProperty.Description,
                    DataType = ((int)payloadProperty.DataType).ToString()
                });
            }

            return new PagedResultDto<PayloadLookupTableDto>(
                payloadProperties.Count() + dataPoints.Count(),
                lookupTableDtoList.Skip(input.SkipCount).Take(input.MaxResultCount).ToList()
            );
        }


        [AbpAuthorize(AppPermissions.Pages_PayloadProperties)]
        public async Task<PagedResultDto<PayloadLookupTableDto>> GetAllPayloadForLookupTable(GetAllForLookupTableInput input)
        {
            var query = _payloadRepository.GetAll().WhereIf(
                   !string.IsNullOrWhiteSpace(input.Filter),
                  e => e.Name.ToString().Contains(input.Filter)
               );

            var totalCount = await query.CountAsync();

            var payloadList = await query
                .PageBy(input)
                .ToListAsync();

            var lookupTableDtoList = new List<PayloadLookupTableDto>();
            foreach (var payload in payloadList)
            {
                lookupTableDtoList.Add(new PayloadLookupTableDto
                {
                    Id = payload.Id.ToString(),
                    DisplayName = payload.Name?.ToString()
                });
            }

            return new PagedResultDto<PayloadLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
        }
    }
}