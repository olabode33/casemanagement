using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using Test.DataExporting.Excel.EpPlus;
using Test.PayloadProperties.Dtos;
using Test.Dto;
using Test.Storage;

namespace Test.PayloadProperties.Exporting
{
    public class PayloadPropertiesExcelExporter : EpPlusExcelExporterBase, IPayloadPropertiesExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public PayloadPropertiesExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetPayloadPropertyForView> payloadProperties)
        {
            return CreateExcelPackage(
                "PayloadProperties.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("PayloadProperties"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("DataType"),
                        (L("Payload")) + L("Name")
                        );

                    AddObjects(
                        sheet, 2, payloadProperties,
                        _ => _.PayloadProperty.Name,
                        _ => _.PayloadProperty.DataType,
                        _ => _.PayloadName
                        );

					

                });
        }
    }
}
