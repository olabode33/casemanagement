using System.Collections.Generic;
using Test.PayloadProperties.Dtos;
using Test.Dto;

namespace Test.PayloadProperties.Exporting
{
    public interface IPayloadPropertiesExcelExporter
    {
        FileDto ExportToFile(List<GetPayloadPropertyForView> payloadProperties);
    }
}