﻿using System.Collections.Generic;
using Test.Chat.Dto;
using Test.Dto;

namespace Test.Chat.Exporting
{
    public interface IChatMessageListExcelExporter
    {
        FileDto ExportToFile(List<ChatMessageExportDto> messages);
    }
}
