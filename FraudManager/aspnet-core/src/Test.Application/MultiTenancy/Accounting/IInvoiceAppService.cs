﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Test.MultiTenancy.Accounting.Dto;

namespace Test.MultiTenancy.Accounting
{
    public interface IInvoiceAppService
    {
        Task<InvoiceDto> GetInvoiceInfo(EntityDto<long> input);

        Task CreateInvoice(CreateInvoiceDto input);
    }
}
