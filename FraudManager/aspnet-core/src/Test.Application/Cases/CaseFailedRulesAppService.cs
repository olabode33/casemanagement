using Test.Cases;
using Test.Rules;

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Test.Cases.Dtos;
using Test.Dto;
using Abp.Application.Services.Dto;
using Test.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Test.RuleConfigurations.Dtos;

namespace Test.Cases
{
	[AbpAuthorize(AppPermissions.Pages_CaseFailedRules)]
    public class CaseFailedRulesAppService : TestAppServiceBase, ICaseFailedRulesAppService
    {
		 private readonly IRepository<CaseFailedRule> _caseFailedRuleRepository;
		 private readonly IRepository<Alert,string> _alertRepository;
		 private readonly IRepository<Rule,Guid> _ruleRepository;
		 

		  public CaseFailedRulesAppService(IRepository<CaseFailedRule> caseFailedRuleRepository , IRepository<Alert, string> alertRepository, IRepository<Rule, Guid> ruleRepository) 
		  {
			_caseFailedRuleRepository = caseFailedRuleRepository;
			_alertRepository = alertRepository;
		_ruleRepository = ruleRepository;
		
		  }

		 public async Task<PagedResultDto<GetCaseFailedRuleForViewDto>> GetAll(GetAllCaseFailedRulesInput input)
         {
			
			var filteredCaseFailedRules = _caseFailedRuleRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false );


			var query = (from o in filteredCaseFailedRules
                         join o1 in _alertRepository.GetAll() on o.AlertId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         
                         join o2 in _ruleRepository.GetAll() on o.RuleId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()
                         
                         select new GetCaseFailedRuleForViewDto() {
							CaseFailedRule = ObjectMapper.Map<CaseFailedRuleDto>(o),
                         	AlertAccountNumber = s1 == null ? "" : s1.AccountNumber.ToString(),
                         	RuleName = s2 == null ? "" : s2.Name.ToString()
						})
						.WhereIf(!string.IsNullOrWhiteSpace(input.AlertAccountNumberFilter), e => e.AlertAccountNumber.ToLower() == input.AlertAccountNumberFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.RuleNameFilter), e => e.RuleName.ToLower() == input.RuleNameFilter.ToLower().Trim());

            var totalCount = await query.CountAsync();

            var caseFailedRules = await query
                .OrderBy(input.Sorting ?? "caseFailedRule.id asc")
                .PageBy(input)
                .ToListAsync();

            return new PagedResultDto<GetCaseFailedRuleForViewDto>(
                totalCount,
                caseFailedRules
            );
         }
		 
		 public async Task<GetCaseFailedRuleForViewDto> GetCaseFailedRuleForView(int id)
         {
            var caseFailedRule = await _caseFailedRuleRepository.GetAsync(id);

            var output = new GetCaseFailedRuleForViewDto { CaseFailedRule = ObjectMapper.Map<CaseFailedRuleDto>(caseFailedRule) };

		    if (output.CaseFailedRule.AlertId != null)
            {
                var alert = await _alertRepository.FirstOrDefaultAsync((string)output.CaseFailedRule.AlertId);
                output.AlertAccountNumber = alert.AccountNumber.ToString();
            }

		    if (output.CaseFailedRule.RuleId != null)
            {
                var rule = await _ruleRepository.FirstOrDefaultAsync((Guid)output.CaseFailedRule.RuleId);
                output.RuleName = rule.Name.ToString();
            }
			
            return output;
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_CaseFailedRules_Edit)]
		 public async Task<GetCaseFailedRuleForEditOutput> GetCaseFailedRuleForEdit(EntityDto input)
         {
            var caseFailedRule = await _caseFailedRuleRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetCaseFailedRuleForEditOutput {CaseFailedRule = ObjectMapper.Map<CreateOrEditCaseFailedRuleDto>(caseFailedRule)};

		    if (output.CaseFailedRule.AlertId != null)
            {
                var alert = await _alertRepository.FirstOrDefaultAsync((string)output.CaseFailedRule.AlertId);
                output.AlertAccountNumber = alert.AccountNumber.ToString();
            }

		    if (output.CaseFailedRule.RuleId != null)
            {
                var rule = await _ruleRepository.FirstOrDefaultAsync((Guid)output.CaseFailedRule.RuleId);
                output.RuleName = rule.Name.ToString();
            }
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditCaseFailedRuleDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_CaseFailedRules_Create)]
		 private async Task Create(CreateOrEditCaseFailedRuleDto input)
         {
            var caseFailedRule = ObjectMapper.Map<CaseFailedRule>(input);

			

            await _caseFailedRuleRepository.InsertAsync(caseFailedRule);
         }

		 [AbpAuthorize(AppPermissions.Pages_CaseFailedRules_Edit)]
		 private async Task Update(CreateOrEditCaseFailedRuleDto input)
         {
            var caseFailedRule = await _caseFailedRuleRepository.FirstOrDefaultAsync((int)input.Id);
             ObjectMapper.Map(input, caseFailedRule);
         }

		 [AbpAuthorize(AppPermissions.Pages_CaseFailedRules_Delete)]
         public async Task Delete(EntityDto input)
         {
            await _caseFailedRuleRepository.DeleteAsync(input.Id);
         } 

		[AbpAuthorize(AppPermissions.Pages_CaseFailedRules)]
         public async Task<PagedResultDto<AlertLookupTableDto>> GetAllAlertForLookupTable(GetAllForLookupTableInput input)
         {
             var query = _alertRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                   e=> e.AccountNumber.ToString().Contains(input.Filter)
                );

            var totalCount = await query.CountAsync();

            var alertList = await query
                .PageBy(input)
                .ToListAsync();

			var lookupTableDtoList = new List<AlertLookupTableDto>();
			foreach(var alert in alertList){
				lookupTableDtoList.Add(new AlertLookupTableDto
				{
					Id = alert.Id,
					DisplayName = alert.AccountNumber?.ToString()
				});
			}

            return new PagedResultDto<AlertLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
         }

		[AbpAuthorize(AppPermissions.Pages_CaseFailedRules)]
         public async Task<PagedResultDto<RuleLookupTableDto>> GetAllRuleForLookupTable(GetAllForLookupTableInput input)
         {
             var query = _ruleRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                   e=> e.Name.ToString().Contains(input.Filter)
                );

            var totalCount = await query.CountAsync();

            var ruleList = await query
                .PageBy(input)
                .ToListAsync();

			var lookupTableDtoList = new List<RuleLookupTableDto>();
			foreach(var rule in ruleList){
				lookupTableDtoList.Add(new RuleLookupTableDto
				{
					Id = rule.Id.ToString(),
					DisplayName = rule.Name?.ToString()
				});
			}

            return new PagedResultDto<RuleLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
         }
    }
}