﻿using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Events.Bus;
using Abp.Events.Bus.Handlers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Test.Authorization.Users;
using Test.Cases.Dtos;

namespace Test.Cases
{
    public class AssignCaseToUserHandler : IEventHandler<AssignCaseToUserDto>, ITransientDependency
    {
        private readonly IRepository<CaseTask> _caseTaskRepository;
        private readonly IRepository<Alert, string> _caseRepository;
        private readonly IRepository<User, long> _userRepository;

        public AssignCaseToUserHandler(
            IRepository<CaseTask> caseTaskRepository, 
            IRepository<Alert, string> caseRepository,
            IRepository<User, long> userRepository)
        {
            _caseTaskRepository = caseTaskRepository;
            _caseRepository = caseRepository;
            _userRepository = userRepository;
        }

        public void HandleEvent(AssignCaseToUserDto eventData)
        {
            var users = _userRepository.GetAllList();
            var autoTask = _caseTaskRepository.GetAllList(x => x.Type == TaskType.Auto);
            var sortedTaks = autoTask.OrderByDescending(x => x.CreationTime).ToList();
            long lastAssignedUser = 1;


            if (autoTask != null)
            {
                var lastUserid = sortedTaks[0].AssigneeUserId;

                var lastIndex = users.FindIndex(x => x.Id == lastUserid);
                if (lastIndex + 1 < users.Count)
                {
                    lastAssignedUser = users[lastIndex + 1].Id;
                }
                else
                {
                    lastAssignedUser = users[0].Id;
                }
            }
            else
            {
                //lastAssignedUser = users[0].Id;
            }


            var newTask = new CaseTask
            {
                AssigneeUserId = lastAssignedUser,
                Description = "Auto-assigned",
                ExpectedCompletionDate = DateTime.Now.Add(TimeSpan.FromDays(7)),
                Priority = Severity.High,
                Status = Status.Open,
                Subject = "Attend to new case.",
                Type = TaskType.Auto,
                CaseId = eventData.CaseId
            };

            _caseTaskRepository.Insert(newTask);
        }
    }
}
