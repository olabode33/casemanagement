﻿using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Threading.BackgroundWorkers;
using Abp.Threading.Timers;
using Abp.Timing;
using System;
using System.Collections.Generic;
using System.Text;

namespace Test.Cases
{
    public class UpdateCaseStatusWorker: PeriodicBackgroundWorkerBase, ISingletonDependency
    {
        private readonly IRepository<Alert, string> _casesRepository;

        public UpdateCaseStatusWorker(AbpTimer timer, IRepository<Alert, string> casesRepository)
            :base(timer)
        {
            _casesRepository = casesRepository;
            Timer.Period = 300000;
        }

        [UnitOfWork]
        protected override void DoWork()
        {
            Logger.Debug("Running Background Worker: " + DateTime.Now);
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                var fiveMinutesAgo = Clock.Now.Subtract(TimeSpan.FromMinutes(5));

                var verifyingCases = _casesRepository.GetAllList(c =>
                    c.Status == Status.Verifying &&
                    (c.LastModificationTime < fiveMinutesAgo && c.LastModificationTime != null)
                );

                foreach (var verifyingCase in verifyingCases)
                {
                    verifyingCase.Status = Status.Open;
                    Logger.Debug("LastModificationTime: " + verifyingCase.LastModificationTime + " | FiveMinuteAgo: " + fiveMinutesAgo);
                }

                CurrentUnitOfWork.SaveChanges();
            }
        }
    }
}
