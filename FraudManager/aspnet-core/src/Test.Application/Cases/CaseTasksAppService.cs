using Test.Authorization.Users;
using Test.Authorization.Users;

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Test.Cases.Dtos;
using Test.Dto;
using Abp.Application.Services.Dto;
using Test.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Test.RuleEscalations.Dtos;
using Abp.UI;

namespace Test.Cases
{
    [AbpAuthorize(AppPermissions.Pages_CaseTasks)]
    public class CaseTasksAppService : TestAppServiceBase, ICaseTasksAppService
    {
        private readonly IRepository<CaseTask> _caseTaskRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<Alert, string> _caseRepository;


        public CaseTasksAppService(
            IRepository<CaseTask> caseTaskRepository, 
            IRepository<User, long> userRepository,
            IRepository<Alert, string> caseRepository)
        {
            _caseTaskRepository = caseTaskRepository;
            _userRepository = userRepository;
            _caseRepository = caseRepository;
        }

        public async Task<PagedResultDto<GetCaseTaskForViewDto>> GetAll(GetAllCaseTasksInput input)
        {
            var priorityFilter = (Severity)input.PriorityFilter;
            var statusFilter = (Status)input.StatusFilter;
            var typeFilter = (TaskType)input.TypeFilter;

            var filteredCaseTasks = _caseTaskRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Subject.Contains(input.Filter) || e.Description.Contains(input.Filter) || e.Comment.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.SubjectFilter), e => e.Subject.ToLower() == input.SubjectFilter.ToLower().Trim())
                        .WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter), e => e.Description.ToLower() == input.DescriptionFilter.ToLower().Trim())
                        .WhereIf(input.PriorityFilter > -1, e => e.Priority == priorityFilter)
                        .WhereIf(input.MinExpectedCompletionDateFilter != null, e => e.ExpectedCompletionDate >= input.MinExpectedCompletionDateFilter)
                        .WhereIf(input.MaxExpectedCompletionDateFilter != null, e => e.ExpectedCompletionDate <= input.MaxExpectedCompletionDateFilter)
                        .WhereIf(input.MinActualCompletionDateFilter != null, e => e.ActualCompletionDate >= input.MinActualCompletionDateFilter)
                        .WhereIf(input.MaxActualCompletionDateFilter != null, e => e.ActualCompletionDate <= input.MaxActualCompletionDateFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.CommentFilter), e => e.Comment.ToLower() == input.CommentFilter.ToLower().Trim())
                        .WhereIf(input.StatusFilter > -1, e => e.Status == statusFilter)
                        .WhereIf(input.TypeFilter > -1, e => e.Type == typeFilter);


            var query = (from o in filteredCaseTasks
                         join o1 in _userRepository.GetAll() on o.AssignerUserId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()

                         join o2 in _userRepository.GetAll() on o.AssigneeUserId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()

                         select new GetCaseTaskForViewDto()
                         {
                             CaseTask = ObjectMapper.Map<CaseTaskDto>(o),
                             AssignerUsername = s1 == null ? "" : s1.FullName.ToString(),
                             AssigneeUsername = s2 == null ? "" : s2.FullName.ToString()
                         })
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AssignerFilter), e => e.AssignerUsername.ToLower() == input.AssignerFilter.ToLower().Trim())
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AssigneeFilter), e => e.AssigneeUsername.ToLower() == input.AssigneeFilter.ToLower().Trim());

            var totalCount = await query.CountAsync();

            var caseTasks = await query
                .OrderBy(input.Sorting ?? "caseTask.id asc")
                .PageBy(input)
                .ToListAsync();

            return new PagedResultDto<GetCaseTaskForViewDto>(
                totalCount,
                caseTasks
            );
        }

        public async Task<GetCaseTaskForViewDto> GetCaseTaskForView(int id)
        {
            var caseTask = await _caseTaskRepository.GetAsync(id);

            var output = new GetCaseTaskForViewDto { CaseTask = ObjectMapper.Map<CaseTaskDto>(caseTask) };

            if (output.CaseTask.AssignerUserId != null)
            {
                var user = await _userRepository.FirstOrDefaultAsync((long)output.CaseTask.AssignerUserId);
                output.AssignerUsername = user.Name.ToString();
            }

            if (output.CaseTask.AssigneeUserId != null)
            {
                var user = await _userRepository.FirstOrDefaultAsync((long)output.CaseTask.AssigneeUserId);
                output.AssigneeUsername = user.Name.ToString();
            }

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_CaseTasks_Edit)]
        public async Task<GetCaseTaskForEditOutput> GetCaseTaskForEdit(EntityDto input)
        {
            var caseTask = await _caseTaskRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetCaseTaskForEditOutput { CaseTask = ObjectMapper.Map<CreateOrEditCaseTaskDto>(caseTask) };

            if (output.CaseTask.AssignerUserId != null)
            {
                var user = await _userRepository.FirstOrDefaultAsync((long)output.CaseTask.AssignerUserId);
                output.UserName = user.FullName.ToString();
            }
            else
            {
                output.UserName = "System";
            }

            if (output.CaseTask.AssigneeUserId != null)
            {
                var user = await _userRepository.FirstOrDefaultAsync((long)output.CaseTask.AssigneeUserId);
                output.UserName2 = user.Name.ToString();
            }

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditCaseTaskDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_CaseTasks_Create)]
        private async Task Create(CreateOrEditCaseTaskDto input)
        {
            if (input.CaseId == null)
            {
                throw new UserFriendlyException("Task must be assigned to a case!");
            }
            if (input.AssigneeUserId == AbpSession.UserId)
            {
                throw new UserFriendlyException("You cannot assign a task to yourself!");
            }

            var caseTask = ObjectMapper.Map<CaseTask>(input);
            caseTask.AssignerUserId = AbpSession.UserId;


            await _caseTaskRepository.InsertAsync(caseTask);
        }

        [AbpAuthorize(AppPermissions.Pages_CaseTasks_Edit)]
        private async Task Update(CreateOrEditCaseTaskDto input)
        {
            var caseTask = await _caseTaskRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, caseTask);
        }

        public async Task CompleteTask(CreateOrEditCaseTaskDto input)
        {
            if (input.AssigneeUserId != AbpSession.UserId)
            {
                throw new UserFriendlyException("Task can only be completed by the user that was assigned to it, You can re-assign the task if the user is not available");
            }

            var caseTask = await _caseTaskRepository.FirstOrDefaultAsync((int)input.Id);
            input.ActualCompletionDate = DateTime.Now;
            input.Status = Status.Closed;

            ObjectMapper.Map(input, caseTask);

            var alert = await _caseRepository.FirstOrDefaultAsync(input.CaseId);

            alert.Status = Status.Verifying;
            ObjectMapper.Map(alert, alert);
        }

        //public async Task ReassignTask(CreateOrEditCaseTaskDto input)
        //{
        //    //Add Check before reassignment

        //    //Close current Task
        //    var assignee = await _userRepository.FirstOrDefaultAsync((long)input.AssigneeUserId);
        //    input.Comment = "Task Reassigned to " + assignee.FullName + " (" + assignee.EmailAddress + ")";
        //    await CompleteTask(input);

        //    //Create new task
        //    await Create(new CreateOrEditCaseTaskDto
        //    {
        //        AssigneeUserId = input.AssigneeUserId,
        //        CaseId = input.CaseId,
        //        Description = "Reassigned Task: " + input.Description,
        //        ExpectedCompletionDate = input.ExpectedCompletionDate,
        //        Priority = input.Priority,
        //        Status = Status.Open,
        //        Subject = "Reassigned: " + input.Subject,
        //        Type = TaskType.Reassigned
        //    });
        //}

        [AbpAuthorize(AppPermissions.Pages_CaseTasks_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _caseTaskRepository.DeleteAsync(input.Id);
        }

        [AbpAuthorize(AppPermissions.Pages_CaseTasks)]
        public async Task<PagedResultDto<UserLookupTableDto>> GetAllUserForLookupTable(GetAllForLookupTableInput input)
        {
            var query = _userRepository.GetAll().WhereIf(
                   !string.IsNullOrWhiteSpace(input.Filter),
                  e => e.Name.ToString().Contains(input.Filter)
               );

            var totalCount = await query.CountAsync();

            var userList = await query
                .PageBy(input)
                .ToListAsync();

            var lookupTableDtoList = new List<UserLookupTableDto>();
            foreach (var user in userList)
            {
                lookupTableDtoList.Add(new UserLookupTableDto
                {
                    Id = user.Id,
                    DisplayName = user.Name?.ToString()
                });
            }

            return new PagedResultDto<UserLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
        }
    }
}