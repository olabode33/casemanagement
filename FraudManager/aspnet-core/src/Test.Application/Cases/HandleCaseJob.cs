﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Events.Bus;
using System;
using System.Collections.Generic;
using System.Text;
using Test.Cases.Dtos;

namespace Test.Cases
{
    public class HandleCaseJob : BackgroundJob<HandleCaseArgs>, ITransientDependency
    {
        public override void Execute(HandleCaseArgs args)
        {
            EventBus.Default.Trigger(this, new RecieveCaseEventData() { Payload = args.Payload });
        }
    }
}
