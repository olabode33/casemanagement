using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using Test.DataExporting.Excel.EpPlus;
using Test.Cases.Dtos;
using Test.Dto;
using Test.Storage;

namespace Test.Cases.Exporting
{
    public class CasesExcelExporter : EpPlusExcelExporterBase, ICasesExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public CasesExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetCaseForView> cases)
        {
            return CreateExcelPackage(
                "Cases.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Cases"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("TransactionDate"),
                        L("AccountNumber"),
                        L("AccountName"),
                        L("AccountType"),
                        L("ProductName"),
                        L("Description"),
                        L("DetectionSource"),
                        L("Source"),
                        L("TransactionStatus"),
                        L("Conclusion"),
                        L("Status"),
                        L("ClosureComments"),
                        (L("TransactionType")) + L("Name"),
                        (L("User")) + L("Name"),
                        (L("Rule")) + L("Name")
                        );

                    AddObjects(
                        sheet, 2, cases,
                        _ => _timeZoneConverter.Convert(_.Case.TransactionDate, _abpSession.TenantId, _abpSession.GetUserId()),
                        _ => _.Case.AccountNumber,
                        _ => _.Case.AccountName,
                        _ => _.Case.AccountType,
                        _ => _.Case.ProductName,
                        _ => _.Case.Description,
                        _ => _.Case.DetectionSource,
                        _ => _.Case.Source,
                        _ => _.Case.TransactionStatus,
                        _ => _.Case.Conclusion,
                        _ => _.Case.Status,
                        _ => _.Case.ClosureComments,
                        _ => _.TransactionTypeName,
                        _ => _.UserName,
                        _ => _.RuleName
                        );

					var transactionDateColumn = sheet.Column(1);
                    transactionDateColumn.Style.Numberformat.Format = "yyyy-mm-dd";
					transactionDateColumn.AutoFit();
					

                });
        }
    }
}
