using System.Collections.Generic;
using Test.Cases.Dtos;
using Test.Dto;

namespace Test.Cases.Exporting
{
    public interface ICasesExcelExporter
    {
        FileDto ExportToFile(List<GetCaseForView> cases);
    }
}