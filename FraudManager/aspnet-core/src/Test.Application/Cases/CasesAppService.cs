using Test.TransactionTypes;
using Test.Authorization.Users;
using Test.Rules;

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Test.Cases.Exporting;
using Test.Cases.Dtos;
using Test.Dto;
using Abp.Application.Services.Dto;
using Test.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using Test.PhoneCall;
using Test.PhoneCall.Dtos;
using System.Net;
using System.Text;
using System.IO;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Hosting;
using Test.Configuration;
using Test.Common;

namespace Test.Cases
{
    //[AbpAuthorize(AppPermissions.Pages_Cases)]
    public class CasesAppService : TestAppServiceBase, ICasesAppService
    {
        private readonly IRepository<Alert, string> _caseRepository;
        private readonly ICasesExcelExporter _casesExcelExporter;
        private readonly IRepository<TransactionType, Guid> _transactionTypeRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<Rule, Guid> _ruleRepository;
        private readonly IRepository<CaseFailedRule> _caseFailedRuleRepository;
        private readonly IRepository<CaseTask> _caseTaskRepository;
        private readonly IRepository<CaseAction> _caseActionRepository;
        private readonly IConfigurationRoot _appConfiguration;


        public CasesAppService(
            IRepository<Alert, string> caseRepository, 
            ICasesExcelExporter casesExcelExporter, 
            IRepository<TransactionType, Guid> transactionTypeRepository, 
            IRepository<User, long> userRepository, 
            IRepository<Rule, Guid> ruleRepository,
            IHostingEnvironment env,
            IRepository<CaseFailedRule> caseFailedRuleRepository,
            IRepository<CaseTask> caseTaskRepository,
            IRepository<CaseAction> caseActionRepository)
        {
            _caseRepository = caseRepository;
            _casesExcelExporter = casesExcelExporter;
            _transactionTypeRepository = transactionTypeRepository;
            _userRepository = userRepository;
            _ruleRepository = ruleRepository;
            _appConfiguration = env.GetAppConfiguration();
            _caseFailedRuleRepository = caseFailedRuleRepository;
            _caseTaskRepository = caseTaskRepository;
            _caseActionRepository = caseActionRepository;
        }

        public async Task<PagedResultDto<GetCaseForView>> GetAll(GetAllCasesInput input)
        {
            var detectionSourceFilter = (DectectionSource)input.DetectionSourceFilter;
            var sourceFilter = (Sources)input.SourceFilter;
            var transactionStatusFilter = (TransactionStatus)input.TransactionStatusFilter;
            var conclusionFilter = (Conclusion)input.ConclusionFilter;
            var statusFilter = (Status)input.StatusFilter;

            var filteredCases = _caseRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.AccountNumber.Contains(input.Filter) || e.AccountName.Contains(input.Filter) || e.AccountType.Contains(input.Filter) || e.ProductName.Contains(input.Filter) || e.Description.Contains(input.Filter) || e.ClosureComments.Contains(input.Filter))
                        .WhereIf(input.MinTransactionDateFilter != null, e => e.TransactionDate >= input.MinTransactionDateFilter)
                        .WhereIf(input.MaxTransactionDateFilter != null, e => e.TransactionDate <= input.MaxTransactionDateFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AccountNumberFilter), e => e.AccountNumber.ToLower() == input.AccountNumberFilter.ToLower().Trim())
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AccountTypeFilter), e => e.AccountType.ToLower() == input.AccountTypeFilter.ToLower().Trim())
                        .WhereIf(input.DetectionSourceFilter > -1, e => e.DetectionSource == detectionSourceFilter)
                        .WhereIf(input.SourceFilter > -1, e => e.Source == sourceFilter)
                        .WhereIf(input.TransactionStatusFilter > -1, e => e.TransactionStatus == transactionStatusFilter)
                        .WhereIf(input.ConclusionFilter > -1, e => e.Conclusion == conclusionFilter)
                        .WhereIf(input.StatusFilter > -1, e => e.Status == statusFilter);


            var query = (from o in filteredCases
                         join o1 in _transactionTypeRepository.GetAll() on o.TransactionTypeId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()

                         join o2 in _userRepository.GetAll() on o.AllocationUserId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()

                         join o3 in _ruleRepository.GetAll() on o.RuleId equals o3.Id into j3
                         from s3 in j3.DefaultIfEmpty()

                         select new GetCaseForView()
                         {
                             Case = ObjectMapper.Map<CaseDto>(o),
                             TransactionTypeName = s1 == null ? "" : s1.Name.ToString(),
                             UserName = s2 == null ? "" : s2.FullName.ToString(),
                             RuleName = s3 == null ? "" : s3.Name.ToString()
                         })
                        .WhereIf(!string.IsNullOrWhiteSpace(input.TransactionTypeNameFilter), e => e.TransactionTypeName.ToLower() == input.TransactionTypeNameFilter.ToLower().Trim())
                        .WhereIf(!string.IsNullOrWhiteSpace(input.UserNameFilter), e => e.UserName.ToLower() == input.UserNameFilter.ToLower().Trim())
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RuleNameFilter), e => e.RuleName.ToLower() == input.RuleNameFilter.ToLower().Trim());

            var totalCount = await query.CountAsync();

            var cases = await query
                .OrderBy(input.Sorting ?? "case.creationTime desc")
                .PageBy(input)
                .ToListAsync();

            return new PagedResultDto<GetCaseForView>(
                totalCount,
                cases
            );
        }

        [AbpAuthorize(AppPermissions.Pages_Cases_Edit)]
        public async Task<GetCaseForEditOutput> GetCaseForEdit(EntityDto<string> input)
        {
            var alert = await _caseRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetCaseForEditOutput { Case = ObjectMapper.Map<CreateOrEditCaseDto>(alert) };

            if (output.Case.TransactionTypeId != null)
            {
                var transactionType = await _transactionTypeRepository.FirstOrDefaultAsync((Guid)output.Case.TransactionTypeId);
                output.TransactionTypeName = transactionType.Name.ToString();
            }

            if (output.Case.AllocationUserId != null)
            {
                var user = await _userRepository.FirstOrDefaultAsync((long)output.Case.AllocationUserId);
                output.UserName = user.FullName.ToString();
            }

            if (output.Case.RuleId != null)
            {
                var rule = await _ruleRepository.FirstOrDefaultAsync((Guid)output.Case.RuleId);
                output.RuleName = rule.Name.ToString();
            }

            //Define local list variable
            List<GetCaseFailedRuleForViewDto> failedRules = new List<GetCaseFailedRuleForViewDto>();
            List<GetCaseTaskForViewDto> caseTasks = new List<GetCaseTaskForViewDto>();
            List<GetCaseActionForViewDto> caseActions = new List<GetCaseActionForViewDto>();

            //Get list for specific case
            var filteredRules = (from cfr in _caseFailedRuleRepository.GetAll().Where(x => x.AlertId == output.Case.Id).OrderByDescending(x => x.CreationTime)
                                 join r in _ruleRepository.GetAll() on cfr.RuleId equals r.Id into r1
                                 from rT in r1.DefaultIfEmpty()
                                 
                                 select new GetCaseFailedRuleForViewDto
                                 {
                                     CaseFailedRule = ObjectMapper.Map<CaseFailedRuleDto>(cfr),
                                     RuleName = rT == null ? "" : rT.Name
                                 }
                                 ).ToListAsync();


            var filteredTask = (from ct in _caseTaskRepository.GetAll().Where(x => x.CaseId == output.Case.Id).OrderByDescending(x => x.CreationTime)
                                join u1 in _userRepository.GetAll() on ct.AssignerUserId equals u1.Id  into a1
                                from a1T in a1.DefaultIfEmpty()
                                join u2 in _userRepository.GetAll() on ct.AssigneeUserId equals u2.Id into a2
                                from a2T in a2.DefaultIfEmpty()
                                
                                select new GetCaseTaskForViewDto
                                {
                                    CaseTask = ObjectMapper.Map<CaseTaskDto>(ct),
                                    AssignerUsername = a1T == null ? "System" : a1T.FullName,
                                    AssigneeUsername = a2T == null ? "" : a2T.FullName
                                }).ToListAsync();


            var filterActions = (from ca in _caseActionRepository.GetAll().Where(x => x.AlertId == output.Case.Id)
                                 select new GetCaseActionForViewDto {
                                     CaseAction = ObjectMapper.Map<CaseActionDto>(ca)
                                 }).ToListAsync();

            //Update local list variables
            output.FailedRules = await filteredRules;
            output.Tasks = await filteredTask;
            output.Actions = await filterActions;
            

            return output;
        }


        public async Task CreateOrEdit(CreateOrEditCaseDto input)
        {
            if (input.Id.IsNullOrWhiteSpace())
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Cases_Create)]
        private async Task Create(CreateOrEditCaseDto input)
        {
            var alert = ObjectMapper.Map<Alert>(input);



            if (alert.Id.IsNullOrWhiteSpace())
            {
                alert.Id = Guid.NewGuid().ToString();
            }

            alert.Status = Status.Open;
            alert.Conclusion = Conclusion.Pending;

            await _caseRepository.InsertAsync(alert);
        }

        [AbpAuthorize(AppPermissions.Pages_Cases_Edit)]
        private async Task Update(CreateOrEditCaseDto input)
        {
            var alert = await _caseRepository.FirstOrDefaultAsync((string)input.Id);
            ObjectMapper.Map(input, alert);
        }

        [AbpAuthorize(AppPermissions.Pages_Cases_Delete)]
        public async Task Delete(EntityDto<string> input)
        {
            await _caseRepository.DeleteAsync(input.Id);
        }

        public async Task<FileDto> GetCasesToExcel(GetAllCasesForExcelInput input)
        {
            var detectionSourceFilter = (DectectionSource)input.DetectionSourceFilter;
            var sourceFilter = (Sources)input.SourceFilter;
            var transactionStatusFilter = (TransactionStatus)input.TransactionStatusFilter;
            var conclusionFilter = (Conclusion)input.ConclusionFilter;
            var statusFilter = (Status)input.StatusFilter;

            var filteredCases = _caseRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.AccountNumber.Contains(input.Filter) || e.AccountName.Contains(input.Filter) || e.AccountType.Contains(input.Filter) || e.ProductName.Contains(input.Filter) || e.Description.Contains(input.Filter) || e.ClosureComments.Contains(input.Filter))
                        .WhereIf(input.MinTransactionDateFilter != null, e => e.TransactionDate >= input.MinTransactionDateFilter)
                        .WhereIf(input.MaxTransactionDateFilter != null, e => e.TransactionDate <= input.MaxTransactionDateFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AccountNumberFilter), e => e.AccountNumber.ToLower() == input.AccountNumberFilter.ToLower().Trim())
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AccountTypeFilter), e => e.AccountType.ToLower() == input.AccountTypeFilter.ToLower().Trim())
                        .WhereIf(input.DetectionSourceFilter > -1, e => e.DetectionSource == detectionSourceFilter)
                        .WhereIf(input.SourceFilter > -1, e => e.Source == sourceFilter)
                        .WhereIf(input.TransactionStatusFilter > -1, e => e.TransactionStatus == transactionStatusFilter)
                        .WhereIf(input.ConclusionFilter > -1, e => e.Conclusion == conclusionFilter)
                        .WhereIf(input.StatusFilter > -1, e => e.Status == statusFilter);


            var query = (from o in filteredCases
                         join o1 in _transactionTypeRepository.GetAll() on o.TransactionTypeId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()

                         join o2 in _userRepository.GetAll() on o.AllocationUserId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()

                         join o3 in _ruleRepository.GetAll() on o.RuleId equals o3.Id into j3
                         from s3 in j3.DefaultIfEmpty()

                         select new GetCaseForView()
                         {
                             Case = ObjectMapper.Map<CaseDto>(o),
                             TransactionTypeName = s1 == null ? "" : s1.Name.ToString(),
                             UserName = s2 == null ? "" : s2.Name.ToString(),
                             RuleName = s3 == null ? "" : s3.Name.ToString()
                         })
                        .WhereIf(!string.IsNullOrWhiteSpace(input.TransactionTypeNameFilter), e => e.TransactionTypeName.ToLower() == input.TransactionTypeNameFilter.ToLower().Trim())
                        .WhereIf(!string.IsNullOrWhiteSpace(input.UserNameFilter), e => e.UserName.ToLower() == input.UserNameFilter.ToLower().Trim())
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RuleNameFilter), e => e.RuleName.ToLower() == input.RuleNameFilter.ToLower().Trim());


            var caseListDtos = await query.ToListAsync();

            return _casesExcelExporter.ExportToFile(caseListDtos);
        }

        public async Task ResolveCase(ResolveCaseInputDto input)
        {
            var alert = await _caseRepository.FirstOrDefaultAsync((string)input.CaseId);
            alert.Conclusion = input.Conclusion;
            alert.Status = Status.Closed;
            alert.AllocationUserId = AbpSession.UserId;
            alert.ClosureComments = input.ClosureComment;

            if (input.Conclusion == Conclusion.FalsePositive)
            {
                alert.TransactionStatus = TransactionStatus.Completed;
            }

            ObjectMapper.Map(alert, alert);
        }

        public async Task<string> GetCaseVerificationCode(EntityDto<string> input)
        {
            var alert = await _caseRepository.FirstOrDefaultAsync((string)input.Id);

            return alert.VerificationCode;
        }

    }
}