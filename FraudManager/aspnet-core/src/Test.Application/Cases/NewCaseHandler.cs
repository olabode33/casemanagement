﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Events.Bus;
using Abp.Events.Bus.Handlers;
using Abp.Extensions;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Test.Cases.Dtos;
using Test.Common;
using Test.Configuration;
using Test.PhoneCall;
using Test.PhoneCall.Dtos;
using Test.TransactionTypes;

namespace Test.Cases
{
    public class NewCaseHandler : IEventHandler<RecieveCaseEventData>, ITransientDependency
    {
        private readonly IRepository<Alert, string> _caseRepository;
        private readonly string _alphabets = "ABCDE";

        private readonly IBackgroundJobManager _backgroundJobManager;
        private readonly IRepository<TransactionType, Guid> _transactionTypeRepository;
        private readonly IRepository<CaseFailedRule> _caseFailedRulesRepository;
        private readonly IConfigurationRoot _appConfiguration;
        private readonly IPhoneAppService _phoneAppService;

        public NewCaseHandler(
            IRepository<Alert, string> caseRepository,
            IRepository<TransactionType, Guid> _transactionTypeRepository,
            IRepository<CaseFailedRule> caseFailedRulesRepository,
            IBackgroundJobManager backgroundJobManager,
            IHostingEnvironment env,
            IPhoneAppService phoneAppService)
        {
            _caseRepository = caseRepository;
            _backgroundJobManager = backgroundJobManager;
            this._transactionTypeRepository = _transactionTypeRepository;
            _caseFailedRulesRepository = caseFailedRulesRepository;
            _appConfiguration = env.GetAppConfiguration();
            _phoneAppService = phoneAppService;
        }

        public void HandleEvent(RecieveCaseEventData eventData)
        {
            //Thread.Sleep(30000);
            //Logger.Debug("Response from Pipeline: " + eventData.Payload);
            JObject result = JObject.Parse(eventData.Payload);

            string is_fraud = (string)result["prediction"];
            string rule_id = (string)result["rule_id"];

            DateTimeOffset trans_Date = DateTimeOffset.FromUnixTimeMilliseconds((long)result["tran_date"]);


            if (is_fraud.Equals("1") || !String.IsNullOrWhiteSpace(rule_id))
            {
                CreateOrEditCaseDto caseDto = new CreateOrEditCaseDto
                {
                    TransactionDate = trans_Date.UtcDateTime,
                    AccountName = (string)result["first"] + " " + (string)result["last"],
                    AccountNumber = (string)result["cc_num"],
                    AccountType = (string)result["Channel"],
                    DetectionSource = String.IsNullOrWhiteSpace(rule_id) ? DectectionSource.Algorithm : DectectionSource.Rules,
                    Source = Sources.System,
                    TransactionStatus = TransactionStatus.Blocked,
                    Conclusion = Conclusion.Pending,
                    TransactionTypeId = Guid.Parse("EB41CC74-6C5A-4DFE-9D2F-08D68D2D086D"),
                    Status = Status.Open,
                    ProductName = (string)result["Channel"],
                    ClosureComments = (string)result["phone_number"], //"+2348094585637", //
                    Amount = (decimal)result["amt"],
                    PhoneNumber = (string)result["phone_number"],
                    Description = "Transaction Type: " + (string)result["Transaction Type"] + "; Amount: " + (string)result["amt"] + "; Balance: " + (string)result["Balance"],
                };



                var transactionType = _transactionTypeRepository.FirstOrDefault(x => x.Name.ToLower() == ((string)result["Transaction Type"]).ToLower());

                caseDto.TransactionTypeId = transactionType.Id;

                //If case failed rule
                if (!String.IsNullOrWhiteSpace(rule_id))
                {
                    caseDto.RuleId = Guid.Parse(rule_id);
                }


                var alert = new Alert()
                {
                    Id = caseDto.Id,
                    AccountName = caseDto.AccountName,
                    AccountNumber = caseDto.AccountNumber,
                    AccountType = caseDto.AccountType,
                    AllocationUserId = caseDto.AllocationUserId,
                    Amount = caseDto.Amount,
                    Anomally = caseDto.Anomally,
                    ClosureComments = caseDto.ClosureComments,
                    Conclusion = caseDto.Conclusion,
                    //CustomerID = caseDto.Custo,
                    Description = caseDto.Description,
                    //FlowId = caseDto.Flow
                    DetectionSource = caseDto.DetectionSource,
                    //OriginalPayLoad = caseDto.Or
                    //OtherDetails = caseDto.Oth
                    PhoneNumber = caseDto.PhoneNumber,
                    ProductName = caseDto.ProductName,
                    RuleId = caseDto.RuleId,
                    Source = caseDto.Source,
                    Status = caseDto.Status,
                    TransactionDate = caseDto.TransactionDate,
                    TransactionStatus = caseDto.TransactionStatus,
                    TransactionTypeId = caseDto.TransactionTypeId
                    //VerificationCode = caseDto.Ver
                };

                if (alert.Id.IsNullOrWhiteSpace())
                {
                    alert.Id = Guid.NewGuid().ToString();
                }

                //Check Anomally
                //string anomally = DetectAnomallyListener(new AnomallyDetectionInputDto()
                //{
                //    cc_num = (string)result["cc_num"],
                //    amt = (string)result["amt"],
                //    unix_time = (string)result["tran_date"],
                //    merch_lat = (string)result["merch_lat"],
                //    merch_long = (string)result["merch_long"]
                //});

                //if (anomally != null)
                //{
                //    JObject anomally_result = JObject.Parse(anomally);

                //    var anomally_59 = (string)anomally_result["59"]["Anomaly"];
                //    alert.Anomally = anomally_59;
                //}
                
                alert.Status = Status.Open;
                alert.VerificationCode = new Random().Next(1000, 9999).ToString();
                //alert.Conclusion = Conclusion.Pending;

                _caseRepository.Insert(alert);
                EventBus.Default.Trigger(this, new AssignCaseToUserDto() { CaseId  = alert.Id });

                //If case failed rule
                if (!String.IsNullOrWhiteSpace(rule_id))
                {
                    _caseFailedRulesRepository.Insert(new CaseFailedRule
                    {
                        AlertId = alert.Id,
                        RuleId = caseDto.RuleId
                    });
                }



                TwilioInputDto phoneDto = new TwilioInputDto()
                {
                    ToPhoneNumber = alert.PhoneNumber,
                    TransactionId = alert.Id,
                    Isfraud = "1",
                    Amount = (double)alert.Amount,
                    VerificationCode = alert.VerificationCode
                };

                //ContactCustomer(phoneDto);
            }

        }

        public void HandleEvent(TwilioInputDto eventData)
        {
            ContactCustomer(eventData);
        }

        public void ContactCustomer(TwilioInputDto input)
        {
            //input.VerificationCode = new Random().Next(1000, 9999).ToString(); //RandomLetters() + "-" + 
            
            //if (input.TransactionId != null)
            //{
            //    var c = _caseRepository.Get(input.TransactionId);
            //    c.Status = Status.Verifying;
            //    c.VerificationCode = input.VerificationCode;
            //    _caseRepository.Update(c);
            //    //CurrentUnitOfWork.SaveChanges();
            //}

            if (input.Isfraud.Equals("1"))
            {
                //Logger.Debug("Fraudulent transaction detected!!!");
                input.MessageBody = input.VerificationCode + " is the OTP for your transaction of NGN " + string.Format("{0:#,0}", input.Amount) + " initiated at " + DateTime.Now.AddMinutes(-1).ToString("HH:mm:ss") + ". Thank You. ";
                SendSMS(input);
                MakeCall(input);
            }
        }

        public string DetectAnomallyListener(AnomallyDetectionInputDto data)
        {
            string AnomallyURL = _appConfiguration["URLs:Anomally_URL"];

            Uri url = new Uri(string.Format(AnomallyURL));

            string json = "{\"cc_num\": {\"59\":" + data.cc_num + "}," +
                                "\"unix_tim\":{ \"59\":" + data.unix_time + "}," +
                                "\"amt\":{ \"59\":" + data.amt + "}," +
                                "\"merch_lat\":{ \"59\":" + data.merch_lat + "}," +
                                "\"merch_long\":{ \"59\":" + data.merch_long + "}}";

            string result = WebHelper.Post(url, json);
            return result;
        }

        public void MakeCall(TwilioInputDto input)
        {
            string TwilioCallback_URL = _appConfiguration["URLs:TwilioCallback_URL"];

            _backgroundJobManager.EnqueueAsync<MakeCallJob, SendPhoneSmSInputArgs>(
                new SendPhoneSmSInputArgs
                {
                    ToPhoneNumber = input.ToPhoneNumber,
                    VerificationCode = input.VerificationCode,
                    TransactionId = input.TransactionId,
                    Amount = input.Amount,
                    amountWords = input.amountWords,
                    Isfraud = input.Isfraud,
                    MessageBody = input.MessageBody,
                    TwilioCallback_URL = TwilioCallback_URL
                }, delay: TimeSpan.FromMinutes(1));
        }

        public void SendSMS(TwilioInputDto input)
        {
            _backgroundJobManager.EnqueueAsync<SendSmsJob, SendPhoneSmSInputArgs>(
                new SendPhoneSmSInputArgs
                {
                    ToPhoneNumber = input.ToPhoneNumber,
                    VerificationCode = input.VerificationCode,
                    TransactionId = input.TransactionId,
                    Amount = input.Amount,
                    amountWords = input.amountWords,
                    Isfraud = input.Isfraud,
                    MessageBody = input.MessageBody
                });
        }



    }
}
