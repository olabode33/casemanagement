using Test.Cases;

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Test.Cases.Dtos;
using Test.Dto;
using Abp.Application.Services.Dto;
using Test.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Test.Common.Dtos;

namespace Test.Cases
{
	[AbpAuthorize(AppPermissions.Pages_CaseActions)]
    public class CaseActionsAppService : TestAppServiceBase, ICaseActionsAppService
    {
		 private readonly IRepository<CaseAction> _caseActionRepository;
		 private readonly IRepository<Alert,string> _alertRepository;
		 

		  public CaseActionsAppService(IRepository<CaseAction> caseActionRepository , IRepository<Alert, string> alertRepository) 
		  {
			_caseActionRepository = caseActionRepository;
			_alertRepository = alertRepository;
		
		  }

		 public async Task<PagedResultDto<GetCaseActionForViewDto>> GetAll(GetAllCaseActionsInput input)
         {
			
			var filteredCaseActions = _caseActionRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.Title.Contains(input.Filter) || e.Comment.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.TitleFilter),  e => e.Title.ToLower() == input.TitleFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.CommentFilter),  e => e.Comment.ToLower() == input.CommentFilter.ToLower().Trim());


			var query = (from o in filteredCaseActions
                         join o1 in _alertRepository.GetAll() on o.AlertId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         
                         select new GetCaseActionForViewDto() {
							CaseAction = ObjectMapper.Map<CaseActionDto>(o),
                         	AlertAccountNumber = s1 == null ? "" : s1.AccountNumber.ToString()
						})
						.WhereIf(!string.IsNullOrWhiteSpace(input.AlertAccountNumberFilter), e => e.AlertAccountNumber.ToLower() == input.AlertAccountNumberFilter.ToLower().Trim());

            var totalCount = await query.CountAsync();

            var caseActions = await query
                .OrderBy(input.Sorting ?? "caseAction.id asc")
                .PageBy(input)
                .ToListAsync();

            return new PagedResultDto<GetCaseActionForViewDto>(
                totalCount,
                caseActions
            );
         }
		 
		 public async Task<GetCaseActionForViewDto> GetCaseActionForView(int id)
         {
            var caseAction = await _caseActionRepository.GetAsync(id);

            var output = new GetCaseActionForViewDto { CaseAction = ObjectMapper.Map<CaseActionDto>(caseAction) };

		    if (output.CaseAction.AlertId != null)
            {
                var alert = await _alertRepository.FirstOrDefaultAsync((string)output.CaseAction.AlertId);
                output.AlertAccountNumber = alert.AccountNumber.ToString();
            }
			
            return output;
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_CaseActions_Edit)]
		 public async Task<GetCaseActionForEditOutput> GetCaseActionForEdit(EntityDto input)
         {
            var caseAction = await _caseActionRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetCaseActionForEditOutput {CaseAction = ObjectMapper.Map<CreateOrEditCaseActionDto>(caseAction)};

		    if (output.CaseAction.AlertId != null)
            {
                var alert = await _alertRepository.FirstOrDefaultAsync((string)output.CaseAction.AlertId);
                output.AlertAccountNumber = alert.AccountNumber.ToString();
            }
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditCaseActionDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_CaseActions_Create)]
		 private async Task Create(CreateOrEditCaseActionDto input)
         {
            var caseAction = ObjectMapper.Map<CaseAction>(input);

			

            await _caseActionRepository.InsertAsync(caseAction);
         }

		 [AbpAuthorize(AppPermissions.Pages_CaseActions_Edit)]
		 private async Task Update(CreateOrEditCaseActionDto input)
         {
            var caseAction = await _caseActionRepository.FirstOrDefaultAsync((int)input.Id);
             ObjectMapper.Map(input, caseAction);
         }

		 [AbpAuthorize(AppPermissions.Pages_CaseActions_Delete)]
         public async Task Delete(EntityDto input)
         {
            await _caseActionRepository.DeleteAsync(input.Id);
         } 

		[AbpAuthorize(AppPermissions.Pages_CaseActions)]
         public async Task<PagedResultDto<AlertLookupTableDto>> GetAllAlertForLookupTable(GetAllForLookupTableInput input)
         {
             var query = _alertRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                   e=> e.AccountNumber.ToString().Contains(input.Filter)
                );

            var totalCount = await query.CountAsync();

            var alertList = await query
                .PageBy(input)
                .ToListAsync();

			var lookupTableDtoList = new List<AlertLookupTableDto>();
			foreach(var alert in alertList){
				lookupTableDtoList.Add(new AlertLookupTableDto
				{
					Id = alert.Id,
					DisplayName = alert.AccountNumber?.ToString()
				});
			}

            return new PagedResultDto<AlertLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
         }
    }
}