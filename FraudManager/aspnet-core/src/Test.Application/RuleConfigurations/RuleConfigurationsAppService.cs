using Test.Rules;
using Test.PayloadProperties;

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Test.RuleConfigurations.Exporting;
using Test.RuleConfigurations.Dtos;
using Test.Dto;
using Abp.Application.Services.Dto;
using Test.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;

namespace Test.RuleConfigurations
{
	[AbpAuthorize(AppPermissions.Pages_RuleConfigurations)]
    public class RuleConfigurationsAppService : TestAppServiceBase, IRuleConfigurationsAppService
    {
		 private readonly IRepository<RuleConfiguration, Guid> _ruleConfigurationRepository;
		 private readonly IRuleConfigurationsExcelExporter _ruleConfigurationsExcelExporter;
		 private readonly IRepository<Rule,Guid> _ruleRepository;
		 private readonly IRepository<DataPoint,Guid> _dataPointRepository;
		 

		  public RuleConfigurationsAppService(IRepository<RuleConfiguration, Guid> ruleConfigurationRepository, IRuleConfigurationsExcelExporter ruleConfigurationsExcelExporter , IRepository<Rule, Guid> ruleRepository, IRepository<DataPoint, Guid> dataPointRepository) 
		  {
			_ruleConfigurationRepository = ruleConfigurationRepository;
			_ruleConfigurationsExcelExporter = ruleConfigurationsExcelExporter;
			_ruleRepository = ruleRepository;
		_dataPointRepository = dataPointRepository;
		
		  }

		 public async Task<PagedResultDto<GetRuleConfigurationForView>> GetAll(GetAllRuleConfigurationsInput input)
         {
			
			var filteredRuleConfigurations = _ruleConfigurationRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.Operand.Contains(input.Filter) || e.Connector.Contains(input.Filter) || e.Value.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.OperandFilter),  e => e.Operand.ToLower() == input.OperandFilter.ToLower().Trim());


			var query = (from o in filteredRuleConfigurations
                         join o1 in _ruleRepository.GetAll() on o.RuleId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         
                         join o2 in _dataPointRepository.GetAll() on o.DataPointId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()
                         
                         select new GetRuleConfigurationForView() {
							RuleConfiguration = ObjectMapper.Map<RuleConfigurationDto>(o),
                         	RuleName = s1 == null ? "" : s1.Name.ToString(),
                         	DataPointName = s2 == null ? "" : s2.Name.ToString()
						})
						.WhereIf(!string.IsNullOrWhiteSpace(input.RuleNameFilter), e => e.RuleName.ToLower() == input.RuleNameFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.DataPointNameFilter), e => e.DataPointName.ToLower() == input.DataPointNameFilter.ToLower().Trim());

            var totalCount = await query.CountAsync();

            var ruleConfigurations = await query
                .OrderBy(input.Sorting ?? "ruleConfiguration.id asc")
                .PageBy(input)
                .ToListAsync();

            return new PagedResultDto<GetRuleConfigurationForView>(
                totalCount,
                ruleConfigurations
            );
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_RuleConfigurations_Edit)]
		 public async Task<GetRuleConfigurationForEditOutput> GetRuleConfigurationForEdit(EntityDto<Guid> input)
         {
            var ruleConfiguration = await _ruleConfigurationRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetRuleConfigurationForEditOutput {RuleConfiguration = ObjectMapper.Map<CreateOrEditRuleConfigurationDto>(ruleConfiguration)};


		    if (output.RuleConfiguration.DataPointId != null)
            {
                var dataPoint = await _dataPointRepository.FirstOrDefaultAsync((Guid)output.RuleConfiguration.DataPointId);
                output.DataPointName = dataPoint.Name.ToString();
            }
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditRuleConfigurationDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_RuleConfigurations_Create)]
		 private async Task Create(CreateOrEditRuleConfigurationDto input)
         {
            var ruleConfiguration = ObjectMapper.Map<RuleConfiguration>(input);

			

            await _ruleConfigurationRepository.InsertAsync(ruleConfiguration);
         }

		 [AbpAuthorize(AppPermissions.Pages_RuleConfigurations_Edit)]
		 private async Task Update(CreateOrEditRuleConfigurationDto input)
         {
            var ruleConfiguration = await _ruleConfigurationRepository.FirstOrDefaultAsync((Guid)input.Id);
             ObjectMapper.Map(input, ruleConfiguration);
         }

		 [AbpAuthorize(AppPermissions.Pages_RuleConfigurations_Delete)]
         public async Task Delete(EntityDto<Guid> input)
         {
            await _ruleConfigurationRepository.DeleteAsync(input.Id);
         } 

		public async Task<FileDto> GetRuleConfigurationsToExcel(GetAllRuleConfigurationsForExcelInput input)
         {
			
			var filteredRuleConfigurations = _ruleConfigurationRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.Operand.Contains(input.Filter) || e.Connector.Contains(input.Filter) || e.Value.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.OperandFilter),  e => e.Operand.ToLower() == input.OperandFilter.ToLower().Trim());


			var query = (from o in filteredRuleConfigurations
                         join o1 in _ruleRepository.GetAll() on o.RuleId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         
                         join o2 in _dataPointRepository.GetAll() on o.DataPointId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()
                         
                         select new GetRuleConfigurationForView() { 
							RuleConfiguration = ObjectMapper.Map<RuleConfigurationDto>(o),
                         	RuleName = s1 == null ? "" : s1.Name.ToString(),
                         	DataPointName = s2 == null ? "" : s2.Name.ToString()
						 })
						.WhereIf(!string.IsNullOrWhiteSpace(input.RuleNameFilter), e => e.RuleName.ToLower() == input.RuleNameFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.DataPointNameFilter), e => e.DataPointName.ToLower() == input.DataPointNameFilter.ToLower().Trim());


            var ruleConfigurationListDtos = await query.ToListAsync();

            return _ruleConfigurationsExcelExporter.ExportToFile(ruleConfigurationListDtos);
         }



		[AbpAuthorize(AppPermissions.Pages_RuleConfigurations)]
         public async Task<PagedResultDto<RuleLookupTableDto>> GetAllRuleForLookupTable(GetAllForLookupTableInput input)
         {
             var query = _ruleRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                   e=> e.Name.ToString().Contains(input.Filter)
                ).WhereIf(input.TransactionTypeId != null && input.TransactionTypeId != Guid.Empty, x => x.TransactionTypeId == input.TransactionTypeId );

            var totalCount = await query.CountAsync();

            var ruleList = await query
                .PageBy(input)
                .ToListAsync();

			var lookupTableDtoList = new List<RuleLookupTableDto>();
			foreach(var rule in ruleList){
				lookupTableDtoList.Add(new RuleLookupTableDto
				{
					Id = rule.Id.ToString(),
					DisplayName = rule.Name?.ToString()
				});
			}

            return new PagedResultDto<RuleLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
         }

		[AbpAuthorize(AppPermissions.Pages_RuleConfigurations)]
         public async Task<PagedResultDto<DataPointLookupTableDto>> GetAllDataPointForLookupTable(GetAllForLookupTableInput input)
         {
             var query = _dataPointRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                   e=> e.Name.ToString().Contains(input.Filter)
                );

            var totalCount = await query.CountAsync();

            var dataPointList = await query
                .PageBy(input)
                .ToListAsync();

			var lookupTableDtoList = new List<DataPointLookupTableDto>();
			foreach(var dataPoint in dataPointList){
				lookupTableDtoList.Add(new DataPointLookupTableDto
				{
					Id = dataPoint.Id.ToString(),
					DisplayName = dataPoint.Name?.ToString(),
                     DataType = (int)dataPoint.DataType
				});
			}

            return new PagedResultDto<DataPointLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
         }
    }
}