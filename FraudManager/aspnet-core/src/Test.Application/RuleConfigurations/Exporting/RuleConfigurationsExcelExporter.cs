using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using Test.DataExporting.Excel.EpPlus;
using Test.RuleConfigurations.Dtos;
using Test.Dto;
using Test.Storage;

namespace Test.RuleConfigurations.Exporting
{
    public class RuleConfigurationsExcelExporter : EpPlusExcelExporterBase, IRuleConfigurationsExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public RuleConfigurationsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetRuleConfigurationForView> ruleConfigurations)
        {
            return CreateExcelPackage(
                "RuleConfigurations.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("RuleConfigurations"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Operand"),
                        L("Connector"),
                        L("Value"),
                        (L("Rule")) + L("Name"),
                        (L("DataPoint")) + L("Name")
                        );

                    AddObjects(
                        sheet, 2, ruleConfigurations,
                        _ => _.RuleConfiguration.Operand,
                        _ => _.RuleConfiguration.Connector,
                        _ => _.RuleConfiguration.Value,
                        _ => _.RuleName,
                        _ => _.DataPointName
                        );

					

                });
        }
    }
}
