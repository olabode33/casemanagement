using System.Collections.Generic;
using Test.RuleConfigurations.Dtos;
using Test.Dto;

namespace Test.RuleConfigurations.Exporting
{
    public interface IRuleConfigurationsExcelExporter
    {
        FileDto ExportToFile(List<GetRuleConfigurationForView> ruleConfigurations);
    }
}