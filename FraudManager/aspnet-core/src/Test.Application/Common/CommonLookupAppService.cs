﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using Test.Common.Dto;
using Test.Common.Dtos;
using Test.Editions;
using Test.Editions.Dto;
using Test.PayloadProperties;
using Test.TransactionTypes;

namespace Test.Common
{
    [AbpAuthorize]
    public class CommonLookupAppService : TestAppServiceBase, ICommonLookupAppService
    {
        private readonly EditionManager _editionManager;
        private readonly IRepository<TransactionType, Guid> _transactionTypeRepository;
        private readonly IRepository<DataPoint, Guid> _dataPointRepository;

        public CommonLookupAppService(
            EditionManager editionManager,
            IRepository<TransactionType, Guid> transactionTypeRepository,
            IRepository<DataPoint, Guid> dataPointRepository)
        {
            _editionManager = editionManager;
            _transactionTypeRepository = transactionTypeRepository;
            _dataPointRepository = dataPointRepository;
        }

        public async Task<ListResultDto<SubscribableEditionComboboxItemDto>> GetEditionsForCombobox(bool onlyFreeItems = false)
        {
            var subscribableEditions = (await _editionManager.Editions.Cast<SubscribableEdition>().ToListAsync())
                .WhereIf(onlyFreeItems, e => e.IsFree)
                .OrderBy(e => e.MonthlyPrice);

            return new ListResultDto<SubscribableEditionComboboxItemDto>(
                subscribableEditions.Select(e => new SubscribableEditionComboboxItemDto(e.Id.ToString(), e.DisplayName, e.IsFree)).ToList()
            );
        }

        public async Task<PagedResultDto<NameValueDto>> FindUsers(FindUsersInput input)
        {
            if (AbpSession.TenantId != null)
            {
                //Prevent tenants to get other tenant's users.
                input.TenantId = AbpSession.TenantId;
            }

            using (CurrentUnitOfWork.SetTenantId(input.TenantId))
            {
                var query = UserManager.Users
                    .WhereIf(
                        !input.Filter.IsNullOrWhiteSpace(),
                        u =>
                            u.Name.Contains(input.Filter) ||
                            u.Surname.Contains(input.Filter) ||
                            u.UserName.Contains(input.Filter) ||
                            u.EmailAddress.Contains(input.Filter)
                    );

                var userCount = await query.CountAsync();
                var users = await query
                    .OrderBy(u => u.Name)
                    .ThenBy(u => u.Surname)
                    .PageBy(input)
                    .ToListAsync();

                return new PagedResultDto<NameValueDto>(
                    userCount,
                    users.Select(u =>
                        new NameValueDto(
                            u.FullName + " (" + u.EmailAddress + ")",
                            u.Id.ToString()
                            )
                        ).ToList()
                    );
            }
        }

        public GetDefaultEditionNameOutput GetDefaultEditionName()
        {
            return new GetDefaultEditionNameOutput
            {
                Name = EditionManager.DefaultEditionName
            };
        }

        public async Task<PagedResultDto<NameValueDto>> GetAllTransactionTypeForLookupTable(GetAllForLookupTableInput input)
        {
            var query = _transactionTypeRepository.GetAll().WhereIf(
                   !string.IsNullOrWhiteSpace(input.Filter),
                  e => e.Name.ToString().Contains(input.Filter)
               );

            var totalCount = await query.CountAsync();

            var transactionTypeList = await query
                .PageBy(input)
                .ToListAsync();

            var lookupTableDtoList = new List<NameValueDto>();
            foreach (var transactionType in transactionTypeList)
            {
                lookupTableDtoList.Add(new NameValueDto
                {
                    Value = transactionType.Id.ToString(),
                    Name = transactionType.Name?.ToString()
                });
            }

            return new PagedResultDto<NameValueDto>(
                totalCount,
                lookupTableDtoList
            );
        }

        public async Task<PagedResultDto<NameValueDto>> GetAllDataPointForLookupTable(GetAllForLookupTableInput input)
        {
            var query = _dataPointRepository.GetAll().WhereIf(
                   !string.IsNullOrWhiteSpace(input.Filter),
                  e => e.Name.ToString().Contains(input.Filter)
               );

            var totalCount = await query.CountAsync();

            var dataPointList = await query
                .PageBy(input)
                .ToListAsync();

            var lookupTableDtoList = new List<NameValueDto>();
            foreach (var dataPoint in dataPointList)
            {
                lookupTableDtoList.Add(new NameValueDto
                {
                    Value = dataPoint.Id.ToString(),
                    Name = dataPoint.Name?.ToString() //+ (int)dataPoint.DataType
                });
            }

            return new PagedResultDto<NameValueDto>(
                totalCount,
                lookupTableDtoList
            );
        }
    }
}
