
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Test.Channels.Dtos;
using Test.Dto;
using Abp.Application.Services.Dto;
using Test.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;

namespace Test.Channels
{
	[AbpAuthorize(AppPermissions.Pages_Channels)]
    public class ChannelsAppService : TestAppServiceBase, IChannelsAppService
    {
		 private readonly IRepository<Channel, Guid> _channelRepository;
		 

		  public ChannelsAppService(IRepository<Channel, Guid> channelRepository ) 
		  {
			_channelRepository = channelRepository;
			
		  }

		 public async Task<PagedResultDto<GetChannelForView>> GetAll(GetAllChannelsInput input)
         {
			
			var filteredChannels = _channelRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.Name.Contains(input.Filter) || e.Description.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter),  e => e.Name.ToLower() == input.NameFilter.ToLower().Trim());


			var query = (from o in filteredChannels
                         select new GetChannelForView() {
							Channel = ObjectMapper.Map<ChannelDto>(o)
						});

            var totalCount = await query.CountAsync();

            var channels = await query
                .OrderBy(input.Sorting ?? "channel.id asc")
                .PageBy(input)
                .ToListAsync();

            return new PagedResultDto<GetChannelForView>(
                totalCount,
                channels
            );
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_Channels_Edit)]
		 public async Task<GetChannelForEditOutput> GetChannelForEdit(EntityDto<Guid> input)
         {
            var channel = await _channelRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetChannelForEditOutput {Channel = ObjectMapper.Map<CreateOrEditChannelDto>(channel)};
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditChannelDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_Channels_Create)]
		 private async Task Create(CreateOrEditChannelDto input)
         {
            var channel = ObjectMapper.Map<Channel>(input);

			

            await _channelRepository.InsertAsync(channel);
         }

		 [AbpAuthorize(AppPermissions.Pages_Channels_Edit)]
		 private async Task Update(CreateOrEditChannelDto input)
         {
            var channel = await _channelRepository.FirstOrDefaultAsync((Guid)input.Id);
             ObjectMapper.Map(input, channel);
         }

		 [AbpAuthorize(AppPermissions.Pages_Channels_Delete)]
         public async Task Delete(EntityDto<Guid> input)
         {
            await _channelRepository.DeleteAsync(input.Id);
         } 
    }
}