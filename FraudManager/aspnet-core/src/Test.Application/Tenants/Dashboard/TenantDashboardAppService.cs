﻿using Abp.Auditing;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Test.Authorization;
using Test.Authorization.Users;
using Test.Cases;
using Test.Rules;
using Test.Tenants.Dashboard.Dto;
using Test.TransactionTypes;

namespace Test.Tenants.Dashboard
{
    [DisableAuditing]
    [AbpAuthorize(AppPermissions.Pages_Tenant_Dashboard)]
    public class TenantDashboardAppService : TestAppServiceBase, ITenantDashboardAppService
    {
        private readonly IRepository<Alert, string> _caseRepository;
        private readonly IRepository<TransactionType, Guid> _transactionTypeRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<Rule, Guid> _ruleRepository;
        private readonly IRepository<CaseTask> _caseTaskRepository;

        public TenantDashboardAppService(
            IRepository<Alert, string> caseRepository, 
            IRepository<TransactionType, Guid> transactionTypeRepository, 
            IRepository<User, long> userRepository, 
            IRepository<Rule, Guid> ruleRepository,
            IRepository<CaseTask> caseTaskRepository)
        {
            _caseRepository = caseRepository;
            _transactionTypeRepository = transactionTypeRepository;
            _userRepository = userRepository;
            _ruleRepository = ruleRepository;
            _caseTaskRepository = caseTaskRepository;
        }

        public GetMemberActivityOutput GetMemberActivity()
        {
            return new GetMemberActivityOutput
            (
                DashboardRandomDataGenerator.GenerateMemberActivities()
            );
        }

        public GetDashboardDataOutput GetDashboardData(GetDashboardDataInput input)
        {
            var output = new GetDashboardDataOutput
            {
                TotalProfit = DashboardRandomDataGenerator.GetRandomInt(5000, 9000),
                NewFeedbacks = DashboardRandomDataGenerator.GetRandomInt(1000, 5000),
                NewOrders = DashboardRandomDataGenerator.GetRandomInt(100, 900),
                NewUsers = DashboardRandomDataGenerator.GetRandomInt(50, 500),
                SalesSummary = DashboardRandomDataGenerator.GenerateSalesSummaryData(input.SalesSummaryDatePeriod),
                Expenses = DashboardRandomDataGenerator.GetRandomInt(5000, 10000),
                Growth = DashboardRandomDataGenerator.GetRandomInt(5000, 10000),
                Revenue = DashboardRandomDataGenerator.GetRandomInt(1000, 9000),
                TotalSales = DashboardRandomDataGenerator.GetRandomInt(10000, 90000),
                TransactionPercent = DashboardRandomDataGenerator.GetRandomInt(10, 100),
                NewVisitPercent = DashboardRandomDataGenerator.GetRandomInt(10, 100),
                BouncePercent = DashboardRandomDataGenerator.GetRandomInt(10, 100),
                DailySales = DashboardRandomDataGenerator.GetRandomArray(30, 10, 50),
                ProfitShares = DashboardRandomDataGenerator.GetRandomPercentageArray(3)
            };

            return output;
        }

        public GetSalesSummaryOutput GetSalesSummary(GetSalesSummaryInput input)
        {
            return new GetSalesSummaryOutput(DashboardRandomDataGenerator.GenerateSalesSummaryData(input.SalesSummaryDatePeriod));
        }

        public GetRegionalStatsOutput GetRegionalStats(GetRegionalStatsInput input)
        {
            return new GetRegionalStatsOutput(DashboardRandomDataGenerator.GenerateRegionalStat());
        }

        public GetGeneralStatsOutput GetGeneralStats(GetGeneralStatsInput input)
        {
            return new GetGeneralStatsOutput
            {
                TransactionPercent = DashboardRandomDataGenerator.GetRandomInt(10, 100),
                NewVisitPercent = DashboardRandomDataGenerator.GetRandomInt(10, 100),
                BouncePercent = DashboardRandomDataGenerator.GetRandomInt(10, 100)
            };
        }

        public GetCaseDashboardOutputDto GetCaseDashboardStats()
        {
            var cases = _caseRepository.GetAll()
                .Include(x => x.TransactionType)
                .Where(x => x.CreationTime.Date == DateTime.Now.Date).ToList();

            var assignedTasks = _caseTaskRepository.GetAll()
                .Include(x => x.AssignerUser)
                .Where(x => x.AssigneeUserId == AbpSession.UserId)
                .Where(x => x.Status != Status.Closed)
                .OrderByDescending(x => x.CreationTime).ToList();

            var recentCases = cases.Where(x => x.Status != Status.Closed).OrderByDescending(x => x.CreationTime);

            List <GetRecentCasesDashboardOutputDto > recentCasesOutput = new List<GetRecentCasesDashboardOutputDto>();
            List<GetAssignedTaskDashboardOutputDto> assignedTasksOuput = new List<GetAssignedTaskDashboardOutputDto>();
            int limit = 10;
            int caseCount = 0;
            int taskCount = 0;

            foreach (var item in recentCases)
            {
                
                if(caseCount > limit)
                {
                    break;
                }

                recentCasesOutput.Add(new GetRecentCasesDashboardOutputDto {
                    CaseId = item.Id,
                    CreationDate = item.CreationTime,
                    CustomerName = item.AccountName,
                    DetectionSource = item.DetectionSource.ToString(),
                    TransactionAmout = item.Amount,
                    TransactionType = item.TransactionType.Name,
                    Status = item.Status,
                    TimeSpan = ToPrettyFormat(DateTime.Now.Subtract(item.CreationTime))
                });

                caseCount++; 
            }

            foreach (var item in assignedTasks)
            {
                if (taskCount > limit)
                {
                    break;
                }

                assignedTasksOuput.Add(new GetAssignedTaskDashboardOutputDto
                {
                    TaskId = item.Id,
                    Priority = item.Priority.ToString(),
                    Subject = item.Subject,
                    AssignedBy = item.AssignerUser == null ? "System" : item.AssignerUser.FullName,
                    TimeSpan = ToPrettyFormat(DateTime.Now.Subtract(item.CreationTime)),
                    Status = item.Status
                });

                taskCount++;
            }

            var output = new GetCaseDashboardOutputDto
            {
                NewCases = cases.Count,
                OutstandingCases = cases.Count(x => x.Status == Status.Open),
                PendingCases = cases.Count(x => x.Status == Status.Verifying),
                AssignedTasks = assignedTasks.Count(),
                RecentCases = recentCasesOutput,
                Tasks = assignedTasksOuput
            };

            return output;

        }

        private string ToPrettyFormat(TimeSpan span)
        {
            if (span == TimeSpan.Zero || span.Minutes <= 0)
                return "Just Now";

            var pretty = new StringBuilder();

            if (span.Days > 0)
                pretty.AppendFormat("{0} day{1} ", span.Days, span.Days > 1 ? "s" : String.Empty);
            if (span.Hours > 0)
                pretty.AppendFormat("{0} hour{1} ", span.Hours, span.Hours > 1 ? "s" : String.Empty);
            if (span.Minutes > 0)
                pretty.AppendFormat("{0} minute{1} ", span.Minutes, span.Minutes > 1 ? "s" : String.Empty);

            return pretty.ToString() + "ago";
        }
    }
}