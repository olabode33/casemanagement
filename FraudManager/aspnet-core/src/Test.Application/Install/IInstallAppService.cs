﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Test.Install.Dto;

namespace Test.Install
{
    public interface IInstallAppService : IApplicationService
    {
        Task Setup(InstallDto input);

        AppSettingsJsonDto GetAppSettingsJson();

        CheckDatabaseOutput CheckDatabase();
    }
}