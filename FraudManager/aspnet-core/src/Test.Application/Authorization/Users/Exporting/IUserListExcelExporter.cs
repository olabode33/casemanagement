using System.Collections.Generic;
using Test.Authorization.Users.Dto;
using Test.Dto;

namespace Test.Authorization.Users.Exporting
{
    public interface IUserListExcelExporter
    {
        FileDto ExportToFile(List<UserListDto> userListDtos);
    }
}