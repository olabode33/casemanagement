﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Test.PhoneCall.Dtos;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;
using Abp.Domain.Repositories;
using Test.Cases;
using Newtonsoft.Json.Linq;
using Test.Cases.Dtos;
using Abp.Extensions;
using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc;
using Abp.BackgroundJobs;
using Test.TransactionTypes;
using Abp.UI;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Hosting;
using Test.Configuration;
using Test.Common;
using Abp.Events.Bus;

namespace Test.PhoneCall
{
    public class PhoneAppService : TestAppServiceBase, IPhoneAppService
    {
        private readonly string accountsid = "ACf96f55b7044bc5ac6c9a83833e3be5d1";
        private readonly string authToken = "a5357ca20b14f82c91dd6ee79cd8dbe7";
        private readonly string fromPhoneNumber = "+13252214024";
        private readonly IRepository<Alert, string> _caseRepository;
        private readonly string _alphabets = "ABCDE";

        private readonly IBackgroundJobManager _backgroundJobManager;
        private readonly IRepository<TransactionType, Guid> _transactionTypeRepository;
        private readonly IConfigurationRoot _appConfiguration;

        private IEventBus EventBus1 { get; set; }

        public PhoneAppService(
            IRepository<Alert, string> caseRepository,
            IRepository<TransactionType, Guid> _transactionTypeRepository,
            IBackgroundJobManager backgroundJobManager,
            IHostingEnvironment env)
        {
            _caseRepository = caseRepository;
            _backgroundJobManager = backgroundJobManager;
            this._transactionTypeRepository = _transactionTypeRepository;
            _appConfiguration = env.GetAppConfiguration();
            EventBus1 = NullEventBus.Instance;
        }

        public async Task ContactCustomer(TwilioInputDto input)
        {
            EventBus.Default.Trigger(this, input);
        }

        [HttpPost]
        public async Task<string> GetResultFromMLEngine([FromBody] string input)
        {
            //EventBus.Trigger(this, new RecieveCaseEventData() { Payload = input });

            //EventBus.Default.Trigger(this, new RecieveCaseEventData() { Payload = input });

            _backgroundJobManager.Enqueue<HandleCaseJob, HandleCaseArgs>(new HandleCaseArgs { Payload = input });

            return "Triggering Event";

        }

        public async Task HandleNewCase(string input) {

            Logger.Debug("Response from Pipeline: " + input);
            JObject result = JObject.Parse(input);

            string is_fraud = (string)result["prediction"];
            string rule_id = (string)result["rule_id"];

            DateTimeOffset trans_Date = DateTimeOffset.FromUnixTimeMilliseconds((long)result["tran_date"]);


            if (is_fraud.Equals("1") || !String.IsNullOrWhiteSpace(rule_id))
            {
                CreateOrEditCaseDto caseDto = new CreateOrEditCaseDto
                {
                    TransactionDate = trans_Date.UtcDateTime,
                    AccountName = (string)result["first"] + " " + (string)result["last"],
                    AccountNumber = (string)result["cc_num"],
                    AccountType = (string)result["Channel"],
                    DetectionSource = String.IsNullOrWhiteSpace(rule_id) ? DectectionSource.Algorithm : DectectionSource.Rules,
                    Source = Sources.System,
                    TransactionStatus = TransactionStatus.Blocked,
                    Conclusion = Conclusion.Pending,
                    TransactionTypeId = Guid.Parse("EB41CC74-6C5A-4DFE-9D2F-08D68D2D086D"),
                    Status = Status.Open,
                    ProductName = (string)result["Channel"],
                    ClosureComments = (string)result["phone_number"], //"+2348094585637", //
                    Amount = (decimal)result["amt"],
                    PhoneNumber = (string)result["phone_number"],
                    Description = "Transaction Type: " + (string)result["Transaction Type"] + "; Amount: " + (string)result["amt"] + "; Balance: " + (string)result["Balance"],
                };



                var transactionType = await _transactionTypeRepository.FirstOrDefaultAsync(x => x.Name.ToLower() == ((string)result["Transaction Type"]).ToLower());

                caseDto.TransactionTypeId = transactionType.Id;


                if (!String.IsNullOrWhiteSpace(rule_id))
                {
                    caseDto.RuleId = Guid.Parse(rule_id);
                }


                var alert = ObjectMapper.Map<Alert>(caseDto);

                if (alert.Id.IsNullOrWhiteSpace())
                {
                    alert.Id = Guid.NewGuid().ToString();
                }

                //Check Anomally
                //string anomally = DetectAnomallyListener(new AnomallyDetectionInputDto()
                //{
                //    cc_num = (string)result["cc_num"],
                //    amt = (string)result["amt"],
                //    unix_time = (string)result["tran_date"],
                //    merch_lat = (string)result["merch_lat"],
                //    merch_long = (string)result["merch_long"]
                //});

                //if (anomally != null)
                //{
                //    JObject anomally_result = JObject.Parse(anomally);

                //    var anomally_59 = (string)anomally_result["59"]["Anomaly"];
                //    alert.Anomally = anomally_59;
                //}


                //alert.Status = Status.Open;
                //alert.Conclusion = Conclusion.Pending;

                await _caseRepository.InsertAsync(alert);



                //TwilioInputDto phoneDto = new TwilioInputDto()
                //{
                //    ToPhoneNumber = (string)result["master"]["phone_number"],
                //    Isfraud = (string)result["transaction"]["is_fraud"],
                //    MessageBody = "Test after creating Case. You have a pending transaction of: ₦" + (string)result["transaction"]["amt"] + ". Below is the verification code to approve the transaction."
                //};

                //await ContactCustomer(phoneDto);
            }

            //return (string)result["is_fraud"];
        }

        private string RandomLetters()
        {
            StringBuilder randLetters = new StringBuilder();

            for (int i = 0; i < 3; i++)
            {
                int rand = new Random().Next(0, _alphabets.Length - 1);
                randLetters.Append(_alphabets[rand]);
            }

            return randLetters.ToString();
        }

        


    }
}
