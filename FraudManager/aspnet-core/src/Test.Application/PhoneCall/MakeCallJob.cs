﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Uow;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.Text;
using Test.PhoneCall.Dtos;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

namespace Test.PhoneCall
{
    public class MakeCallJob : BackgroundJob<SendPhoneSmSInputArgs>, ITransientDependency
    {
        private readonly string accountsid = "ACf96f55b7044bc5ac6c9a83833e3be5d1";
        private readonly string authToken = "a5357ca20b14f82c91dd6ee79cd8dbe7";
        private readonly string fromPhoneNumber = "+13252214024";

        [UnitOfWork]
        public override void Execute(SendPhoneSmSInputArgs args)
        {
            try
            {
                Logger.Debug("About to make call: " + args.ToPhoneNumber);
                TwilioClient.Init(accountsid, authToken);

                var from = new PhoneNumber(fromPhoneNumber);
                var to = new PhoneNumber(args.ToPhoneNumber);
                var call = CallResource.Create(
                    to: to,
                    from: from,
                    url: new Uri(args.TwilioCallback_URL + args.TransactionId)
                );
                Logger.Debug("Call Made to: " + args.ToPhoneNumber);
            } catch(Exception e)
            {
                Logger.Error("Error making call: " + e.Message);
                throw new UserFriendlyException("Internet connection Required!");
            }
        }
    }
}
