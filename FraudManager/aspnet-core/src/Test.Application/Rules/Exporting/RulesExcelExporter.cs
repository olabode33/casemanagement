using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using Test.DataExporting.Excel.EpPlus;
using Test.Rules.Dtos;
using Test.Dto;
using Test.Storage;

namespace Test.Rules.Exporting
{
    public class RulesExcelExporter : EpPlusExcelExporterBase, IRulesExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public RulesExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetRuleForView> rules)
        {
            return CreateExcelPackage(
                "Rules.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Rules"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("Severity"),
                        L("Action"),
                        (L("TransactionType")) + L("Name")
                        );

                    AddObjects(
                        sheet, 2, rules,
                        _ => _.Rule.Name,
                        _ => _.Rule.Severity,
                        _ => _.Rule.Action,
                        _ => _.TransactionTypeName
                        );

					

                });
        }
    }
}
