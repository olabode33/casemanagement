using System.Collections.Generic;
using Test.Rules.Dtos;
using Test.Dto;

namespace Test.Rules.Exporting
{
    public interface IRulesExcelExporter
    {
        FileDto ExportToFile(List<GetRuleForView> rules);
    }
}