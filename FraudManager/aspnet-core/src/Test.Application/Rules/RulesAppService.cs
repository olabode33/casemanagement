using Test.TransactionTypes;
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Test.Rules.Exporting;
using Test.Rules.Dtos;
using Test.Dto;
using Abp.Application.Services.Dto;
using Test.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Test.RuleEscalations;
using Test.Authorization.Users;
using Test.RuleConfigurations;
using Test.RuleEscalations.Dtos;
using Test.RuleConfigurations.Dtos;
using Newtonsoft.Json.Linq;
using Test.PayloadProperties;
using System.Net;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Hosting;
using Test.Configuration;
using Abp.UI;

namespace Test.Rules
{
    [AbpAuthorize(AppPermissions.Pages_Rules)]
    public class RulesAppService : TestAppServiceBase, IRulesAppService
    {
        private readonly IRepository<Rule, Guid> _ruleRepository;
        private readonly IRulesExcelExporter _rulesExcelExporter;
        private readonly IRepository<TransactionType, Guid> _transactionTypeRepository;

        private readonly IRepository<RuleEscalation, Guid> _ruleEscalationRepository;
        private readonly IRepository<RuleConfiguration, Guid> _ruleConfigurationRepository;
        private readonly IRepository<User, long> _userRepository;

        private readonly IRepository<DataPoint, Guid> _dataPointRepository;
        private readonly IConfigurationRoot _appConfiguration;


        public RulesAppService(IRepository<RuleEscalation, Guid> _ruleEscalationRepository,
            IRepository<RuleConfiguration, Guid> _ruleConfigurationRepository,
            IRepository<Rule, Guid> ruleRepository, IRulesExcelExporter rulesExcelExporter,
            IRepository<User, long> _userRepository,
            IRepository<TransactionType, Guid> transactionTypeRepository,
            IRepository<DataPoint, Guid> datapointRepository,
            IHostingEnvironment env)
        {
            _ruleRepository = ruleRepository;
            _rulesExcelExporter = rulesExcelExporter;
            _transactionTypeRepository = transactionTypeRepository;
            this._ruleEscalationRepository = _ruleEscalationRepository;
            this._ruleConfigurationRepository = _ruleConfigurationRepository;
            this._userRepository = _userRepository;
            _dataPointRepository = datapointRepository;
            _appConfiguration = env.GetAppConfiguration();
        }

        public async Task<PagedResultDto<GetRuleForView>> GetAll(GetAllRulesInput input)
        {
            var severityFilter = (Severity)input.SeverityFilter;
            var actionFilter = (Actions)input.ActionFilter;
            var statusFilter = (RuleStatus)input.RuleStatusFilter;

            var filteredRules = _ruleRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter) || e.Description.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name.ToLower() == input.NameFilter.ToLower().Trim())
                        .WhereIf(input.SeverityFilter > -1, e => e.Severity == severityFilter)
                        .WhereIf(input.ActionFilter > -1, e => e.Action == actionFilter)
                        .WhereIf(input.RuleStatusFilter > -1, e => e.RuleStatus == statusFilter);


            var query = (from o in filteredRules
                         join o1 in _transactionTypeRepository.GetAll() on o.TransactionTypeId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         join u in _userRepository.GetAll() on o.ApprovedBy equals u.Id into u1
                         from uT in u1.DefaultIfEmpty()

                         select new GetRuleForView()
                         {
                             Rule = ObjectMapper.Map<RuleDto>(o),
                             TransactionTypeName = s1 == null ? "" : s1.Name.ToString(),
                             ApprovedBy = uT.FullName
                         })
                        .WhereIf(!string.IsNullOrWhiteSpace(input.TransactionTypeNameFilter), e => e.TransactionTypeName.ToLower() == input.TransactionTypeNameFilter.ToLower().Trim());

            var totalCount = await query.CountAsync();

            var rules = await query
                .OrderBy(input.Sorting ?? "rule.creationTime desc")
                .PageBy(input)
                .ToListAsync();

            return new PagedResultDto<GetRuleForView>(
                totalCount,
                rules
            );
        }

        [AbpAuthorize(AppPermissions.Pages_Rules_Edit)]
        public async Task<GetRuleForEditOutput> GetRuleForEdit(EntityDto<Guid> input)
        {
            var rule = await _ruleRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetRuleForEditOutput
            {
                Rule = new CreateOrEditRuleDto()
                {
                    Action = rule.Action,
                    Name = rule.Name,
                    Description = rule.Description,
                    Id = rule.Id,
                    Severity = rule.Severity,
                    TransactionTypeId = rule.TransactionTypeId,
                    Active = rule.Active,
                    ApprovedBy = rule.ApprovedBy,
                    ApprovedDate = rule.ApprovedDate,
                    IsApproved = rule.IsApproved,
                    Score = rule.Score,
                    BaseConnector = rule.BaseConnector,
                    RuleStatus = rule.RuleStatus
                }
            };

            if (output.Rule.TransactionTypeId != null)
            {
                var transactionType = await _transactionTypeRepository.FirstOrDefaultAsync((Guid)output.Rule.TransactionTypeId);
                output.TransactionTypeName = transactionType.Name.ToString();
            }

            if (output.Rule.ApprovedBy != null)
            {
                var approvedBy = await _userRepository.FirstOrDefaultAsync((long)output.Rule.ApprovedBy);
                output.ApprovedBy = approvedBy != null ? approvedBy.FullName : null;
            }

            var ruleEscalations = await _ruleEscalationRepository.GetAll().Include(x => x.User)
                .Where(x => x.RuleId == input.Id)
                .ToListAsync();

            List<RuleEscalationForRuleEditDto> escalationOutput = new List<RuleEscalationForRuleEditDto>();

            foreach (var item in ruleEscalations)
            {
                escalationOutput.Add(new RuleEscalationForRuleEditDto()
                {
                    Id = item.Id,
                    RuleId = item.RuleId,
                    UserId = item.UserId,
                    UserFullName = item.User.FullName
                });
            }

            var ruleConfigurations = await _ruleConfigurationRepository.GetAll().Include(x => x.DataPoint)
                                        .Where(x => x.RuleId == input.Id)
                                        .ToListAsync();

            List<RuleConfigurationForRuleEditDto> configurationOutput = new List<RuleConfigurationForRuleEditDto>();
            List<RuleObjGroup> ruleGroup = new List<RuleObjGroup>();
            List<RuleConfigurationForRuleEditDto> ruleSubgroup = new List<RuleConfigurationForRuleEditDto>();
            string groupName = "First"; string connector = "";

            foreach (var item in ruleConfigurations)
            {
                var ruleObj = new RuleConfigurationForRuleEditDto()
                {
                    Id = item.Id,
                    RuleId = item.RuleId,
                    Connector = item.Connector,
                    DataPointId = item.DataPointId,
                    DataPointName = item.DataPoint.Name,
                    Operand = item.Operand,
                    Value = item.Value,
                    DataPointType = item.GetType().Name,
                    GroupName = item.GroupName
                };

                configurationOutput.Add(ruleObj);

                if (groupName == item.GroupName || ruleSubgroup.Capacity == 0)
                {
                    ruleSubgroup.Add(ruleObj);
                }
                else
                {
                    if (ruleSubgroup.Capacity > 0)
                    {
                        ruleGroup.Add(new RuleObjGroup()
                        {
                            GroupName = groupName,
                            Connector = connector,
                            RuleObjs = ruleSubgroup
                        });

                        ruleSubgroup = new List<RuleConfigurationForRuleEditDto>
                        {
                            ruleObj
                        };
                    }
                }

                groupName = item.GroupName;
                connector = item.Connector;
            }

            if (ruleSubgroup.Capacity > 0)
            {
                ruleGroup.Add(new RuleObjGroup()
                {
                    GroupName = groupName,
                    Connector = connector,
                    RuleObjs = ruleSubgroup
                });
            }

            output.RuleConfigurations = configurationOutput.ToArray();
            output.RuleEscalations = escalationOutput.ToArray();
            output.Rule.RuleGroups = ruleGroup;
            return output;
        }

        public async Task<string> CreateOrEdit(CreateOrEditRuleDto input)
        {
            string output = "";

            if (input.Id == null)
            {
                output = await Create(input);
            }
            else
            {
                output = await Update(input);
            }

            return output;
        }

        public async Task MakeCopy(EntityDto<Guid> input)
        {
            var rule = await GetRuleForEdit(input);

            List<CreateOrEditRuleEscalationDto> ruleEscalations = new List<CreateOrEditRuleEscalationDto>();

            foreach (var item in rule.RuleEscalations)
            {
                ruleEscalations.Add(new CreateOrEditRuleEscalationDto
                {
                    RuleId = item.RuleId,
                    UserId = item.UserId
                });
            }

            CreateOrEditRuleDto ruleCopy = new CreateOrEditRuleDto
            {
                Action = rule.Rule.Action,
                Active = false,
                BaseConnector = rule.Rule.BaseConnector,
                Description = rule.Rule.Description,
                Name = rule.Rule.Name + " - Copy",
                RuleEscalations = ruleEscalations.ToArray(),
                RuleGroups = rule.Rule.RuleGroups,
                RuleStatus = RuleStatus.Draft,
                Score = rule.Rule.Score,
                Severity = rule.Rule.Severity,
                TransactionTypeId = rule.Rule.TransactionTypeId,
                IsApproved = false
            };

            var output = await Create(ruleCopy);
        }

        [AbpAuthorize(AppPermissions.Pages_Rules_Create)]
        private async Task<string> Create(CreateOrEditRuleDto input)
        {
            var rule = ObjectMapper.Map<Rule>(input);

            JObject ruleJSON = GenerateJsonLogicRule(input);
            rule.JsonLogicRule = ruleJSON.ToString();
            rule.RuleStatus = RuleStatus.Draft;

            var id = await _ruleRepository.InsertAndGetIdAsync(rule);

            //foreach (var ruleConfig in input.RuleConfigurations)
            //{
            //    await _ruleConfigurationRepository.InsertAsync(new RuleConfiguration()
            //    {
            //        Connector = ruleConfig.Connector,
            //        DataPointId = ruleConfig.DataPointId,
            //        Operand = ruleConfig.Operand,
            //        RuleId = id,
            //        Value = ruleConfig.Value
            //    });
            //}

            foreach (var ruleGroup in input.RuleGroups)
            {
                foreach (var ruleObj in ruleGroup.RuleObjs)
                {
                    await _ruleConfigurationRepository.InsertAsync(new RuleConfiguration()
                    {
                        Connector = ruleGroup.Connector,
                        DataPointId = ruleObj.DataPointId,
                        Operand = ruleObj.Operand,
                        RuleId = id,
                        Value = ruleObj.Value,
                        GroupName = ruleGroup.GroupName
                    });
                }
            }

            
            foreach (var escalation in input.RuleEscalations)
            {
                await _ruleEscalationRepository.InsertAsync(new RuleEscalation()
                {
                    RuleId = id,
                    UserId = escalation.UserId
                });
            }
            

            JObject output = new JObject(
                                        new JProperty("id", rule.Id),
                                        new JProperty("rule", ruleJSON)
                              );

            string output_string = Regex.Replace(output.ToString(), @"\t|\n|\r", "");


            //SendRuleToRulesListener(output_string);
            return output_string;
        }

        [AbpAuthorize(AppPermissions.Pages_Rules_Edit)]
        private async Task<string> Update(CreateOrEditRuleDto input)
        {
            var rule = await _ruleRepository.FirstOrDefaultAsync((Guid)input.Id);

            if (rule.RuleStatus == RuleStatus.Published)
            {
                throw new UserFriendlyException(L("EditApprovedRuleError"));
            }

            rule.Description = input.Description;
            rule.Severity = input.Severity;
            rule.Name = input.Name;
            rule.TransactionTypeId = input.TransactionTypeId;
            rule.Action = input.Action;
            rule.BaseConnector = input.BaseConnector;
            rule.RuleStatus = input.RuleStatus;

            JObject ruleJSON = GenerateJsonLogicRule(input);
            rule.JsonLogicRule = ruleJSON.ToString();


            var existingRuleConfigurations = await _ruleConfigurationRepository.GetAllListAsync(x => x.Id == input.Id);

            //Delete Existing Rule Configuration for existing rule
            await _ruleConfigurationRepository.HardDeleteAsync(x => x.RuleId == input.Id);
            await _ruleEscalationRepository.HardDeleteAsync(x => x.RuleId == input.Id);

            //foreach (var item in input.RuleConfigurations)
            //{
            //    await _ruleConfigurationRepository.InsertAsync(new RuleConfiguration()
            //    {
            //        Connector = item.Connector,
            //        DataPointId = item.DataPointId,
            //        Operand = item.Operand,
            //        RuleId = (Guid)input.Id,
            //        Value = item.Value
            //    });
            //}


            foreach (var ruleGroup in input.RuleGroups)
            {
                foreach (var ruleObj in ruleGroup.RuleObjs)
                {
                    await _ruleConfigurationRepository.InsertAsync(new RuleConfiguration()
                    {
                        Connector = ruleGroup.Connector,
                        DataPointId = ruleObj.DataPointId,
                        Operand = ruleObj.Operand,
                        RuleId = (Guid)input.Id,
                        Value = ruleObj.Value,
                        GroupName = ruleGroup.GroupName
                    });
                }
            }

            foreach (var escalation in input.RuleEscalations)
            {
                await _ruleEscalationRepository.InsertAsync(new RuleEscalation()
                {
                    RuleId = (Guid)input.Id,
                    UserId = escalation.UserId
                });
            }

            JObject output = new JObject(
                                        new JProperty("id", rule.Id),
                                        new JProperty("rule", rule.JsonLogicRule)
                              );

            string output_string = Regex.Replace(output.ToString(), @"\t|\n|\r", "");

            return output_string;
            //SendRuleToRulesListener(output_string);
        }

        [AbpAuthorize(AppPermissions.Pages_Rules_Approve)]
        public async Task UpdateStatus(UpdateRuleStatusInputDto input)
        {
            var rule = await _ruleRepository.FirstOrDefaultAsync(input.Id);
            var transType = await _transactionTypeRepository.FirstOrDefaultAsync((Guid)rule.TransactionTypeId);

            if (input.NewStatus == RuleStatus.Published)
            {
                //MakerChecker
                if (rule.CreatorUserId == AbpSession.UserId)
                {
                    throw new UserFriendlyException(L("MakerCheckerError"));
                }

                rule.IsApproved = true;
                rule.ApprovedDate = DateTime.Now;
                rule.ApprovedBy = AbpSession.UserId;
                rule.RuleStatus = RuleStatus.Published;

                JObject output = new JObject(
                                            new JProperty("id", rule.Id),
                                            new JProperty("transactionType", transType != null ? transType.Name : ""),
                                            new JProperty("isActive", rule.Active),
                                            new JProperty("rule", rule.JsonLogicRule)
                                  );

                string output_string = Regex.Replace(output.ToString(), @"\t|\n|\r", "");

                SendRuleToRulesListener(output_string);
                //return output_string;
            }
            else
            {
                rule.RuleStatus = input.NewStatus;
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Rules_Activate)]
        public async Task ToggleActiveStatus(EntityDto<Guid> input)
        {
            var rule = await _ruleRepository.FirstOrDefaultAsync(input.Id);

            if (rule.RuleStatus != RuleStatus.Published)
            {
                throw new UserFriendlyException(L("ActivateNotApprovedRuleError"));
            }

            if (rule.Active)
            {
                rule.Active = false;
            }
            else
            {
                rule.Active = true;
            }

            //Todo: Update RulesListener
        }

        private JObject GenerateJsonLogicRule(CreateOrEditRuleDto input)
        {
            JObject rule = new JObject();
            JArray group = new JArray();


            foreach (var ruleGroup in input.RuleGroups)
            {
                JArray subgroup = new JArray();
                foreach (var ruleObj in ruleGroup.RuleObjs)
                {
                    var datapoint = _dataPointRepository.FirstOrDefault(ruleObj.DataPointId);

                    JValue datapoint_value = new JValue("");

                    if (datapoint.DataType == DataTypes.DateTime)
                    {
                        datapoint_value = new JValue(Convert.ToDateTime(ruleObj.Value));
                    }
                    else if (datapoint.DataType == DataTypes.Decimal)
                    {
                        datapoint_value = new JValue(Convert.ToDecimal(ruleObj.Value));
                    }
                    else if (datapoint.DataType == DataTypes.Integer)
                    {
                        datapoint_value = new JValue(Convert.ToInt32(ruleObj.Value));
                    }
                    else
                    {
                        datapoint_value = new JValue(ruleObj.Value.ToString());
                    }

                    subgroup.Add(
                        new JObject(
                                new JProperty(ruleObj.Operand.ToLower(),
                                                    new JArray(
                                                        new JObject(
                                                            new JProperty("var", datapoint.Name)
                                                            ),
                                                             datapoint_value
                                                        )
                                )
                        )
                    );
                }

                group.Add(new JObject(
                        new JProperty(ruleGroup.Connector.ToLower(), subgroup)
                    ));
            }

            rule.Add(new JProperty(input.BaseConnector.ToLower(), group));

            string rule_string = Regex.Replace(rule.ToString(), @"\t|\n|\r", "");

            return rule;
        }

        [AbpAuthorize(AppPermissions.Pages_Rules_Delete)]
        public async Task Delete(EntityDto<Guid> input)
        {
            var rule = await _ruleRepository.FirstOrDefaultAsync(input.Id);

            if (rule.RuleStatus == RuleStatus.Published)
            {
                throw new UserFriendlyException(L("ActivateNotApprovedRuleError"));
            }

            await _ruleRepository.DeleteAsync(input.Id);
        }

        public async Task<FileDto> GetRulesToExcel(GetAllRulesForExcelInput input)
        {
            var severityFilter = (Severity)input.SeverityFilter;
            var actionFilter = (Actions)input.ActionFilter;

            var filteredRules = _ruleRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter) || e.Description.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name.ToLower() == input.NameFilter.ToLower().Trim())
                        .WhereIf(input.SeverityFilter > -1, e => e.Severity == severityFilter)
                        .WhereIf(input.ActionFilter > -1, e => e.Action == actionFilter);


            var query = (from o in filteredRules
                         join o1 in _transactionTypeRepository.GetAll() on o.TransactionTypeId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()

                         select new GetRuleForView()
                         {
                             Rule = ObjectMapper.Map<RuleDto>(o),
                             TransactionTypeName = s1 == null ? "" : s1.Name.ToString()
                         })
                        .WhereIf(!string.IsNullOrWhiteSpace(input.TransactionTypeNameFilter), e => e.TransactionTypeName.ToLower() == input.TransactionTypeNameFilter.ToLower().Trim());


            var ruleListDtos = await query.ToListAsync();

            return _rulesExcelExporter.ExportToFile(ruleListDtos);
        }

        private string SendRuleToRulesListener(string value)
        {
            string RuleListenerURL = _appConfiguration["URLs:RuleListener_URL"];

            Uri url = new Uri(string.Format(RuleListenerURL));

            var request = HttpWebRequest.Create(url);
            var byteData = Encoding.ASCII.GetBytes(value);
            request.ContentType = "application/json";
            request.Method = "POST";

            try
            {
                using (var stream = request.GetRequestStream())
                {
                    stream.Write(byteData, 0, byteData.Length);
                }
                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                return responseString;
            }
            catch (WebException e)
            {
                Logger.Error("Error Sending Rule to RulesListener: " + e.Message);
                return null;
            }
        }
    }
}