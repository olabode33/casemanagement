using System.Collections.Generic;
using Test.Payloads.Dtos;
using Test.Dto;

namespace Test.Payloads.Exporting
{
    public interface IPayloadsExcelExporter
    {
        FileDto ExportToFile(List<GetPayloadForView> payloads);
    }
}