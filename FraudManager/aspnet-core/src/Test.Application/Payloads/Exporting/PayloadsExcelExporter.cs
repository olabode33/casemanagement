using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using Test.DataExporting.Excel.EpPlus;
using Test.Payloads.Dtos;
using Test.Dto;
using Test.Storage;

namespace Test.Payloads.Exporting
{
    public class PayloadsExcelExporter : EpPlusExcelExporterBase, IPayloadsExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public PayloadsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetPayloadForView> payloads)
        {
            return CreateExcelPackage(
                "Payloads.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Payloads"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Name"),
                        (L("TransactionType")) + L("Name")
                        );

                    AddObjects(
                        sheet, 2, payloads,
                        _ => _.Payload.Name,
                        _ => _.TransactionTypeName
                        );

					

                });
        }
    }
}
