using Test.TransactionTypes;

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Test.Payloads.Exporting;
using Test.Payloads.Dtos;
using Test.Dto;
using Abp.Application.Services.Dto;
using Test.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Test.PayloadProperties;
using Test.PayloadProperties.Dtos;

namespace Test.Payloads
{
    [AbpAuthorize(AppPermissions.Pages_Payloads)]
    public class PayloadsAppService : TestAppServiceBase, IPayloadsAppService
    {
        private readonly IRepository<Payload, Guid> _payloadRepository;
        private readonly IRepository<DataPoint, Guid> _dataPointRepository;
        private readonly IRepository<PayloadProperty, Guid> _payloadPropertyRepository;
        private readonly IPayloadsExcelExporter _payloadsExcelExporter;
        private readonly IRepository<TransactionType, Guid> _transactionTypeRepository;


        public PayloadsAppService(IRepository<DataPoint, Guid> _dataPointRepository,
            IRepository<PayloadProperty, Guid> _payloadPropertyRepository,
            IRepository<Payload, Guid> payloadRepository, IPayloadsExcelExporter payloadsExcelExporter, IRepository<TransactionType, Guid> transactionTypeRepository)
        {
            this._dataPointRepository = _dataPointRepository;
            this._payloadPropertyRepository = _payloadPropertyRepository;
            _payloadRepository = payloadRepository;
            _payloadsExcelExporter = payloadsExcelExporter;
            _transactionTypeRepository = transactionTypeRepository;

        }

        public async Task<PagedResultDto<GetDataPointsForView>> GetAll(GetAllPayloadsInput input)
        {

            var filteredPayloads = _payloadRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter) || e.Description.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name.ToLower() == input.NameFilter.ToLower().Trim());


            var query = (from o in filteredPayloads
                         join o1 in _transactionTypeRepository.GetAll() on o.TransactionTypeId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()

                         select new GetDataPointsForView()
                         {
                              Name = o.Name, Id = o.Id, Description = o.Description, DataClass = "Payload", 
                             TransactionTypeName = s1 == null ? "" : s1.Name.ToString()
                         })
                        .WhereIf(!string.IsNullOrWhiteSpace(input.TransactionTypeNameFilter), e => e.TransactionTypeName.ToLower() == input.TransactionTypeNameFilter.ToLower().Trim());



            List< GetDataPointsForView> payloads = await query.ToListAsync();

            var filteredDataPoints = _dataPointRepository.GetAll()
            .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter) || e.Description.Contains(input.Filter))
            .WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name.ToLower() == input.NameFilter.ToLower().Trim());

            query = (from o in filteredDataPoints
                     where o.GetType().Name == "DataPoint"
                     select new GetDataPointsForView()
                     {
                         Name = o.Name,
                         Id = o.Id,
                         Description = o.Description,
                         DataClass = "Standing Data",
                          DataType = o.DataType.ToString()
                     });

            List<GetDataPointsForView> dataPoints = await query.ToListAsync();

            dataPoints.AddRange(payloads);

            var output = dataPoints.AsQueryable()
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input)
                .ToList();

            var totalCount = await query.CountAsync();

            return new PagedResultDto<GetDataPointsForView>(
                totalCount,
                output
            );
        }

        [AbpAuthorize(AppPermissions.Pages_Payloads_Edit)]
        public async Task<GetPayloadForEditOutput> GetPayloadForEdit(EntityDto<Guid> input)
        {
            var payload = await _payloadRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetPayloadForEditOutput
            {
                Payload = new CreateOrEditPayloadDto()
                {
                    Description = payload.Description,
                    Id = payload.Id,
                    Name = payload.Name,
                    TransactionTypeId = payload.TransactionTypeId
                }
            };

            if (output.Payload.TransactionTypeId != null)
            {
                var transactionType = await _transactionTypeRepository.FirstOrDefaultAsync((Guid)output.Payload.TransactionTypeId);
                output.TransactionTypeName = transactionType.Name.ToString();
            }


            var existingpayloadProperties = await _payloadPropertyRepository.GetAllListAsync(x => x.PayloadId == input.Id);

            List<CreateOrEditPayloadPropertyDto> payloadProperties = new List<CreateOrEditPayloadPropertyDto>();

            foreach (var item in existingpayloadProperties)
            {
                payloadProperties.Add(new CreateOrEditPayloadPropertyDto()
                {
                    Id = item.Id,
                    DataType = item.DataType,
                    DataTypeName = item.DataType.ToString(),
                    Description = item.Description,
                    Name = item.Name,
                    PayloadId = item.PayloadId
                });
            }

            output.Payload.PayloadProperties = payloadProperties.ToArray();
            return output;
        }

        public async Task CreateOrEdit(CreateOrEditPayloadDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Payloads_Create)]
        private async Task Create(CreateOrEditPayloadDto input)
        {
            var payload = ObjectMapper.Map<Payload>(input);
            var id = await _payloadRepository.InsertAndGetIdAsync(payload);

            foreach (var property in input.PayloadProperties)
            {
                await _payloadPropertyRepository.InsertAsync(new PayloadProperty()
                {
                     Id = Guid.NewGuid(),
                    DataType = property.DataType,
                    Name = property.Name,
                    PayloadId = id
                });
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Payloads_Edit)]
        private async Task Update(CreateOrEditPayloadDto input)
        {
            var payload = await _payloadRepository.FirstOrDefaultAsync((Guid)input.Id);
            ObjectMapper.Map(input, payload);

            var existingpayloadProperties = await _payloadPropertyRepository.GetAllListAsync(x => x.PayloadId == input.Id);

            // Create new config
            var payloadProperties = input.PayloadProperties.Where(x => x.Id == Guid.Empty || x.Id == null);
            foreach (var item in payloadProperties)
            {
                await _payloadPropertyRepository.InsertAsync(new PayloadProperty()
                {
                    Name = item.Name,
                    PayloadId = (Guid)input.Id,
                    DataType = item.DataType,
                    Description = item.Description
                });
            }

            // update existing and delete removed config
            foreach (var item in input.PayloadProperties.Where(x => x.Id != Guid.Empty && x.Id != null))
            {
                var payloadProperty = existingpayloadProperties.FirstOrDefault(x => x.Id == item.Id);

                payloadProperty.DataType = item.DataType;
                payloadProperty.Description = item.Description;
                payloadProperty.Name = item.Name;
                payloadProperty.PayloadId = item.PayloadId;

                await _payloadPropertyRepository.UpdateAsync(payloadProperty);
            }

            //delete removed config
            foreach (var item in existingpayloadProperties)
            {
                int checkIfRemoved = input.PayloadProperties.Count(x => x.Id == item.Id);

                if (checkIfRemoved == 0)
                {
                    await _payloadPropertyRepository.DeleteAsync(item);
                }
            }

        }

        [AbpAuthorize(AppPermissions.Pages_Payloads_Delete)]
        public async Task Delete(EntityDto<Guid> input)
        {
            await _payloadRepository.DeleteAsync(input.Id);
        }

        public async Task<FileDto> GetPayloadsToExcel(GetAllPayloadsForExcelInput input)
        {

            var filteredPayloads = _payloadRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter) || e.Description.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name.ToLower() == input.NameFilter.ToLower().Trim());


            var query = (from o in filteredPayloads
                         join o1 in _transactionTypeRepository.GetAll() on o.TransactionTypeId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()

                         select new GetPayloadForView()
                         {
                             Payload = ObjectMapper.Map<PayloadDto>(o),
                             TransactionTypeName = s1 == null ? "" : s1.Name.ToString()
                         })
                        .WhereIf(!string.IsNullOrWhiteSpace(input.TransactionTypeNameFilter), e => e.TransactionTypeName.ToLower() == input.TransactionTypeNameFilter.ToLower().Trim());


            var payloadListDtos = await query.ToListAsync();

            return _payloadsExcelExporter.ExportToFile(payloadListDtos);
        }

        public async Task CreateDataPoint(CreateOrEditDataPointDto input)
        {
            DataPoint datapoint = new DataPoint()
            {
                Id = Guid.NewGuid(),
                Name = input.Name,
                DataType = input.DataType,
                Description = input.Description
            };
            await _dataPointRepository.InsertAndGetIdAsync(datapoint);
        }

        public async Task<CreateOrEditDataPointDto> GetDataPointForEdit(Guid input)
        {
            var dataPoint = await _dataPointRepository.FirstOrDefaultAsync((Guid)input);

            CreateOrEditDataPointDto output = new CreateOrEditDataPointDto()
            {
                Id = dataPoint.Id,
                Name = dataPoint.Name,
                DataType = dataPoint.DataType,
                Description = dataPoint.Description
            };

            return output;
        }

        public async Task UpdateDataPoint(CreateOrEditDataPointDto input)
        {
            var dataPoint = await _dataPointRepository.FirstOrDefaultAsync((Guid)input.Id);
            ObjectMapper.Map(input, dataPoint);
        }

        public async Task DeleteDataPoint(EntityDto<Guid> input)
        {
            await _dataPointRepository.DeleteAsync(input.Id);
        }

        [AbpAuthorize(AppPermissions.Pages_Payloads)]
        public async Task<PagedResultDto<TransactionTypeLookupTableDto>> GetAllTransactionTypeForLookupTable(Dtos.GetAllForLookupTableInput input)
        {
            var query = _transactionTypeRepository.GetAll().WhereIf(
                   !string.IsNullOrWhiteSpace(input.Filter),
                  e => e.Name.ToString().Contains(input.Filter)
               );

            var totalCount = await query.CountAsync();

            var transactionTypeList = await query
                .PageBy(input)
                .ToListAsync();

            var lookupTableDtoList = new List<TransactionTypeLookupTableDto>();
            foreach (var transactionType in transactionTypeList)
            {
                lookupTableDtoList.Add(new TransactionTypeLookupTableDto
                {
                    Id = transactionType.Id.ToString(),
                    DisplayName = transactionType.Name?.ToString()
                });
            }

            return new PagedResultDto<TransactionTypeLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
        }
    }
}