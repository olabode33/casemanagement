using System.Collections.Generic;
using Test.RuleEscalations.Dtos;
using Test.Dto;

namespace Test.RuleEscalations.Exporting
{
    public interface IRuleEscalationsExcelExporter
    {
        FileDto ExportToFile(List<GetRuleEscalationForView> ruleEscalations);
    }
}