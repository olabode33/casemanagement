using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using Test.DataExporting.Excel.EpPlus;
using Test.RuleEscalations.Dtos;
using Test.Dto;
using Test.Storage;

namespace Test.RuleEscalations.Exporting
{
    public class RuleEscalationsExcelExporter : EpPlusExcelExporterBase, IRuleEscalationsExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public RuleEscalationsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetRuleEscalationForView> ruleEscalations)
        {
            return CreateExcelPackage(
                "RuleEscalations.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("RuleEscalations"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        (L("Rule")) + L("Name"),
                        (L("User")) + L("Name")
                        );

                    AddObjects(
                        sheet, 2, ruleEscalations,
                        _ => _.RuleName,
                        _ => _.UserName
                        );

					

                });
        }
    }
}
