using Test.Rules;
using Test.Authorization.Users;

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Test.RuleEscalations.Exporting;
using Test.RuleEscalations.Dtos;
using Test.Dto;
using Abp.Application.Services.Dto;
using Test.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;

namespace Test.RuleEscalations
{
	[AbpAuthorize(AppPermissions.Pages_RuleEscalations)]
    public class RuleEscalationsAppService : TestAppServiceBase, IRuleEscalationsAppService
    {
		 private readonly IRepository<RuleEscalation, Guid> _ruleEscalationRepository;
		 private readonly IRuleEscalationsExcelExporter _ruleEscalationsExcelExporter;
		 private readonly IRepository<Rule,Guid> _ruleRepository;
		 private readonly IRepository<User,long> _userRepository;
		 

		  public RuleEscalationsAppService(IRepository<RuleEscalation, Guid> ruleEscalationRepository, IRuleEscalationsExcelExporter ruleEscalationsExcelExporter , IRepository<Rule, Guid> ruleRepository, IRepository<User, long> userRepository) 
		  {
			_ruleEscalationRepository = ruleEscalationRepository;
			_ruleEscalationsExcelExporter = ruleEscalationsExcelExporter;
			_ruleRepository = ruleRepository;
		_userRepository = userRepository;
		
		  }

		 public async Task<PagedResultDto<GetRuleEscalationForView>> GetAll(GetAllRuleEscalationsInput input)
         {
			
			var filteredRuleEscalations = _ruleEscalationRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false );


			var query = (from o in filteredRuleEscalations
                         join o1 in _ruleRepository.GetAll() on o.RuleId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         
                         join o2 in _userRepository.GetAll() on o.UserId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()
                         
                         select new GetRuleEscalationForView() {
							RuleEscalation = ObjectMapper.Map<RuleEscalationDto>(o),
                         	RuleName = s1 == null ? "" : s1.Name.ToString(),
                         	UserName = s2 == null ? "" : s2.Name.ToString()
						})
						.WhereIf(!string.IsNullOrWhiteSpace(input.RuleNameFilter), e => e.RuleName.ToLower() == input.RuleNameFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.UserNameFilter), e => e.UserName.ToLower() == input.UserNameFilter.ToLower().Trim());

            var totalCount = await query.CountAsync();

            var ruleEscalations = await query
                .OrderBy(input.Sorting ?? "ruleEscalation.id asc")
                .PageBy(input)
                .ToListAsync();

            return new PagedResultDto<GetRuleEscalationForView>(
                totalCount,
                ruleEscalations
            );
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_RuleEscalations_Edit)]
		 public async Task<GetRuleEscalationForEditOutput> GetRuleEscalationForEdit(EntityDto<Guid> input)
         {
            var ruleEscalation = await _ruleEscalationRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetRuleEscalationForEditOutput {RuleEscalation = ObjectMapper.Map<CreateOrEditRuleEscalationDto>(ruleEscalation)};

		    if (output.RuleEscalation.RuleId != null)
            {
                var rule = await _ruleRepository.FirstOrDefaultAsync((Guid)output.RuleEscalation.RuleId);
                output.RuleName = rule.Name.ToString();
            }

		    if (output.RuleEscalation.UserId != null)
            {
                var user = await _userRepository.FirstOrDefaultAsync((long)output.RuleEscalation.UserId);
                output.UserName = user.Name.ToString();
            }
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditRuleEscalationDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_RuleEscalations_Create)]
		 private async Task Create(CreateOrEditRuleEscalationDto input)
         {
            var ruleEscalation = ObjectMapper.Map<RuleEscalation>(input);

			

            await _ruleEscalationRepository.InsertAsync(ruleEscalation);
         }

		 [AbpAuthorize(AppPermissions.Pages_RuleEscalations_Edit)]
		 private async Task Update(CreateOrEditRuleEscalationDto input)
         {
            var ruleEscalation = await _ruleEscalationRepository.FirstOrDefaultAsync((Guid)input.Id);
             ObjectMapper.Map(input, ruleEscalation);
         }

		 [AbpAuthorize(AppPermissions.Pages_RuleEscalations_Delete)]
         public async Task Delete(EntityDto<Guid> input)
         {
            await _ruleEscalationRepository.DeleteAsync(input.Id);
         } 

		public async Task<FileDto> GetRuleEscalationsToExcel(GetAllRuleEscalationsForExcelInput input)
         {
			
			var filteredRuleEscalations = _ruleEscalationRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false );


			var query = (from o in filteredRuleEscalations
                         join o1 in _ruleRepository.GetAll() on o.RuleId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         
                         join o2 in _userRepository.GetAll() on o.UserId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()
                         
                         select new GetRuleEscalationForView() { 
							RuleEscalation = ObjectMapper.Map<RuleEscalationDto>(o),
                         	RuleName = s1 == null ? "" : s1.Name.ToString(),
                         	UserName = s2 == null ? "" : s2.Name.ToString()
						 })
						.WhereIf(!string.IsNullOrWhiteSpace(input.RuleNameFilter), e => e.RuleName.ToLower() == input.RuleNameFilter.ToLower().Trim())
						.WhereIf(!string.IsNullOrWhiteSpace(input.UserNameFilter), e => e.UserName.ToLower() == input.UserNameFilter.ToLower().Trim());


            var ruleEscalationListDtos = await query.ToListAsync();

            return _ruleEscalationsExcelExporter.ExportToFile(ruleEscalationListDtos);
         }


		[AbpAuthorize(AppPermissions.Pages_RuleEscalations)]
         public async Task<PagedResultDto<UserLookupTableDto>> GetAllUserForLookupTable(GetAllForLookupTableInput input)
         {
             var query = _userRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                   e=> e.Name.ToString().Contains(input.Filter)
                );

            var totalCount = await query.CountAsync();

            var userList = await query
                .PageBy(input)
                .ToListAsync();

			var lookupTableDtoList = new List<UserLookupTableDto>();
			foreach(var user in userList){
				lookupTableDtoList.Add(new UserLookupTableDto
				{
					Id = user.Id,
					DisplayName = user.FullName.ToString()
				});
			}

            return new PagedResultDto<UserLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
         }
    }
}