﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test
{
    public enum Actions
    {
        Stop,
        Escalate,
        Authorization
    }

    public enum Status
    {
        Open,
        Closed,
        Verifying
    }

    public enum Sources
    {
        System, User
    }

    public enum Severity
    {
        Low,Medium, High
    }

    public enum DataTypes
    {
        String,Integer,Decimal, DateTime
    }

    public enum DectectionSource
    {
        Rules, Algorithm
    }

    public enum Conclusion
    {
        Pending, Fraud, FalsePositive, AttemptedFraud
    }

    public enum TransactionStatus
    {
        Blocked, Completed
    }

    public enum RuleStatus
    {
        Draft, AwaitingApproval, Published
    }

    public enum TaskType
    {
        Auto, Manual, Reassigned, Generic
    }


}
