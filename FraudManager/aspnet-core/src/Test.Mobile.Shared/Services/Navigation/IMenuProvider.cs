using System.Collections.Generic;
using MvvmHelpers;
using Test.Models.NavigationMenu;

namespace Test.Services.Navigation
{
    public interface IMenuProvider
    {
        ObservableRangeCollection<NavigationMenuItem> GetAuthorizedMenuItems(Dictionary<string, string> grantedPermissions);
    }
}