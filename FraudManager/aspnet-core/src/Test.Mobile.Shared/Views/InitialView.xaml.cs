﻿using Xamarin.Forms;

namespace Test.Views
{
    public partial class InitialView : ContentPage, IXamarinView
    {
        public InitialView()
        {
            InitializeComponent();
        }
    }
}