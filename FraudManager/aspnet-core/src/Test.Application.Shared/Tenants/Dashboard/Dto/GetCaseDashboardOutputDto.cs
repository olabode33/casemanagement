﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test.Tenants.Dashboard.Dto
{
    public class GetCaseDashboardOutputDto
    {
        public int NewCases { get; set; }
        public int PendingCases { get; set; }
        public int OutstandingCases { get; set; }
        public int AssignedTasks { get; set; }
        public List<GetRecentCasesDashboardOutputDto> RecentCases { get; set; }
        public List<GetAssignedTaskDashboardOutputDto> Tasks { get; set; }
    }

    public class GetRecentCasesDashboardOutputDto
    {
        public string CaseId { get; set; }
        public DateTime CreationDate { get; set; }
        public string TimeSpan { get; set; }
        public string CustomerName { get; set; }
        public string DetectionSource { get; set; }
        public string TransactionType { get; set; }
        public decimal TransactionAmout { get; set; }
        public Status Status { get; set; }
    }

    public class GetAssignedTaskDashboardOutputDto
    {
        public int TaskId { get; set; }
        public string Subject { get; set; }
        public string Priority { get; set; }
        public string AssignedBy { get; set; }
        public string TimeSpan { get; set; }
        public Status Status { get; set; }
    }
}
