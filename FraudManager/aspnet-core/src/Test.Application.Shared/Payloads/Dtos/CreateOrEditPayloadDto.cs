
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using Test.PayloadProperties.Dtos;

namespace Test.Payloads.Dtos
{
    public class CreateOrEditPayloadDto : EntityDto<Guid?>
    {

		[Required]
		[StringLength(PayloadConsts.MaxNameLength, MinimumLength = PayloadConsts.MinNameLength)]
		public string Name { get; set; }
		
		
		public string Description { get; set; }
		
		
		 public Guid TransactionTypeId { get; set; }

        public CreateOrEditPayloadPropertyDto[] PayloadProperties { get; set; }
    }
}