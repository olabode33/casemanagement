using Abp.Application.Services.Dto;

namespace Test.Payloads.Dtos
{
    public class TransactionTypeLookupTableDto
    {
		public string Id { get; set; }

		public string DisplayName { get; set; }
    }
}