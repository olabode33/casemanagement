
using System;
using Abp.Application.Services.Dto;

namespace Test.Payloads.Dtos
{
    public class PayloadDto : EntityDto<Guid>
    {
		public string Name { get; set; }


		 public Guid TransactionTypeId { get; set; }

		 
    }
}