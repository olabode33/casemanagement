using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace Test.Payloads.Dtos
{
    public class GetPayloadForEditOutput
    {
		public CreateOrEditPayloadDto Payload { get; set; }

		public string TransactionTypeName { get; set;}


    }
}