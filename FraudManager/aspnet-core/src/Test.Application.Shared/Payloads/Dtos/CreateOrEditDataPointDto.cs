﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Test.Payloads.Dtos
{
    public class CreateOrEditDataPointDto : EntityDto<Guid?>
    {
        public string Name { get; set; }

        public DataTypes DataType { get; set; }

        public string Description { get; set; }
    }
}
