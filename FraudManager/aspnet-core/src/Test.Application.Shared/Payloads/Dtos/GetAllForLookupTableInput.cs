using Abp.Application.Services.Dto;

namespace Test.Payloads.Dtos
{
    public class GetAllForLookupTableInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }
    }
}