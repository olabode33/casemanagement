using System;

namespace Test.Payloads.Dtos
{
    public class GetPayloadForView
    {
		public PayloadDto Payload { get; set; }

		public string TransactionTypeName { get; set;}


    }


    public class GetDataPointsForView
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public string DataClass { get; set; }

        public string DataType { get; set; }

        public Guid Id { get; set; }

        public string TransactionTypeName { get; set; }
        

    }
}