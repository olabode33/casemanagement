using Abp.Application.Services.Dto;
using System;

namespace Test.Payloads.Dtos
{
    public class GetAllPayloadsForExcelInput
    {
		public string Filter { get; set; }

		public string NameFilter { get; set; }


		 public string TransactionTypeNameFilter { get; set; }

		 
    }
}