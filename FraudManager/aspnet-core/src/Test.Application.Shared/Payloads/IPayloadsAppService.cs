using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Test.Payloads.Dtos;
using Test.Dto;

namespace Test.Payloads
{
    public interface IPayloadsAppService : IApplicationService 
    {
        Task<PagedResultDto<GetDataPointsForView>> GetAll(GetAllPayloadsInput input);

		Task<GetPayloadForEditOutput> GetPayloadForEdit(EntityDto<Guid> input);

		Task CreateOrEdit(CreateOrEditPayloadDto input);

		Task Delete(EntityDto<Guid> input);

		Task<FileDto> GetPayloadsToExcel(GetAllPayloadsForExcelInput input);
        
		Task<PagedResultDto<TransactionTypeLookupTableDto>> GetAllTransactionTypeForLookupTable(GetAllForLookupTableInput input);

        Task<CreateOrEditDataPointDto> GetDataPointForEdit(Guid input);

        Task DeleteDataPoint(EntityDto<Guid> input);

        Task UpdateDataPoint(CreateOrEditDataPointDto input);

        Task CreateDataPoint(CreateOrEditDataPointDto input);
    }
}