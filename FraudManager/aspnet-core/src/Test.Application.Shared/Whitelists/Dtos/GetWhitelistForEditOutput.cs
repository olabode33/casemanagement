using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace Test.Whitelists.Dtos
{
    public class GetWhitelistForEditOutput
    {
		public CreateOrEditWhitelistDto Whitelist { get; set; }

		public string TransactionTypeName { get; set;}

		public string UserName { get; set;}


    }
}