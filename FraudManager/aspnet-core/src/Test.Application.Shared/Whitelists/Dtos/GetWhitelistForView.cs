namespace Test.Whitelists.Dtos
{
    public class GetWhitelistForView
    {
		public WhitelistDto Whitelist { get; set; }

		public string TransactionTypeName { get; set;}

		public string UserName { get; set;}


    }
}