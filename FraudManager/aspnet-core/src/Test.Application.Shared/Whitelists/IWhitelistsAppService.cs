using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Test.Whitelists.Dtos;
using Test.Dto;

namespace Test.Whitelists
{
    public interface IWhitelistsAppService : IApplicationService 
    {
        Task<PagedResultDto<GetWhitelistForView>> GetAll(GetAllWhitelistsInput input);

		Task<GetWhitelistForEditOutput> GetWhitelistForEdit(EntityDto<Guid> input);

		Task CreateOrEdit(CreateOrEditWhitelistDto input);

		Task Delete(EntityDto<Guid> input);

		Task<FileDto> GetWhitelistsToExcel(GetAllWhitelistsForExcelInput input);

        Task Suspend(Guid input);

        Task Approve(Guid input);
    }
}