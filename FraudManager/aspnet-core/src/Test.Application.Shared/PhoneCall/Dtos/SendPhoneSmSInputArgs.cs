﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test.PhoneCall.Dtos
{
    [Serializable]
    public class SendPhoneSmSInputArgs
    {
        public string ToPhoneNumber { get; set; }
        public string MessageBody { get; set; }
        public string TransactionId { get; set; }
        public double Amount { get; set; }
        public string Isfraud { get; set; }
        public string VerificationCode { get; set; }
        public string amountWords { get; set; }
        public string TwilioCallback_URL { get; set; }

    }
}
