﻿using Abp.Events.Bus;
using System;
using System.Collections.Generic;
using System.Text;

namespace Test.PhoneCall.Dtos
{
    public class TwilioInputDto: EventData
    {
        public string ToPhoneNumber { get; set; }
        public string MessageBody { get; set; }
        public string TransactionId { get; set; }
        public double Amount { get; set; }
        public string Isfraud { get; set; }
        public string VerificationCode { get; set; }
        public string amountWords { get; set; }

    }
}
