﻿using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Test.PhoneCall.Dtos;

namespace Test.PhoneCall
{
    public interface IPhoneAppService
    {
        Task ContactCustomer(TwilioInputDto input);
        Task<string> GetResultFromMLEngine([FromBody] string input);
        Task HandleNewCase(string input);
    }
}