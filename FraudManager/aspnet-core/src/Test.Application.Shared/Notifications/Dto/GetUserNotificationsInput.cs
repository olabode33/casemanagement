﻿using Abp.Notifications;
using Test.Dto;

namespace Test.Notifications.Dto
{
    public class GetUserNotificationsInput : PagedInputDto
    {
        public UserNotificationState? State { get; set; }
    }
}