using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Test.RuleEscalations.Dtos;
using Test.Dto;

namespace Test.RuleEscalations
{
    public interface IRuleEscalationsAppService : IApplicationService 
    {
        Task<PagedResultDto<GetRuleEscalationForView>> GetAll(GetAllRuleEscalationsInput input);

		Task<GetRuleEscalationForEditOutput> GetRuleEscalationForEdit(EntityDto<Guid> input);

		Task CreateOrEdit(CreateOrEditRuleEscalationDto input);

		Task Delete(EntityDto<Guid> input);

		Task<FileDto> GetRuleEscalationsToExcel(GetAllRuleEscalationsForExcelInput input);

		Task<PagedResultDto<UserLookupTableDto>> GetAllUserForLookupTable(GetAllForLookupTableInput input);
		
    }
}