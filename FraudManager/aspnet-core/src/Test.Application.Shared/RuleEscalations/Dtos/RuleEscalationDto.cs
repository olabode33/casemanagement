
using System;
using Abp.Application.Services.Dto;

namespace Test.RuleEscalations.Dtos
{
    public class RuleEscalationDto : EntityDto<Guid>
    {
		 public Guid RuleId { get; set; }

		 		 public long UserId { get; set; }
    }

    public class RuleEscalationForRuleEditDto : RuleEscalationDto
    {
        public string UserFullName { get; set; }
    }
}