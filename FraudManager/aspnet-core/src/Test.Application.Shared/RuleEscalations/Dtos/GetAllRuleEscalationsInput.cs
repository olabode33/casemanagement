using Abp.Application.Services.Dto;
using System;

namespace Test.RuleEscalations.Dtos
{
    public class GetAllRuleEscalationsInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }


		 public string RuleNameFilter { get; set; }

		 		 public string UserNameFilter { get; set; }

		 
    }
}