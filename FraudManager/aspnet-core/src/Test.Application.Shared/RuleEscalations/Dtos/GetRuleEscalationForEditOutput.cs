using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace Test.RuleEscalations.Dtos
{
    public class GetRuleEscalationForEditOutput
    {
		public CreateOrEditRuleEscalationDto RuleEscalation { get; set; }

		public string RuleName { get; set;}

		public string UserName { get; set;}


    }
}