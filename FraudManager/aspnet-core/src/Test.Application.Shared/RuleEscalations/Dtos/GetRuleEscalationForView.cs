namespace Test.RuleEscalations.Dtos
{
    public class GetRuleEscalationForView
    {
		public RuleEscalationDto RuleEscalation { get; set; }

		public string RuleName { get; set;}

		public string UserName { get; set;}


    }
}