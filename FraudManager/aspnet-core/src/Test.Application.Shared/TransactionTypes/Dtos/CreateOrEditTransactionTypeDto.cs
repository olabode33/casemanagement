
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace Test.TransactionTypes.Dtos
{
    public class CreateOrEditTransactionTypeDto : EntityDto<Guid?>
    {

		[Required]
		[StringLength(TransactionTypeConsts.MaxNameLength, MinimumLength = TransactionTypeConsts.MinNameLength)]
		public string Name { get; set; }
		
		
		public string Description { get; set; }
		
		
		 public Guid ChannelId { get; set; }
		 
		 
    }
}