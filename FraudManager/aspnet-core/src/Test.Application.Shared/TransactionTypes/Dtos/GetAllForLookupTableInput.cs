using Abp.Application.Services.Dto;

namespace Test.TransactionTypes.Dtos
{
    public class GetAllForLookupTableInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }
    }
}