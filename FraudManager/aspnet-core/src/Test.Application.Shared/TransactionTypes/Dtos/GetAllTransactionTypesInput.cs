using Abp.Application.Services.Dto;
using System;

namespace Test.TransactionTypes.Dtos
{
    public class GetAllTransactionTypesInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }

		public string NameFilter { get; set; }


		 public string ChannelNameFilter { get; set; }

		 
    }
}