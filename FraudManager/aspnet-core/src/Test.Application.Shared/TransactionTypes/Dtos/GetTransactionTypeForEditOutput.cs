using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace Test.TransactionTypes.Dtos
{
    public class GetTransactionTypeForEditOutput
    {
		public CreateOrEditTransactionTypeDto TransactionType { get; set; }

		public string ChannelName { get; set;}


    }
}