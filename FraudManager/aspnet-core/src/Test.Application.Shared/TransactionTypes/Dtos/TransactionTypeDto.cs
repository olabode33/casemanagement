
using System;
using Abp.Application.Services.Dto;

namespace Test.TransactionTypes.Dtos
{
    public class TransactionTypeDto : EntityDto<Guid>
    {
		public string Name { get; set; }


		 public Guid ChannelId { get; set; }

		 
    }
}