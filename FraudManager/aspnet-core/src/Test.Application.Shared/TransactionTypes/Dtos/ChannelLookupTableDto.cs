using Abp.Application.Services.Dto;

namespace Test.TransactionTypes.Dtos
{
    public class ChannelLookupTableDto
    {
		public string Id { get; set; }

		public string DisplayName { get; set; }
    }
}