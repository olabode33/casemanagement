using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Test.TransactionTypes.Dtos;
using Test.Dto;

namespace Test.TransactionTypes
{
    public interface ITransactionTypesAppService : IApplicationService 
    {
        Task<PagedResultDto<GetTransactionTypeForView>> GetAll(GetAllTransactionTypesInput input);

		Task<GetTransactionTypeForEditOutput> GetTransactionTypeForEdit(EntityDto<Guid> input);

		Task CreateOrEdit(CreateOrEditTransactionTypeDto input);

		Task Delete(EntityDto<Guid> input);

		Task<FileDto> GetTransactionTypesToExcel(GetAllTransactionTypesForExcelInput input);

		
		Task<PagedResultDto<ChannelLookupTableDto>> GetAllChannelForLookupTable(GetAllForLookupTableInput input);
		
    }
}