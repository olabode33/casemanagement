using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Test.Rules.Dtos;
using Test.Dto;

namespace Test.Rules
{
    public interface IRulesAppService : IApplicationService 
    {
        Task<PagedResultDto<GetRuleForView>> GetAll(GetAllRulesInput input);

		Task<GetRuleForEditOutput> GetRuleForEdit(EntityDto<Guid> input);

        Task MakeCopy(EntityDto<Guid> input);

        Task<string> CreateOrEdit(CreateOrEditRuleDto input);

        Task UpdateStatus(UpdateRuleStatusInputDto input);

        Task ToggleActiveStatus(EntityDto<Guid> input);

        Task Delete(EntityDto<Guid> input);

		Task<FileDto> GetRulesToExcel(GetAllRulesForExcelInput input);
    }
}