﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Test.Rules.Dtos
{
    public class UpdateRuleStatusInputDto: EntityDto<Guid>
    {
        public RuleStatus NewStatus { get; set; }
    }
}
