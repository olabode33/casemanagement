namespace Test.Rules.Dtos
{
    public class GetRuleForView
    {
        public RuleDto Rule { get; set; }

        public string TransactionTypeName { get; set; }
        public string ApprovedBy { get; set; }

    }
}