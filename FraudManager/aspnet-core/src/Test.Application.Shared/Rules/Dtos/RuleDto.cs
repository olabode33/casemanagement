using Test;
using Test;

using System;
using Abp.Application.Services.Dto;

namespace Test.Rules.Dtos
{
    public class RuleDto : EntityDto<Guid>
    {
        public DateTime CreationTime { get; set; }

        public string Name { get; set; }

		public Severity Severity { get; set; }

		public Actions Action { get; set; }


		 public Guid? TransactionTypeId { get; set; }

        public decimal? Score { get; set; }
        public DateTime? ApprovedDate { get; set; }
        public long? ApprovedBy { get; set; }
        public bool IsApproved { get; set; }
        public RuleStatus RuleStatus { get; set; }
        public bool Active { get; set; }
        public string BaseConnector { get; set; }
    }
}