using Test;
using Test;

using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using Test.RuleConfigurations.Dtos;
using Test.RuleEscalations.Dtos;
using System.Collections.Generic;

namespace Test.Rules.Dtos
{
    public class CreateOrEditRuleDto : EntityDto<Guid?>
    {

        [Required]
        [StringLength(RuleConsts.MaxNameLength, MinimumLength = RuleConsts.MinNameLength)]
        public string Name { get; set; }


        public string Description { get; set; }


        public Severity Severity { get; set; }


        public Actions Action { get; set; }


        public Guid? TransactionTypeId { get; set; }

        public CreateOrEditRuleConfigurationDto[] RuleConfigurations { get; set; }

        public CreateOrEditRuleEscalationDto[] RuleEscalations { get; set; }

        public decimal? Score { get; set; }
        public DateTime? ApprovedDate { get; set; }
        public long? ApprovedBy { get; set; }
        public bool IsApproved { get; set; }
        public RuleStatus RuleStatus { get; set; }
        public bool Active { get; set; }
        public string BaseConnector { get; set; }
        public List<RuleObjGroup> RuleGroups { get; set; }
    }
}