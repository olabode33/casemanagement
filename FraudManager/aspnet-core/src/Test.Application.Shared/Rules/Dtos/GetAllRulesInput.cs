using Abp.Application.Services.Dto;
using System;

namespace Test.Rules.Dtos
{
    public class GetAllRulesInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public string NameFilter { get; set; }

        public int SeverityFilter { get; set; }

        public int ActionFilter { get; set; }

        public string TransactionTypeNameFilter { get; set; }

        public int RuleStatusFilter { get; set; }
    }
}