using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using Test.RuleEscalations.Dtos;
using Test.RuleConfigurations.Dtos;

namespace Test.Rules.Dtos
{
    public class GetRuleForEditOutput
    {
        public CreateOrEditRuleDto Rule { get; set; }

        public string TransactionTypeName { get; set; }

        public RuleEscalationForRuleEditDto[] RuleEscalations { get; set; }

        public RuleConfigurationForRuleEditDto[] RuleConfigurations { get; set; }

        public string ApprovedBy { get; set; }
    }
}