using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Test.Cases.Dtos;
using Test.Dto;
using Test.RuleEscalations.Dtos;

namespace Test.Cases
{
    public interface ICaseTasksAppService : IApplicationService 
    {
        Task<PagedResultDto<GetCaseTaskForViewDto>> GetAll(GetAllCaseTasksInput input);

        Task<GetCaseTaskForViewDto> GetCaseTaskForView(int id);

		Task<GetCaseTaskForEditOutput> GetCaseTaskForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditCaseTaskDto input);

		Task Delete(EntityDto input);

        Task CompleteTask(CreateOrEditCaseTaskDto input);

        Task<PagedResultDto<UserLookupTableDto>> GetAllUserForLookupTable(GetAllForLookupTableInput input);
		
    }
}