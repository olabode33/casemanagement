using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Test.Cases.Dtos;
using Test.Dto;
using Test.RuleConfigurations.Dtos;

namespace Test.Cases
{
    public interface ICaseFailedRulesAppService : IApplicationService 
    {
        Task<PagedResultDto<GetCaseFailedRuleForViewDto>> GetAll(GetAllCaseFailedRulesInput input);

        Task<GetCaseFailedRuleForViewDto> GetCaseFailedRuleForView(int id);

		Task<GetCaseFailedRuleForEditOutput> GetCaseFailedRuleForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditCaseFailedRuleDto input);

		Task Delete(EntityDto input);

		
		Task<PagedResultDto<AlertLookupTableDto>> GetAllAlertForLookupTable(GetAllForLookupTableInput input);
		
		Task<PagedResultDto<RuleLookupTableDto>> GetAllRuleForLookupTable(GetAllForLookupTableInput input);
		
    }
}