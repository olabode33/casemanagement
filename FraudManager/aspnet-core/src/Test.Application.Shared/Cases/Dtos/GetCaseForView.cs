namespace Test.Cases.Dtos
{
    public class GetCaseForView
    {
		public CaseDto Case { get; set; }

		public string TransactionTypeName { get; set;}

		public string UserName { get; set;}

		public string RuleName { get; set;}


    }
}