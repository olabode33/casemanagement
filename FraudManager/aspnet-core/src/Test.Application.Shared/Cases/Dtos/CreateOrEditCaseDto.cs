using Test;
using Test;
using Test;
using Test;
using Test;

using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace Test.Cases.Dtos
{
    public class CreateOrEditCaseDto : EntityDto<string>
    {

		public DateTime TransactionDate { get; set; }
		
		
		[Required]
		public string AccountNumber { get; set; }
		
		
		public string AccountName { get; set; }
		
		
		public string AccountType { get; set; }
		
		
		public string ProductName { get; set; }
		
		
		public string Description { get; set; }
		
		
		public DectectionSource DetectionSource { get; set; }
		
		
		public Sources Source { get; set; }
		
		
		public TransactionStatus TransactionStatus { get; set; }
		
		
		public Conclusion Conclusion { get; set; }
		
		
		public Status Status { get; set; }
		
		
		public string ClosureComments { get; set; }

        public DateTime CreationTime { set; get; }

        public decimal Amount { get; set; }
        public string  PhoneNumber { get; set; }
        public string Anomally { get; set; }

        public Guid TransactionTypeId { get; set; }
		 
		public long? AllocationUserId { get; set; }
		 
		public Guid? RuleId { get; set; }

    }
}