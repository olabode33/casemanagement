namespace Test.Cases.Dtos
{
    public class GetCaseFailedRuleForViewDto
    {
		public CaseFailedRuleDto CaseFailedRule { get; set; }

		public string AlertAccountNumber { get; set;}

		public string RuleName { get; set;}


    }
}