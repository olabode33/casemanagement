using Abp.Application.Services.Dto;

namespace Test.Cases.Dtos
{
    public class AlertLookupTableDto
    {
		public string Id { get; set; }

		public string DisplayName { get; set; }
    }
}