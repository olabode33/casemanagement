﻿using Abp.Events.Bus;
using System;
using System.Collections.Generic;
using System.Text;

namespace Test.Cases.Dtos
{
    public class RecieveCaseEventData: EventData
    {
        public string Payload;
    }
}
