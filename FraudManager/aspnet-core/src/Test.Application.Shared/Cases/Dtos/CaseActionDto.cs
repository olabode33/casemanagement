
using System;
using Abp.Application.Services.Dto;

namespace Test.Cases.Dtos
{
    public class CaseActionDto : EntityDto
    {
		public string Title { get; set; }

		public string Comment { get; set; }


		 public string AlertId { get; set; }

		 
    }
}