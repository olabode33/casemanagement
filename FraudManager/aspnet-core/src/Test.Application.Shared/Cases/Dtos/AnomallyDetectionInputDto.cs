﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test.Cases.Dtos
{
    public class AnomallyDetectionInputDto
    {
        public string cc_num { get; set; }
        public string unix_time { get; set; }
        public string amt { get; set; }
        public string merch_lat { get; set; }
        public string merch_long { get; set; }
    }
}
