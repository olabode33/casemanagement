using Abp.Application.Services.Dto;
using System;

namespace Test.Cases.Dtos
{
    public class GetAllCaseActionsInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }

		public string TitleFilter { get; set; }

		public string CommentFilter { get; set; }


		 public string AlertAccountNumberFilter { get; set; }

		 
    }
}