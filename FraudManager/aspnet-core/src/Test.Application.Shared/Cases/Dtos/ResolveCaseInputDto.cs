﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test.Cases.Dtos
{
    public class ResolveCaseInputDto
    {
        public string CaseId { get; set; }
        public Conclusion Conclusion { get; set; }
        public string ClosureComment { get; set; }
    }
}
