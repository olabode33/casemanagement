﻿using Abp.Events.Bus;
using System;
using System.Collections.Generic;
using System.Text;

namespace Test.Cases.Dtos
{
    public class AssignCaseToUserDto: EventData
    {
        public string CaseId { get; set; }
    }
}
