using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace Test.Cases.Dtos
{
    public class GetCaseForEditOutput
    {
		public CreateOrEditCaseDto Case { get; set; }

		public string TransactionTypeName { get; set;}

		public string UserName { get; set;}

		public string RuleName { get; set;}

        public List<GetCaseFailedRuleForViewDto> FailedRules { get; set; }
        public List<GetCaseTaskForViewDto> Tasks { get; set; }
        public List<GetCaseActionForViewDto> Actions { get; set; }
    }
}