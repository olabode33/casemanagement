namespace Test.Cases.Dtos
{
    public class GetCaseTaskForViewDto
    {
		public CaseTaskDto CaseTask { get; set; }

		public string AssignerUsername { get; set;}

		public string AssigneeUsername { get; set;}


    }
}