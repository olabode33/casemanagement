using Test;
using Test;
using Test;

using System;
using Abp.Application.Services.Dto;

namespace Test.Cases.Dtos
{
    public class CaseTaskDto : EntityDto
    {
        public string Subject { get; set; }

        public string Description { get; set; }

        public Severity Priority { get; set; }

        public DateTime ExpectedCompletionDate { get; set; }

        public DateTime? ActualCompletionDate { get; set; }

        public string Comment { get; set; }

        public Status Status { get; set; }

        public TaskType Type { get; set; }


        public long? AssignerUserId { get; set; }

        public long? AssigneeUserId { get; set; }
        public string CaseId { get; set; }

    }
}