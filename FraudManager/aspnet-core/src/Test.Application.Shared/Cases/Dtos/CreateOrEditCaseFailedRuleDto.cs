
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace Test.Cases.Dtos
{
    public class CreateOrEditCaseFailedRuleDto : EntityDto<int?>
    {

		 public string AlertId { get; set; }
		 
		 		 public Guid? RuleId { get; set; }
		 
		 
    }
}