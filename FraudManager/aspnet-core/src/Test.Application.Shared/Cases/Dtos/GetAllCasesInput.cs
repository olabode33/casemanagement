using Abp.Application.Services.Dto;
using System;

namespace Test.Cases.Dtos
{
    public class GetAllCasesInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }

		public DateTime? MaxTransactionDateFilter { get; set; }
		public DateTime? MinTransactionDateFilter { get; set; }

		public string AccountNumberFilter { get; set; }

		public string AccountTypeFilter { get; set; }

		public int DetectionSourceFilter { get; set; }

		public int SourceFilter { get; set; }

		public int TransactionStatusFilter { get; set; }

		public int ConclusionFilter { get; set; }

		public int StatusFilter { get; set; }


		 public string TransactionTypeNameFilter { get; set; }

		 		 public string UserNameFilter { get; set; }

		 		 public string RuleNameFilter { get; set; }

		 
    }
}