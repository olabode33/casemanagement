using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace Test.Cases.Dtos
{
    public class GetCaseFailedRuleForEditOutput
    {
		public CreateOrEditCaseFailedRuleDto CaseFailedRule { get; set; }

		public string AlertAccountNumber { get; set;}

		public string RuleName { get; set;}


    }
}