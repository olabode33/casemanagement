﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test.Cases.Dtos
{
    [Serializable]
    public class HandleCaseArgs
    {
        public string Payload { get; set; }
    }
}
