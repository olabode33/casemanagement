using Abp.Application.Services.Dto;
using System;

namespace Test.Cases.Dtos
{
    public class GetAllCaseTasksInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }

		public string SubjectFilter { get; set; }

		public string DescriptionFilter { get; set; }

		public int PriorityFilter { get; set; }

		public DateTime? MaxExpectedCompletionDateFilter { get; set; }
		public DateTime? MinExpectedCompletionDateFilter { get; set; }

		public DateTime? MaxActualCompletionDateFilter { get; set; }
		public DateTime? MinActualCompletionDateFilter { get; set; }

		public string CommentFilter { get; set; }

		public int StatusFilter { get; set; }

		public int TypeFilter { get; set; }


		 public string AssignerFilter { get; set; }

		 		 public string AssigneeFilter { get; set; }

		 
    }
}