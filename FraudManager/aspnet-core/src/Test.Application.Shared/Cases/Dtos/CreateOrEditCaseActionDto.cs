
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace Test.Cases.Dtos
{
    public class CreateOrEditCaseActionDto : EntityDto<int?>
    {

		[Required]
		[StringLength(CaseActionConsts.MaxTitleLength, MinimumLength = CaseActionConsts.MinTitleLength)]
		public string Title { get; set; }
		
		
		public string Comment { get; set; }
		
		
		 public string AlertId { get; set; }
		 
		 
    }
}