using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace Test.Cases.Dtos
{
    public class GetCaseActionForEditOutput
    {
		public CreateOrEditCaseActionDto CaseAction { get; set; }

		public string AlertAccountNumber { get; set;}


    }
}