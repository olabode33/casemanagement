using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Test.Cases.Dtos;
using Test.Common.Dtos;
using Test.Dto;

namespace Test.Cases
{
    public interface ICaseActionsAppService : IApplicationService 
    {
        Task<PagedResultDto<GetCaseActionForViewDto>> GetAll(GetAllCaseActionsInput input);

        Task<GetCaseActionForViewDto> GetCaseActionForView(int id);

		Task<GetCaseActionForEditOutput> GetCaseActionForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditCaseActionDto input);

		Task Delete(EntityDto input);

		
		Task<PagedResultDto<AlertLookupTableDto>> GetAllAlertForLookupTable(GetAllForLookupTableInput input);
		
    }
}