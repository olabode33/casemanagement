using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Test.Cases.Dtos;
using Test.Dto;

namespace Test.Cases
{
    public interface ICasesAppService : IApplicationService 
    {
        Task<PagedResultDto<GetCaseForView>> GetAll(GetAllCasesInput input);

		Task<GetCaseForEditOutput> GetCaseForEdit(EntityDto<string> input);

		Task CreateOrEdit(CreateOrEditCaseDto input);

		Task Delete(EntityDto<string> input);

		Task<FileDto> GetCasesToExcel(GetAllCasesForExcelInput input);

        Task ResolveCase(ResolveCaseInputDto input);

        Task<string> GetCaseVerificationCode(EntityDto<string> input);
    }
}