using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace Test.PayloadProperties.Dtos
{
    public class GetPayloadPropertyForEditOutput
    {
		public CreateOrEditPayloadPropertyDto PayloadProperty { get; set; }

		public string PayloadName { get; set;}


    }
}