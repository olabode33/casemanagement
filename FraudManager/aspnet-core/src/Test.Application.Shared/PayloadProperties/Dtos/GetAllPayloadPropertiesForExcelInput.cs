using Abp.Application.Services.Dto;
using System;

namespace Test.PayloadProperties.Dtos
{
    public class GetAllPayloadPropertiesForExcelInput
    {
		public string Filter { get; set; }

		public string NameFilter { get; set; }

		public int DataTypeFilter { get; set; }


		 public string PayloadNameFilter { get; set; }

		 
    }
}