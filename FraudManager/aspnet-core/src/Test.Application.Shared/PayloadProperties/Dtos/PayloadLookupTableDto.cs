using Abp.Application.Services.Dto;

namespace Test.PayloadProperties.Dtos
{
    public class PayloadLookupTableDto
    {
		public string Id { get; set; }

		public string DisplayName { get; set; }
        public string Description { get; set; }
        public string DataType { get; set; }
    }
}