using Test;

using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace Test.PayloadProperties.Dtos
{
    public class CreateOrEditPayloadPropertyDto : EntityDto<Guid?>
    {

		[StringLength(PayloadPropertyConsts.MaxNameLength, MinimumLength = PayloadPropertyConsts.MinNameLength)]
		public string Name { get; set; }
		
		
		public DataTypes DataType { get; set; }
		
		
		public string Description { get; set; }
		
		
		 public Guid PayloadId { get; set; }
        public string DataTypeName { get; set; }
    }
}