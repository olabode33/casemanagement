using Abp.Application.Services.Dto;
using System;

namespace Test.PayloadProperties.Dtos
{
    public class GetAllForLookupTableInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }

        public Guid? TransactionType { get; set; }
    }
}