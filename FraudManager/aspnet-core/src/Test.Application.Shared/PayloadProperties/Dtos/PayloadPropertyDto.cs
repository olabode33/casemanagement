using Test;

using System;
using Abp.Application.Services.Dto;

namespace Test.PayloadProperties.Dtos
{
    public class PayloadPropertyDto : EntityDto<Guid>
    {
		public string Name { get; set; }

		public DataTypes DataType { get; set; }


		 public Guid PayloadId { get; set; }

		 
    }
}