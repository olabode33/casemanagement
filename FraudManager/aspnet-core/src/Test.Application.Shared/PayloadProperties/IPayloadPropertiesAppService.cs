using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Test.PayloadProperties.Dtos;
using Test.Dto;

namespace Test.PayloadProperties
{
    public interface IPayloadPropertiesAppService : IApplicationService 
    {
        Task<PagedResultDto<GetPayloadPropertyForView>> GetAll(GetAllPayloadPropertiesInput input);

		Task<GetPayloadPropertyForEditOutput> GetPayloadPropertyForEdit(EntityDto<Guid> input);

		Task CreateOrEdit(CreateOrEditPayloadPropertyDto input);

		Task Delete(EntityDto<Guid> input);

		Task<FileDto> GetPayloadPropertiesToExcel(GetAllPayloadPropertiesForExcelInput input);

		
		Task<PagedResultDto<PayloadLookupTableDto>> GetAllPayloadForLookupTable(GetAllForLookupTableInput input);

        Task<PagedResultDto<PayloadLookupTableDto>> GetAllDataPointForLookupTable(GetAllForLookupTableInput input);



    }
}