﻿using System.Collections.Generic;

namespace Test.Logging.Dto
{
    public class GetLatestWebLogsOutput
    {
        public List<string> LatestWebLogLines { get; set; }
    }
}
