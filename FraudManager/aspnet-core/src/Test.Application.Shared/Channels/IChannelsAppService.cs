using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Test.Channels.Dtos;
using Test.Dto;

namespace Test.Channels
{
    public interface IChannelsAppService : IApplicationService 
    {
        Task<PagedResultDto<GetChannelForView>> GetAll(GetAllChannelsInput input);

		Task<GetChannelForEditOutput> GetChannelForEdit(EntityDto<Guid> input);

		Task CreateOrEdit(CreateOrEditChannelDto input);

		Task Delete(EntityDto<Guid> input);

		
    }
}