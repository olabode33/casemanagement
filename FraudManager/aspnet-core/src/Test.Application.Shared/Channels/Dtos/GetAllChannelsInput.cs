using Abp.Application.Services.Dto;
using System;

namespace Test.Channels.Dtos
{
    public class GetAllChannelsInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }

		public string NameFilter { get; set; }



    }
}