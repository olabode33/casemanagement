using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace Test.Channels.Dtos
{
    public class GetChannelForEditOutput
    {
		public CreateOrEditChannelDto Channel { get; set; }


    }
}