
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace Test.Channels.Dtos
{
    public class CreateOrEditChannelDto : EntityDto<Guid?>
    {

		[Required]
		[StringLength(ChannelConsts.MaxNameLength, MinimumLength = ChannelConsts.MinNameLength)]
		public string Name { get; set; }
		
		
		public string Description { get; set; }
		
		

    }
}