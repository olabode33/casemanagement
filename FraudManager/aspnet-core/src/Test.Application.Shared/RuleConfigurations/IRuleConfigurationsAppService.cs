using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Test.RuleConfigurations.Dtos;
using Test.Dto;

namespace Test.RuleConfigurations
{
    public interface IRuleConfigurationsAppService : IApplicationService 
    {
        Task<PagedResultDto<GetRuleConfigurationForView>> GetAll(GetAllRuleConfigurationsInput input);

		Task<GetRuleConfigurationForEditOutput> GetRuleConfigurationForEdit(EntityDto<Guid> input);

		Task CreateOrEdit(CreateOrEditRuleConfigurationDto input);

		Task Delete(EntityDto<Guid> input);

		Task<FileDto> GetRuleConfigurationsToExcel(GetAllRuleConfigurationsForExcelInput input);

		
		Task<PagedResultDto<RuleLookupTableDto>> GetAllRuleForLookupTable(GetAllForLookupTableInput input);
		
		Task<PagedResultDto<DataPointLookupTableDto>> GetAllDataPointForLookupTable(GetAllForLookupTableInput input);
		
    }
}