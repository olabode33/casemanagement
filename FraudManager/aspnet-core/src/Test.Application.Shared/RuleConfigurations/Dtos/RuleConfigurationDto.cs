
using System;
using System.Collections.Generic;
using Abp.Application.Services.Dto;

namespace Test.RuleConfigurations.Dtos
{
    public class RuleConfigurationDto 
    {
        public Guid? Id { get; set; }

        public string Operand { get; set; }

		public string Connector { get; set; }

		public string Value { get; set; }


		 public Guid RuleId { get; set; }

		public Guid DataPointId { get; set; }

        public string GroupName { get; set; }
    }


    public class RuleConfigurationForRuleEditDto : RuleConfigurationDto
    {
        public string DataPointName { get; set; }

        public string DataPointType { get; set; }
    }

    public class RuleObjGroup
    {
        public string GroupName { get; set; }
        public string Connector { get; set; }
        public List<RuleConfigurationForRuleEditDto> RuleObjs { get; set; }
    }

}