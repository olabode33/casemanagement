using Abp.Application.Services.Dto;

namespace Test.RuleConfigurations.Dtos
{
    public class DataPointLookupTableDto
    {
		public string Id { get; set; }

		public string DisplayName { get; set; }

        public int DataType { get; set; }
    }
}