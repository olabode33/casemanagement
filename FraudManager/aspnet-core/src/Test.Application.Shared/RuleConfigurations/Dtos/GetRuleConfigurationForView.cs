namespace Test.RuleConfigurations.Dtos
{
    public class GetRuleConfigurationForView
    {
		public RuleConfigurationDto RuleConfiguration { get; set; }

		public string RuleName { get; set;}

		public string DataPointName { get; set;}


    }
}