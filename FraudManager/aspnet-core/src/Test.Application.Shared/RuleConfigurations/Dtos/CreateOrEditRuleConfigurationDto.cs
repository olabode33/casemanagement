
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace Test.RuleConfigurations.Dtos
{
    public class CreateOrEditRuleConfigurationDto : EntityDto<Guid?>
    {

        [Required]
        public string Operand { get; set; }


        public string Connector { get; set; }


        [Required]
        public string Value { get; set; }



        public Guid DataPointId { get; set; }

        public string GroupName { get; set; }

    }
}