using Abp.Application.Services.Dto;
using System;

namespace Test.RuleConfigurations.Dtos
{
    public class GetAllForLookupTableInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }

        public Guid? TransactionTypeId { set; get; }
    }
}