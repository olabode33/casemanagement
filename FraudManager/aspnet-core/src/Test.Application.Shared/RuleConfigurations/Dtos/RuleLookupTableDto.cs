using Abp.Application.Services.Dto;

namespace Test.RuleConfigurations.Dtos
{
    public class RuleLookupTableDto
    {
		public string Id { get; set; }

		public string DisplayName { get; set; }
    }
}