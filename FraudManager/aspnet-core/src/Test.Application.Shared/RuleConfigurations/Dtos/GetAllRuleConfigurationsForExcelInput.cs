using Abp.Application.Services.Dto;
using System;

namespace Test.RuleConfigurations.Dtos
{
    public class GetAllRuleConfigurationsForExcelInput
    {
		public string Filter { get; set; }

		public string OperandFilter { get; set; }


		 public string RuleNameFilter { get; set; }

		 		 public string DataPointNameFilter { get; set; }

		 
    }
}