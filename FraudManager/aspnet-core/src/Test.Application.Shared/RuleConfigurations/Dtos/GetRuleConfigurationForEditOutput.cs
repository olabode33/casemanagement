using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace Test.RuleConfigurations.Dtos
{
    public class GetRuleConfigurationForEditOutput
    {
		public CreateOrEditRuleConfigurationDto RuleConfiguration { get; set; }

		public string RuleName { get; set;}

		public string DataPointName { get; set;}


    }
}