using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Test.Blacklists.Dtos;
using Test.Dto;
using Test.Payloads.Dtos;

namespace Test.Blacklists
{
    public interface IBlacklistsAppService : IApplicationService 
    {
        Task<PagedResultDto<GetBlacklistForViewDto>> GetAll(GetAllBlacklistsInput input);

        Task<GetBlacklistForViewDto> GetBlacklistForView(Guid id);

		Task<GetBlacklistForEditOutput> GetBlacklistForEdit(EntityDto<Guid> input);

		Task CreateOrEdit(CreateOrEditBlacklistDto input);

		Task Delete(EntityDto<Guid> input);

		Task<FileDto> GetBlacklistsToExcel(GetAllBlacklistsForExcelInput input);

		Task<PagedResultDto<TransactionTypeLookupTableDto>> GetAllTransactionTypeForLookupTable(GetAllForLookupTableInput input);

		
    }
}