namespace Test.Blacklists.Dtos
{
    public class GetBlacklistForViewDto
    {
		public BlacklistDto Blacklist { get; set; }

		public string TransactionTypeName { get; set;}

		public string UserName { get; set;}


    }
}