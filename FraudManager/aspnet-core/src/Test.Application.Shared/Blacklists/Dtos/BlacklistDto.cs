
using System;
using Abp.Application.Services.Dto;

namespace Test.Blacklists.Dtos
{
    public class BlacklistDto : EntityDto<Guid>
    {
		public string AccountNumber { get; set; }

		public string AccountName { get; set; }

		public string Description { get; set; }

		public DateTime? ApprovalDate { get; set; }


		 public Guid TransactionTypeId { get; set; }

		 		 public long? ApproverUserId { get; set; }

		 
    }
}