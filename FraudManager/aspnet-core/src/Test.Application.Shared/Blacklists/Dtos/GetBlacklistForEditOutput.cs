using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace Test.Blacklists.Dtos
{
    public class GetBlacklistForEditOutput
    {
		public CreateOrEditBlacklistDto Blacklist { get; set; }

		public string TransactionTypeName { get; set;}

		public string UserName { get; set;}


    }
}