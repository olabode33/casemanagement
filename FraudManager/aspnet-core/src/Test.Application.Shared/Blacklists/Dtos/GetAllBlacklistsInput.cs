using Abp.Application.Services.Dto;
using System;

namespace Test.Blacklists.Dtos
{
    public class GetAllBlacklistsInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }

		public string AccountNumberFilter { get; set; }


		 public string TransactionTypeNameFilter { get; set; }

		 		 public string UserNameFilter { get; set; }

		 
    }
}