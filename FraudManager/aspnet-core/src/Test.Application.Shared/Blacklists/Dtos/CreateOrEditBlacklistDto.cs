
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace Test.Blacklists.Dtos
{
    public class CreateOrEditBlacklistDto : EntityDto<Guid?>
    {

		[Required]
		[StringLength(BlacklistConsts.MaxAccountNumberLength, MinimumLength = BlacklistConsts.MinAccountNumberLength)]
		public string AccountNumber { get; set; }
		
		
		public string AccountName { get; set; }
		
		
		public string Description { get; set; }
		
		
		public DateTime? ApprovalDate { get; set; }
		
		
		 public Guid TransactionTypeId { get; set; }
		 
		 		 public long? ApproverUserId { get; set; }
		 
		 
    }
}