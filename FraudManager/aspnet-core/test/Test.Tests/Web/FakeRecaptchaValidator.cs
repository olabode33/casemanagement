﻿using System.Threading.Tasks;
using Test.Security.Recaptcha;

namespace Test.Tests.Web
{
    public class FakeRecaptchaValidator : IRecaptchaValidator
    {
        public Task ValidateAsync(string captchaResponse)
        {
            return Task.CompletedTask;
        }
    }
}
