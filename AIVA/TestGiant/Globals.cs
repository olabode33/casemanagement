﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;

namespace TestGiant
{
    public static class Globals
    {
        public static string RESOLVE_CASE_URL = "http://localhost:22742/api/services/app/Cases/ResolveCase";
        public static string GET_CASE_VERIFICATION_CODE_URL = "http://localhost:22742/api/services/app/Cases/GetCaseVerificationCode";

        public static readonly HttpClient CLIENT = new HttpClient();

        public static string GetNgRokUrl()
        {
            string ngRokUrl = ConfigurationManager.ConnectionStrings["ngRok_tunnel_url"].ConnectionString;

            return ngRokUrl;
        }

        public enum Conclusion
        {
            Pending, Fraud, FalsePositive, AttemptedFraud
        }
    }

    public static class WebHelper
    {
        public static string Post(Uri url, string value)
        {
            var request = HttpWebRequest.Create(url);
            var byteData = Encoding.ASCII.GetBytes(value);
            request.ContentType = "application/json";
            request.Method = "POST";

            try
            {
                using (var stream = request.GetRequestStream())
                {
                    stream.Write(byteData, 0, byteData.Length);
                }
                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                return responseString;
            }
            catch (WebException e)
            {
                return null;
            }
        }

        public static string Get(Uri url)
        {
            var request = HttpWebRequest.Create(url);
            request.ContentType = "application/json";
            request.Method = "GET";

            try
            {
                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                return responseString;
            }
            catch (WebException e)
            {
                return null;
            }
        }
    }
}