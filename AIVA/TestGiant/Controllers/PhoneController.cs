﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

using Twilio.TwiML;
using Twilio.TwiML.Voice;
using Twilio.AspNet.Mvc;

namespace TestGiant.Controllers
{
    public class PhoneController : Controller
    {

        private readonly string NgRok_URL = Globals.GetNgRokUrl();

        // GET: Phone
        public ActionResult Index()
        {
            var accountsid = "ACf96f55b7044bc5ac6c9a83833e3be5d1";
            var authToken = "a5357ca20b14f82c91dd6ee79cd8dbe7";
            TwilioClient.Init(accountsid, authToken);

            var from = new PhoneNumber("+13252214024");
            //var to = new PhoneNumber("+2348056755861");
            var to = new PhoneNumber("+2348094585637");
            var call = CallResource.Create(
                to: to,
                from: from,
                url: new Uri( NgRok_URL + "voice/work/f15a1608-8fcb-4ad9-8aeb-336acc8dcbd1")
            );
            return Content(call.Sid);
        }

        [HttpPost]
        public FileResult VoiceXML()
        {
            byte[] fileBytes = System.IO.File.ReadAllBytes(@"C:\Phone\voice.xml");
            string fileName = "voice.xml";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Text.Xml, fileName);

        }

        
        public FileResult VoiceeXML()
        {
            byte[] fileBytes = System.IO.File.ReadAllBytes(@"");
            string fileName = "voice.xml";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Text.Xml, fileName);

        }

        public ActionResult SendSMS()
        {
            var accountsid = "ACf96f55b7044bc5ac6c9a83833e3be5d1";
            var authToken = "a5357ca20b14f82c91dd6ee79cd8dbe7";
            TwilioClient.Init(accountsid, authToken);

            var from = new PhoneNumber("+13252214024");
            var to = new PhoneNumber("+2348068754606");
            var message = MessageResource.Create(
                body: "This is a test message... RUNNNN",
                from: from,
                to: to
              );
            return Content(message.Sid);
        }
    }
}