﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Twilio;
using Twilio.AspNet.Mvc;
using Twilio.TwiML;
using Twilio.TwiML.Voice;

namespace TestGiant.Controllers
{
    public class VoiceController : TwilioController
    {
        private string VerificationCode = "0000";
        private string NgRok_URL = Globals.GetNgRokUrl();

        [HttpPost]
        public TwiMLResult Index(string id, string code, string digits)
        {
            var response = new VoiceResponse();
            using (System.IO.StreamWriter file =
            new System.IO.StreamWriter(@"C:\dotnet_core_apps\TwilioCall_MVC_Log.txt", true))
            {
                file.WriteLine("Index:");
                file.WriteLine("Verification Code: " + code);
                file.WriteLine("Digits from User: " + digits);
            }

            if (!string.IsNullOrEmpty(digits))
            {
                if (digits.Equals(code.ToString()))
                {
                    response.Play(new Uri("https://heliotrope-cow-7318.twil.io/assets/successful_vetting.mp3"));
                    ResolveCase(id, Globals.Conclusion.FalsePositive.ToString());
                }
                else
                {

                    switch (digits)
                    {
                        case "1994":
                        case "1993":
                        case "1992":
                        case "1991":
                            response.Play(new Uri("https://heliotrope-cow-7318.twil.io/assets/successful_vetting.mp3"));
                            ResolveCase(id, Globals.Conclusion.FalsePositive.ToString());
                            break;

                        default:
                            response.Play(new Uri("https://heliotrope-cow-7318.twil.io/assets/rejected_vetting.mp3"));
                            ResolveCase(id, Globals.Conclusion.Fraud.ToString());
                            break;
                    }
                }
            }
            else
            {
                // If no input was sent, use the <Gather> verb to collect user input
                response.Append(
                new Gather(numDigits: 4, action: new Uri(NgRok_URL + "voice/index/" + id.ToString())).Play(new Uri("https://heliotrope-cow-7318.twil.io/assets/yob_question.mp3"), loop: 1));

                // If the user doesn't enter input, loop
                //response.Redirect("/voice");
            }


            //response.Say("Why are you not working", voice: "alice");

            return TwiML(response);
        }

        [HttpPost]
        public TwiMLResult Initiated(string id, string digits)
        {
            //Get verification code
            VerificationCode = GetCaseVerificationCode(id);
            //char[] codeArray = VerificationCode.ToCharArray();
            //string[] code_split = VerificationCode.Split('-');

            var response = new VoiceResponse();
            using (System.IO.StreamWriter file =
            new System.IO.StreamWriter(@"C:\dotnet_core_apps\TwilioCall_MVC_Log.txt", true))
            {
                file.WriteLine("Initiated:");
                file.WriteLine("Verification Code: " + VerificationCode);
                file.WriteLine("Digits from User: " + digits);
            }

            if (!string.IsNullOrEmpty(digits))
            {

                switch (digits)
                {
                    case "1":
                        //response
                        //    .Append(new Gather(numDigits: 4, action: new Uri(NgRok_URL + "voice/index/" + id.ToString()))
                        //    .Play(new Uri("https://heliotrope-cow-7318.twil.io/assets/yob_question.mp3")));

                        //response.Append(new Gather(numDigits: 4, action: new Uri(NgRok_URL + "voice/index/" + id.ToString() + "?code=" + VerificationCode), timeout: 10))
                        //    .Play(new Uri("https://heliotrope-cow-7318.twil.io/assets/yob_question.mp3")).Pause(length: 10);

                        response.Play(new Uri("https://heliotrope-cow-7318.twil.io/assets/otp_question.mp3"));
                        //response.Pause(length: 5);
                        response.Append(new Gather(numDigits: 4, action: new Uri(NgRok_URL + "voice/index/" + id.ToString() + "?code=" + VerificationCode), timeout: 10));

                        //for (int i=0; i<3; i++)
                        //{
                        //    response.Play(GetAlphabetAudio(codeArray[i]));
                        //}
                        //response.Append(new Gather(numDigits: 4, action: new Uri(NgRok_URL + "voice/index/" + id.ToString() + "?code=" + code_split[1])));
                        break;

                    case "0":
                        response.Redirect(new Uri(NgRok_URL + "voice/Instruct/" + id.ToString()));
                        break;

                    default:
                        //response.Play(new Uri(NgRok_URL + "cowbell.mp3"));
                        response.Play(new Uri("https://heliotrope-cow-7318.twil.io/assets/rejected_vetting.mp3"), loop: 1);
                        ResolveCase(id, Globals.Conclusion.AttemptedFraud.ToString());
                        break;
                }

            }
            else
            {
                // If no input was sent, use the <Gather> verb to collect user input
                response.Append(
                new Gather(numDigits: 1, action: new Uri(NgRok_URL + "voice/index/" + id.ToString())).Play(new Uri("https://heliotrope-cow-7318.twil.io/assets/instructions.mp3")));

                // If the user doesn't enter input, loop
                //response.Redirect("/voice");
            }


            //response.Say("Why are you not working", voice: "alice");

            return TwiML(response);
        }

        [HttpPost]
        public ActionResult Work(string id)
        {
            VerificationCode = id.ToString();

            using (System.IO.StreamWriter file =
            new System.IO.StreamWriter(@"C:\dotnet_core_apps\TwilioCall_MVC_Log.txt", true))
            {
                file.WriteLine("Work Id: " + id.ToString());
            }

            var response = new VoiceResponse();
            //response.Play(new Uri("https://api.twilio.com/cowbell.mp3"));
            //response.Play(new Uri("https://heliotrope-cow-7318.twil.io/assets/instructions.mp3"));
            response.Play(new Uri("https://heliotrope-cow-7318.twil.io/assets/welcome_message_union.mp3"));

            response.Redirect(new Uri(NgRok_URL + "voice/Instruct/" + id.ToString()));

            //response.Append(
            //    new Gather(numDigits: 4, action: new Uri(NgRok_URL + "voice/Instruct/" + id.ToString())).Play(new Uri(NgRok_URL + "content/voices/welcome_message.mp3")));
            // .Say("Welcome to my bank. You have a pending transaction, please press 1 if you initiated this transaction followed by the # key. If not, press 2. followed by the # key"));


            return TwiML(response);
        }

        [HttpPost]
        public ActionResult Instruct(string id)
        {
            var response = new VoiceResponse();
            response.Append(
                new Gather(numDigits: 4, action: new Uri(NgRok_URL + "voice/initiated/" + id.ToString())).Play(new Uri("https://heliotrope-cow-7318.twil.io/assets/instructions.mp3")));
            return TwiML(response);
        }

        private void ResolveCase(string id, string conclusion)
        {
            var values = new Dictionary<string, string>
                            {
                                { "CaseId",  id},
                                { "Conclusion", conclusion}
                            };

            string data = JsonConvert.SerializeObject(values);

            Uri uri = new Uri(string.Format(Globals.RESOLVE_CASE_URL));

            WebHelper.Post(uri, data);
        }

        private string GetCaseVerificationCode(string id)
        {
            Uri uri = new Uri(string.Format(Globals.GET_CASE_VERIFICATION_CODE_URL + "?Id=" + id));

            string code = WebHelper.Get(uri);

            JObject response = JObject.Parse(code);
            return (string)response["result"];
        }

        private Uri GetAlphabetAudio(char alphabet)
        {
            switch (alphabet)
            {
                case 'A':
                    return new Uri("https://heliotrope-cow-7318.twil.io/assets/a.mp3");
                case 'B':
                    return new Uri("https://heliotrope-cow-7318.twil.io/assets/b.mp3");
                case 'C':
                    return new Uri("https://heliotrope-cow-7318.twil.io/assets/c.mp3");
                case 'D':
                    return new Uri("https://heliotrope-cow-7318.twil.io/assets/d.mp3");
                case 'E':
                    return new Uri("https://heliotrope-cow-7318.twil.io/assets/e.mp3");
                default:
                    return new Uri("https://heliotrope-cow-7318.twil.io/assets/yob_question.mp3");
            }
        }



    }
}